package com.fax.utils.frameAnim;

import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipFile;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.util.DisplayMetrics;
import android.util.TypedValue;

public class FileBitmapFrame extends BasicBitmapFrame {
	private String filePath;
	public FileBitmapFrame (String filePath){
		this.filePath = filePath;
	}
	public FileBitmapFrame (String filePath, int duration){
		super(duration);
		this.filePath = filePath;
	}
	public String getFilePath(){
		return filePath;
	}
	@Override
	protected Bitmap decodeBitmap(Context context, int inDensity) throws Exception {
		Options opts = new Options();
        if (inDensity == TypedValue.DENSITY_DEFAULT) {
            opts.inDensity = DisplayMetrics.DENSITY_DEFAULT;
        } else if (inDensity != TypedValue.DENSITY_NONE && inDensity>0) {
            opts.inDensity = inDensity;
        }
        opts.inTargetDensity = context.getResources().getDisplayMetrics().densityDpi;
		return BitmapFactory.decodeFile(filePath, opts);
	}

    @Override
    protected Bitmap decodePreviewBitmap(Context context, int inSampleSize) throws Exception {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = false;
        options.inSampleSize = inSampleSize;
		return BitmapFactory.decodeFile(filePath, options);
    }
}
