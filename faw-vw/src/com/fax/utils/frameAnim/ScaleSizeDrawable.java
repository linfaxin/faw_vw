package com.fax.utils.frameAnim;

import java.lang.reflect.Field;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.Gravity;


public class ScaleSizeDrawable extends Drawable implements Drawable.Callback
{
	public static final String  TAG = ScaleSizeDrawable.class.getSimpleName();
	
	private ScaleSizeState mState;
	private boolean             mMutated;
	
	private final Rect mTmpRect = new Rect();
	
	
	public ScaleSizeDrawable()
	{
		this(null, null);
	}

	public ScaleSizeDrawable(Drawable drawable)
	{
		this(null, null);
		setDrawable(drawable);
	}
	public ScaleSizeDrawable(Drawable drawable,float scaleX, float scaleY)
	{
		this(null, null);
		setDrawable(drawable);
		setScale(scaleX, scaleY);
	}
	
	private ScaleSizeDrawable(ScaleSizeState pulsingState, Resources res)
	{
		mState = new ScaleSizeState(pulsingState, this, res);
	}
	
	public void setDrawable(Drawable drawable)
	{
		if(mState.mDrawable != drawable){
			if(mState.mDrawable != null){
				mState.mDrawable.setCallback(null);
			}
			mState.mDrawable = drawable;
			if(drawable != null){
				drawable.setCallback(this);
			}
		}
	}
	
	public void setScale(float scaleX, float scaleY)
	{
		mState.mScaleX = scaleX;
		mState.mScaleY = scaleY;
	}
	
	public void setUseBounds(boolean useBounds)
	{
		mState.mUseBounds = useBounds;
		onBoundsChange(getBounds());
	}
	public float getFromScale()
	{
		return mState.mScaleX;
	}
	
	public float getToScale()
	{
		return mState.mScaleY;
	}
	
	public boolean isUsingBounds()
	{
		return mState.mUseBounds;
	}
	
	public void draw(Canvas canvas)
	{
		final ScaleSizeState st = mState;
		
		if(st.mDrawable == null){
			return;
		}
		
//		final Rect bounds = (st.mUseBounds ? getBounds() : mTmpRect);
		
		int saveCount = canvas.save();
//		canvas.scale(st.mScale, st.mScale, 
//				bounds.left + bounds.width()  / 2, 
//				bounds.top  + bounds.height() / 2);
		st.mDrawable.draw(canvas);
		canvas.restoreToCount(saveCount);
		
	}
	
	@Override
	protected boolean onStateChange(int[] state)
	{
		boolean changed = false;
		if(mState.mDrawable != null){
			changed |= mState.mDrawable.setState(state);
		}
		onBoundsChange(getBounds());
		return changed;
	}
	
	@Override
	protected boolean onLevelChange(int level)
	{
		if(mState.mDrawable != null){
			mState.mDrawable.setLevel(level);
		}
		onBoundsChange(getBounds());
		return true;
	}
	
	@Override
	protected void onBoundsChange(Rect bounds)
	{
		if(mState.mDrawable != null){
			if(mState.mUseBounds){
				mState.mDrawable.setBounds(bounds);
			}
			else{
				Gravity.apply(Gravity.CENTER, getIntrinsicWidth(), 
					getIntrinsicHeight(), bounds, mTmpRect);
				mState.mDrawable.setBounds(mTmpRect);
			}
		}
	}
	
	@Override
	public int getIntrinsicWidth()
	{
		if(mState.mDrawable != null){
			return (int) (mState.mDrawable.getIntrinsicWidth() * mState.mScaleX);
		}else{
			return -1;
		}
	}
	
	@Override
	public int getIntrinsicHeight()
	{
		if(mState.mDrawable != null){
			return (int) (mState.mDrawable.getIntrinsicHeight() * mState.mScaleY);
		}
		else{
			return -1;
		}
	}
	
	public Drawable getDrawable()
	{
		return mState.mDrawable;
	}
	
	@Override
	public int getChangingConfigurations()
	{
		int changing = super.getChangingConfigurations() 
			| mState.mChangingConfigurations;
		if(mState.mDrawable != null){
			changing |= mState.mDrawable.getChangingConfigurations();
		}
		return changing;
	}
	
	public void setAlpha(int alpha)
	{
		if(mState.mDrawable != null){
			mState.mDrawable.setAlpha(alpha);
		}
	}
	
	public void setColorFilter(ColorFilter cf)
	{
		if(mState.mDrawable != null){
			mState.mDrawable.setColorFilter(cf);
		}
	}
	
	public int getOpacity()
	{
		if(mState.mDrawable != null){
			return mState.mDrawable.getOpacity();
		}
		else{
			return PixelFormat.TRANSLUCENT;
		}
	}
	
	@Override
	public void invalidateDrawable(Drawable who)
	{
		final Callback callback = getCallbackCompat();
		if(callback != null){
			callback.invalidateDrawable(this);
		}
	}
	
	@Override
	public void scheduleDrawable(Drawable who, Runnable what, long when)
	{
		final Callback callback = getCallbackCompat();
		if(callback != null){
			callback.scheduleDrawable(this, what, when);
		}
	}
	
	@Override
	public void unscheduleDrawable(Drawable who, Runnable what)
	{
		final Callback callback = getCallbackCompat();
		if(callback != null){
			callback.unscheduleDrawable(this, what);
		}
	}
	
	@SuppressLint("NewApi")
	private Callback getCallbackCompat()
	{
		if(Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB){
			try{
				Field mCallback = getClass().getField("mCallback");
				mCallback.setAccessible(true);
				return (Callback)mCallback.get(this);
			}
			catch(IllegalArgumentException e){
				return null;
			}
			catch(IllegalAccessException e){
				return null;
			}
			catch(NoSuchFieldException e){
				return null;
			}
		}
		else{
			return getCallback();
		}
	}
	
	@Override
	public boolean getPadding(Rect padding)
	{
		if(mState.mDrawable != null){
			return mState.mDrawable.getPadding(padding);
		}
		else{
			padding.set(0, 0, 0, 0);
			return false;
		}
	}
	
	@Override
	public boolean setVisible(boolean visible, boolean restart)
	{
		if(mState.mDrawable != null){
			mState.mDrawable.setVisible(visible, restart);
		}
		return super.setVisible(visible, restart);
	}
	
	@Override
	public boolean isStateful()
	{
		if(mState.mDrawable != null){
			return mState.mDrawable.isStateful();
		}
		else{
			return false;
		}
	}
	
	@Override
	public ConstantState getConstantState()
	{
		if(mState.canConstantState()){
			mState.mChangingConfigurations = super.getChangingConfigurations();
			return mState;
		}
		return null;
	}
	
	@Override
	public ScaleSizeDrawable mutate()
	{
		if(!mMutated && super.mutate() == this){
			mState = new ScaleSizeState(mState, this, null);
			mMutated = true;
		}
		return this;
	}
	
	
	final static class ScaleSizeState extends Drawable.ConstantState
	{
		Drawable mDrawable;
		
		int mChangingConfigurations;
		
		float   mScaleX  = 1.0f;
		float   mScaleY  = 1.0f;
		boolean mUseBounds = true;
		
		private boolean mCanConstantState;
		private boolean mCheckedConstantState;
		
		
		public ScaleSizeState(ScaleSizeState source, ScaleSizeDrawable owner, Resources res)
		{
			if(source != null){
				if(res != null){
					mDrawable = source.mDrawable.getConstantState().newDrawable(res);
				}
				else{
					mDrawable = source.mDrawable.getConstantState().newDrawable();
				}
				mDrawable.setCallback(owner);
				mScaleX  = source.mScaleX;
				mScaleY  = source.mScaleY;
				mUseBounds = source.mUseBounds;
				mCanConstantState = mCheckedConstantState = true;
			}
		}
		
		@Override
		public Drawable newDrawable()
		{
			return new ScaleSizeDrawable(this, null);
		}
		
		@Override
		public Drawable newDrawable(Resources res)
		{
			return new ScaleSizeDrawable(this, res);
		}
		
		@Override
		public int getChangingConfigurations()
		{
			return mChangingConfigurations;
		}
		
		public boolean canConstantState()
		{
			if(!mCheckedConstantState){
				mCanConstantState = (mDrawable.getConstantState() != null);
				mCheckedConstantState = true;
			}
			
			return mCanConstantState;
		}
	}
}
