package com.fax.utils.frameAnim;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.xmlpull.v1.XmlPullParser;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.util.AttributeSet;
import android.util.Pair;
import android.util.Xml;

public class FrameFactory {

	public static List<FileBitmapFrame> createFramesFromDir(String dir, int duration){
		return createFramesFromDir(dir, duration, false);
	}
	/**
	 * @param naturalSort 是否进行数字排序，为true可以修复xx_1,xx_2,...,xx_11,xx_12,排序错乱的问题
	 * @return
	 */
	public static List<FileBitmapFrame> createFramesFromDir(String dir, int duration, boolean naturalSort){
		try {
			List<FileBitmapFrame> assetFrames = createFramesFromDir(new File(dir).listFiles(), duration);
			if (naturalSort) {
				Collections.sort(assetFrames, new Comparator<FileBitmapFrame>() {
					@Override
					public int compare(FileBitmapFrame lhs, FileBitmapFrame rhs) {
						return NaturalSortUtils.compare(lhs.getFilePath(), rhs.getFilePath());
					}
				});
			}
			return assetFrames;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ArrayList<FileBitmapFrame>();
	}
	public static List<FileBitmapFrame> createFramesFromDir(File[] files, int duration){
		ArrayList<FileBitmapFrame> frames=new ArrayList<FileBitmapFrame>();
		for(File file: files){
			frames.add(new FileBitmapFrame(file.getPath(), duration));
		}
		return frames;
	}
	
	public static List<ZipBitmapFrame> createFramesFromZip(ZipFile zipFile, String entryNameFormat, int start, int end, int duration){
		ArrayList<ZipBitmapFrame> frames = new ArrayList<ZipBitmapFrame>();
		for(int i=start;i<end;i++){
			ZipEntry zipEntry = zipFile.getEntry(String.format(entryNameFormat, i));
			if(zipEntry!=null) frames.add(new ZipBitmapFrame(zipFile, zipEntry.getName(), duration));
		}
		return frames;
	}

	public static List<ZipBitmapFrame> createFramesFromZip(ZipFile zipFile, String entryDir, int duration){
		return createFramesFromZip(zipFile, entryDir, duration, false);
	}
	public static List<ZipBitmapFrame> createFramesFromZip(ZipFile zipFile, String entryDir, int duration, boolean naturalSort){
		ArrayList<ZipBitmapFrame> frames = new ArrayList<ZipBitmapFrame>();
		for(ZipEntry entry : Collections.list(zipFile.entries())){
			String name = entry.getName();
			if(name.contains("__MACOSX")||name.contains(".DS_Store")) continue;
			if(!entry.isDirectory() && name.startsWith(entryDir)){
				frames.add(new ZipBitmapFrame(zipFile, name, duration));
			}
		}
		if (naturalSort) {
			Collections.sort(frames, new Comparator<ZipBitmapFrame>() {
				@Override
				public int compare(ZipBitmapFrame lhs, ZipBitmapFrame rhs) {
					return NaturalSortUtils.compare(lhs.entryName, rhs.entryName);
				}
			});
		}
		return frames;
	}

	public static List<AssetFrame> createFramesFromAsset(Context context, String dir, int duration){
		return createFramesFromAsset(context, dir, duration, false);
	}
	/**
	 * @param naturalSort 是否进行数字排序，为true可以修复xx_1,xx_2,...,xx_11,xx_12,排序错乱的问题
	 * @return
	 */
	public static List<AssetFrame> createFramesFromAsset(Context context, String dir, int duration, boolean naturalSort){
		try {
			List<AssetFrame> assetFrames = createFramesFromAsset(dir, context.getAssets().list(dir), duration);
			if (naturalSort) {
				Collections.sort(assetFrames, new Comparator<AssetFrame>() {
					@Override
					public int compare(AssetFrame lhs, AssetFrame rhs) {
						return NaturalSortUtils.compare(lhs.mPath, rhs.mPath);
					}
				});
			}
			return assetFrames;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ArrayList<AssetFrame>();
	}
	public static List<AssetFrame> createFramesFromAsset(String dir, String[] fileNames, int duration){
		ArrayList<AssetFrame> frames=new ArrayList<AssetFrame>();
		for(String name:fileNames){
			frames.add(new AssetFrame(duration, dir+"/"+name));
		}
		return frames;
	}
	public static List<AssetFrame> createFramesFromAsset(String[] paths, int duration){
		ArrayList<AssetFrame> frames=new ArrayList<AssetFrame>();
		for(String path:paths){
			frames.add(new AssetFrame(duration, path));
		}
		return frames;
	}
	
	public static FrameList createFrameFromAnimRes(Context context, int animResId){
		FrameList frames=new FrameList();
		
		XmlResourceParser parser=context.getResources().getXml(animResId);
        AttributeSet attrs = Xml.asAttributeSet(parser);

        int type;
        try { while ((type = parser.next()) != XmlPullParser.START_TAG && type != XmlPullParser.END_DOCUMENT) {
			}
		} catch (Exception e) {
		}
        
		Resources r=context.getResources();
        TypedArray a = r.obtainAttributes(attrs,getInnernalResIntArray("AnimationDrawable"));
            
        boolean mOneShot = a.getBoolean(getInnernalResInt("AnimationDrawable_oneshot"), true);
        frames.setOneShot(mOneShot);
        
        a.recycle();
        
        final int innerDepth = parser.getDepth()+1;
        int depth;
        try {
			while ((type=parser.next()) != XmlPullParser.END_DOCUMENT &&
			        ((depth = parser.getDepth()) >= innerDepth || type != XmlPullParser.END_TAG)) {
			    if (type != XmlPullParser.START_TAG) {
			        continue;
			    }

			    if (!parser.getName().equals("item")) {
			        continue;
			    }
			    
			    a = r.obtainAttributes(attrs, getInnernalResIntArray("AnimationDrawableItem"));
			    int duration = a.getInt(getInnernalResInt("AnimationDrawableItem_duration") , -1);
			    int drawableRes = a.getResourceId(getInnernalResInt("AnimationDrawableItem_drawable") , 0);
			    a.recycle();
			    if(drawableRes!=0){
			    	if (duration >= 0) {//时间>=0代表正常图片
			            frames.add(new ResBitmapFrame(duration, drawableRes));
			        } else {//时间<0代表动画组
			            frames.addAll(createFrameFromAnimRes(context, drawableRes));
			        }
			    }
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
        
        return frames;
	}
	
	private static int[] getInnernalResIntArray(String res){
		return (int[]) getInnernalRes(res);
	}
	private static int getInnernalResInt(String res){
		return (Integer) getInnernalRes(res);
	}
	private static Object getInnernalRes(String res){
		try {
			String className="com.android.internal.R$styleable";
			if(res.startsWith(className)) res=res.substring(className.length());
			Class styleable=Class.forName(className);
			return styleable.getField(res).get(styleable);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}


