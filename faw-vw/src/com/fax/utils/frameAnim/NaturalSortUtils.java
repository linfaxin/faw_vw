package com.fax.utils.frameAnim;

public class NaturalSortUtils {
	public static int compare(String str1, String str2) {
		int i, j;
		i = 0;
		j = 0;
		String temp1, temp2;
		long num1, num2;
		int length = Math.min(str1.length(), str2.length());
		while (i < length && j < length) {
			temp1="";
			temp2="";
			if (str1.charAt(i) > '9' || str1.charAt(i) < '0' || str2.charAt(j) > '9' || str2.charAt(j) < '0') {
				if (str1.charAt(i) == str2.charAt(j)) {
					i++;
					j++;
					continue;
				} else if (str1.charAt(i) > str2.charAt(j))
					return 1;
				else
					return -1;

			}
			while (str1.charAt(i) <= '9' && str1.charAt(i) >= '0') {
				temp1 += str1.charAt(i);
				i++;
			}
			while (str2.charAt(j) <= '9' && str2.charAt(j) >= '0') {
				temp2 += str2.charAt(j);
				j++;
			}
			try {
				num1 = Long.parseLong(temp1);
				num2 = Long.parseLong(temp2);
				if (num1 == num2) {
					if (temp1.length() < temp2.length())
						return 1;
					else if (temp1.length() > temp2.length())
						return -1;
					else
						continue;
				} else if (num1 > num2)
					return 1;
				else
					return -1;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return (str1.length() > str2.length()) ? 1 : -1;
	}
}
