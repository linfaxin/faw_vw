package com.fax.utils.frameAnim;

import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipFile;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.util.DisplayMetrics;
import android.util.TypedValue;

public class ZipBitmapFrame extends BasicBitmapFrame {
	private String zipFilePath;
	transient ZipFile zipFile;
	String entryName;
	public ZipBitmapFrame (String zipFilePath, String entryName) throws IOException{
		this(new ZipFile(zipFilePath), entryName);
	}
	public ZipBitmapFrame (ZipFile zipFile, String entryName){
		this.zipFile = zipFile;
		this.entryName = entryName;
	}
	public ZipBitmapFrame (ZipFile zipFile, String entryName, int duration){
		super(duration);
		this.zipFile = zipFile;
		this.entryName = entryName;
	}
	private ZipFile getZipFile(){
		if(zipFile==null && zipFilePath!=null){
			synchronized (zipFilePath) {
				try {
					zipFile = new ZipFile(zipFilePath);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return zipFile;
	}
	public String getEntryName(){
		return entryName;
	}
	@Override
	protected Bitmap decodeBitmap(Context context, int inDensity) throws Exception {
		Options opts = new Options();
        if (inDensity == TypedValue.DENSITY_DEFAULT) {
            opts.inDensity = DisplayMetrics.DENSITY_DEFAULT;
        } else if (inDensity != TypedValue.DENSITY_NONE && inDensity>0) {
            opts.inDensity = inDensity;
        }
        opts.inTargetDensity = context.getResources().getDisplayMetrics().densityDpi;
		InputStream is = getZipFile().getInputStream(getZipFile().getEntry(entryName));
		return BitmapFactory.decodeStream(is, null, opts);
	}

    @Override
    protected Bitmap decodePreviewBitmap(Context context, int inSampleSize) throws Exception {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = false;
        options.inSampleSize = inSampleSize;
		InputStream is = getZipFile().getInputStream(getZipFile().getEntry(entryName));
        return BitmapFactory.decodeStream(is, null, options);
    }
}
