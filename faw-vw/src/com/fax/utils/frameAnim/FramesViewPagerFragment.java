package com.fax.utils.frameAnim;

import java.util.ArrayList;
import java.util.List;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.fax.faw_vw.MyApp;
import com.fax.faw_vw.MyFragment;
import com.fax.faw_vw.R;
import com.fax.utils.view.pager.FixViewPager;
import com.fax.utils.view.pager.SamePagerAdapter;
import com.fax.utils.view.photoview.PhotoView;

public class FramesViewPagerFragment extends MyFragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	ViewPager viewPager = new FixViewPager(context);
    	viewPager.setPageMargin((int) MyApp.convertToDp(6));
    	viewPager.setBackgroundColor(Color.BLACK);
        List<Frame> frames = getSerializableExtra(ArrayList.class);
        
        viewPager.setAdapter(new SamePagerAdapter<Frame>(frames) {
			@Override
			public View getView(Frame t, int position, View convertView) {
//				if(convertView == null){
					convertView = new PhotoView(context);
					convertView.setOnClickListener(new View.OnClickListener() {
						public void onClick(View v) {
							backStack();
						}
					});
//				}
				((PhotoView)convertView).setImageDrawable(t.decodeDrawable(context));
				return convertView;
			}
			@Override
			protected void onItemDestroyed(View view, Frame t) {
				super.onItemDestroyed(view, t);
				t.recycle();
			}
		});
        Integer position = getSerializableExtra(Integer.class);
        if(position!=null){
        	viewPager.setCurrentItem(position);
        }
        return viewPager;
    }
}
