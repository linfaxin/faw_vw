package com.fax.utils.frameAnim;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.util.DisplayMetrics;
import android.util.TypedValue;

public class AssetFrame extends BasicBitmapFrame {
	String mPath;
	public AssetFrame(String assetPath) {
		super();
		this.mPath=assetPath;
	}
	public AssetFrame(int duration, String assetPath) {
		super(duration);
		this.mPath=assetPath;
	}

	public String getPath() {
		return mPath;
	}
	@Override
	protected Bitmap decodeBitmap(Context context, int inDensity) throws Exception{
		Options opts = new Options();
        if (inDensity == TypedValue.DENSITY_DEFAULT) {
            opts.inDensity = DisplayMetrics.DENSITY_DEFAULT;
        } else if (inDensity != TypedValue.DENSITY_NONE && inDensity>0) {
            opts.inDensity = inDensity;
        }
        opts.inTargetDensity = context.getResources().getDisplayMetrics().densityDpi;
		return BitmapFactory.decodeStream(context.getAssets().open(mPath), null, opts);
	}

    @Override
    protected Bitmap decodePreviewBitmap(Context context, int inSampleSize) throws Exception {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = false;
        options.inSampleSize = inSampleSize;
        return BitmapFactory.decodeStream(context.getAssets().open(mPath), null, options);
    }

}
