package com.fax.faw_vw;

import java.util.ArrayList;
import java.util.List;

import com.fax.faw_vw.fragment_dealer.SearchDealerFragment;
import com.fax.faw_vw.fragment_more.FeedbackFragment;
import com.fax.faw_vw.fragment_more.MenuListSettingFragment;
import com.fax.faw_vw.fragment_more.OnlineQAFragment;
import com.fax.faw_vw.fragment_more.PersonFragment;
import com.fax.faw_vw.fragment_more.QueryIllegalFragment;
import com.fax.faw_vw.fragments_car.BookDriveFragment;
import com.fax.faw_vw.fragments_main.BrandFragment;
import com.fax.faw_vw.fragments_main.FindCarsAssistorFragment;
import com.fax.faw_vw.fragments_main.HomeFragment;
import com.fax.faw_vw.fragments_main.MoreFragment;
import com.fax.faw_vw.fragments_main.ShowCarsFragment;
import com.fax.faw_vw.menu.QRFragment;
import com.fax.faw_vw.menu.SearchAppFragment;
import com.fax.faw_vw.model.ImageResPagePair;
import com.fax.faw_vw.model.ImageTextPagePair;
import com.fax.faw_vw.util.Blur;
import com.fax.faw_vw.views.AppDialogBuilder;
import com.fax.utils.frameAnim.AssetFrame;
import com.fax.utils.frameAnim.Frame;
import com.fax.utils.frameAnim.FrameAnimListener;
import com.fax.utils.frameAnim.FrameAnimation;
import com.fax.utils.frameAnim.FrameFactory;
import com.fax.utils.view.RadioGroupFragmentBinder;
import com.fax.utils.view.RadioGroupStateFragmentBinder;
import com.fax.utils.view.list.ObjectXAdapter;
import com.fax.utils.view.list.ObjectXListView;
import com.umeng.analytics.MobclickAgent;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Transformation;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

public class MainActivity extends BaseActivity {
	RadioGroup radioGroup;
	ObjectXListView menuList;
	@Override
	protected void onResume() {
		super.onResume();
		menuList.reload();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		final DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
		View drawerLayoutControlBtn = findViewById(R.id.drawerLayoutControlBtn);
		if(drawerLayoutControlBtn!=null){
			drawerLayoutControlBtn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if(drawerLayout.isDrawerOpen(Gravity.START)){
						drawerLayout.closeDrawers();
					}else{
						drawerLayout.openDrawer(Gravity.START);
					}
				}
			});
		}
		
		//初始化侧边栏
		findViewById(R.id.main_menu_btn_tel).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String tel = ((TextView)v).getText().toString();
				startActivity(new Intent(Intent.ACTION_DIAL).setData(Uri.parse("tel:"+tel)));
				MobclickAgent.onEvent(MainActivity.this, "android_home_Call");
			}
		});
		findViewById(R.id.main_menu_btn_search).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				FragmentContain.start(MainActivity.this, SearchAppFragment.class);
			}
		});
		//初始化侧边栏里可以被配置的条目
		menuList = (ObjectXListView) findViewById(android.R.id.list);
		menuList.setAdapter(new ObjectXAdapter.SingleLocalPageAdapter<ImageTextPagePair>() {
			@Override
			public View bindView(ImageTextPagePair t, int position, View convertView) {
				TextView tv = (TextView) convertView;
				if(tv==null){
					tv = new TextView(MainActivity.this);
					int padding = (int) MyApp.convertToDp(12);
					tv.setPadding(padding, padding, padding, padding);
					tv.setBackgroundResource(R.drawable.common_btn_in_black);
					tv.setCompoundDrawablePadding((int) MyApp.convertToDp(12));
					tv.setGravity(Gravity.LEFT|Gravity.CENTER_VERTICAL);
					tv.setMinHeight((int) MyApp.convertToDp(50));
					tv.setTextColor(Color.WHITE);
					tv.setTextSize(20);
				}
				tv.setText(t.getText());
				tv.setCompoundDrawablesWithIntrinsicBounds(t.getImgResId(), 0, 0, 0);
				return tv;
			}
			@Override
			public List<ImageTextPagePair> instanceNewList() throws Exception {
				return MenuListSettingFragment.getEnablePagePairs(MainActivity.this);
			}
			@Override
			public boolean isDynamicHeight() {
				return true;
			}
			@Override
			public void onItemClick(ImageTextPagePair t, View view, int position, long id) {
				super.onItemClick(t, view, position, id);
				MobclickAgent.onEvent(MainActivity.this, "android_home_menu_"+t.getText());
				if(t.getFragment()!=null){
					if(t.isLandscape()){
						FragmentContainLandscape.start(MainActivity.this, t.getFragment());
					}else{
						FragmentContain.start(MainActivity.this, t.getFragment());
					}
				}
			}
		});
		
		
		//初始化模糊效果
		drawerLayout.setScrimColor(Color.TRANSPARENT);
		final View contextView = drawerLayout.getChildAt(0);
		final ImageView frontView = (ImageView) contextView.findViewById(android.R.id.background);
		drawerLayout.setDrawerListener(new DrawerLayout.SimpleDrawerListener() {
			@SuppressLint("NewApi")
			@Override
			public void onDrawerSlide(View drawerView, float slideOffset) {
				super.onDrawerSlide(drawerView, slideOffset);
				if(Build.VERSION.SDK_INT>=11) frontView.setAlpha(slideOffset);
			}
			Bitmap blurBitmap;
			@Override
			public void onDrawerStateChanged(int newState) {
				super.onDrawerStateChanged(newState);
				if(newState == DrawerLayout.STATE_DRAGGING){
					if(blurBitmap==null) 
						blurBitmap = Bitmap.createBitmap(contextView.getWidth()/4 , contextView.getHeight()/4 ,Bitmap.Config.ARGB_8888);
					try {
						Canvas canvas = new Canvas(blurBitmap);
						canvas.scale(.25f, .25f);
						contextView.draw(canvas);
						blurBitmap = Blur.fastblur(getApplicationContext(), blurBitmap, 1);
						frontView.setImageBitmap(blurBitmap);
						frontView.setVisibility(View.VISIBLE);
					} catch (Exception e) {
					}
				}else if(newState == DrawerLayout.STATE_IDLE && !drawerLayout.isDrawerOpen(GravityCompat.START)){
					frontView.setVisibility(View.INVISIBLE);
				}
			}
		});
		
		//绑定底部按钮与页卡
		radioGroup = (RadioGroup) findViewById(R.id.radio_group);
		radioGroup.setOnCheckedChangeListener(new RadioGroupFragmentBinder(getSupportFragmentManager(), R.id.contain) {
			@Override
			public Fragment instanceFragment(int checkedId) {
				switch(checkedId){
				case R.id.bottom_bar_home: return new HomeFragment();
				case R.id.bottom_bar_show_cars: return new ShowCarsFragment();
				case R.id.bottom_bar_find_cars: return new FindCarsAssistorFragment();
				case R.id.bottom_bar_brand: return new BrandFragment();
				case R.id.bottom_bar_more: return new MoreFragment();
				}
				return null;
			}
			@Override
			public void onChecked(int checkedId, Fragment fragment) {
				super.onChecked(checkedId, fragment);
				switch(checkedId){
				case R.id.bottom_bar_home: MobclickAgent.onEvent(MainActivity.this, "android_home_PV");
				case R.id.bottom_bar_show_cars: MobclickAgent.onEvent(MainActivity.this, "android_carlist_PV");
				case R.id.bottom_bar_find_cars: MobclickAgent.onEvent(MainActivity.this, "android_Assistant_PV");
				case R.id.bottom_bar_brand: MobclickAgent.onEvent(MainActivity.this, "android_Brand_PV");
				case R.id.bottom_bar_more: MobclickAgent.onEvent(MainActivity.this, "android_More_PV");
				}
				if(checkedId==R.id.bottom_bar_show_cars){
					//首次进入选车型的提示
					if(!MyApp.hasKeyOnce("tip_show_cars")){
						final View tip = View.inflate(MainActivity.this, R.layout.tip_choose_model, null);
						if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
							tip.setPadding(0 , 0, 0, radioGroup.getHeight());
						}
						addContentView(tip, new FrameLayout.LayoutParams(-1, -1));
						tip.setOnClickListener(new View.OnClickListener() {
							@Override
							public void onClick(View v) {
								((ViewGroup)tip.getParent()).removeView(tip);
							}
						});
					}
				}
			}
		});
		((CompoundButton)radioGroup.findViewById(R.id.bottom_bar_home)).setChecked(true);
		
		//每次进入App显示右滑的提示动画
		final View slibIndicator = findViewById(R.id.main_indicator);
		List<AssetFrame> animFrames = FrameFactory.createFramesFromAsset(this, "arrow_anim/anim", 30);
		List<AssetFrame> dismissFrames = FrameFactory.createFramesFromAsset(this, "arrow_anim/dismiss", 30);
		ArrayList<Frame> frames = new ArrayList<Frame>();
		frames.addAll(animFrames);
		frames.addAll(animFrames);
		frames.addAll(animFrames);
		frames.addAll(dismissFrames);
		FrameAnimation frameAnimation = new FrameAnimation(slibIndicator, frames);
		float scale = 1f * getResources().getDisplayMetrics().widthPixels / 640;//资源是640宽的设计稿里下来的
		frameAnimation.setScale(scale, scale);
		frameAnimation.setFrameAnimListener(new FrameAnimListener() {
			@Override
			public void onStart(FrameAnimation animation) {
			}
			@Override
			public void onPlaying(FrameAnimation animation, int frameIndex) {
			}
			@Override
			public void onFinish(FrameAnimation animation) {
				try {
					((ViewGroup) slibIndicator.getParent()).removeView(slibIndicator);
				} catch (Exception e) {
				}
			}
		});
		frameAnimation.start();
		
		//首次进入操作提示
		if(!MyApp.hasKeyOnce("tip_main")){
			final View tip = View.inflate(this, R.layout.tip_main, null);
			addContentView(tip, new FrameLayout.LayoutParams(-1, -1));
			tip.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					((ViewGroup)tip.getParent()).removeView(tip);
				}
			});
		}
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		int rId = intent.getIntExtra(RadioGroup.class.getName(), -1);
		if(rId>0){
			radioGroup.check(rId);
		}
	}
}
