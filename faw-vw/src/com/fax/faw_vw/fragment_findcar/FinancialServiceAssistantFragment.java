package com.fax.faw_vw.fragment_findcar;

import java.io.IOException;
import java.io.Serializable;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.fax.faw_vw.MyApp;
import com.fax.faw_vw.MyFragment;
import com.fax.faw_vw.R;
import com.fax.faw_vw.fargment_common.WebViewFragment;
import com.fax.faw_vw.fragment_dealer.SearchDealerFragment;
import com.fax.faw_vw.fragment_findcar.FinancialServiceAssistantFragment.CarSelectPage.BankInfo;
import com.fax.faw_vw.fragment_findcar.FinancialServiceAssistantFragment.FuckCarInfos.FuckCar;
import com.fax.faw_vw.fragments_car.BookDriveFragment;
import com.fax.faw_vw.fragments_car.CarSpecsFragment;
import com.fax.faw_vw.fragments_car.ModelListFragment;
import com.fax.faw_vw.fragments_main.ShowCarsFragment;
import com.fax.faw_vw.model.Dealer;
import com.fax.faw_vw.model.Response;
import com.fax.faw_vw.model.ShowCarItem;
import com.fax.faw_vw.model.CarModelList.CarModel;
import com.fax.faw_vw.model.ShowCarItemRes.CarItemChild;
import com.fax.faw_vw.model.Specs;
import com.fax.faw_vw.util.ResponseTask;
import com.fax.faw_vw.views.AppDialogBuilder;
import com.fax.faw_vw.views.CommonRowItem;
import com.fax.faw_vw.views.MyTopBar;
import com.fax.utils.view.MultiFormatTextView;
import com.fax.utils.view.RadioGroupFragmentBinder;
import com.fax.utils.view.TopBarContain;
import com.fax.utils.view.list.ObjectXAdapter;
import com.fax.utils.view.list.ObjectXListView;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

public class FinancialServiceAssistantFragment extends MyFragment {
	static FinancialServiceAssistantFragment financialServiceAssistantFragment;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		financialServiceAssistantFragment = this;
	}
	RadioGroup radioGroup;
	CarSelectPage carSelectPage;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.financial_service_assistant_frame, container, false);
		radioGroup = (RadioGroup) view.findViewById(R.id.radio_group);
		RadioGroupFragmentBinder.bindFragment(radioGroup, new RadioGroupFragmentBinder(getFragmentManager(), R.id.contain2) {
			@Override
			public Fragment instanceFragment(int checkedId) {
				switch (checkedId) {
				case R.id.radioButton1:
					return carSelectPage = new CarSelectPage();
				case R.id.radioButton2:
					return new SelectMoneyPage();
				default:
					break;
				}
				return null;
			}
		});
		radioGroup.check(R.id.radioButton1);
		return new MyTopBar(context).setLeftBack().setTitle("金融助手").setContentView(view);
	}

	public static class CarSelectPage extends MyFragment{
		private static int[] months = new int[]{12, 18, 24, 36, 48, 60};
		private static float[] firstPays = new float[]{.2f, .3f, .4f, .5f, .6f, .7f, .8f};
		ObjectXListView listView;
		View selectCarHead;
		
		ShowCarItem showCarItem;
		CarModel carModel;
		int monthIndex = 0;
		float firstPay = .3f;
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			listView = new ObjectXListView(context);
			listView.setVerticalScrollBarEnabled(false);
			initListHead();
			listView.addHeaderView(selectCarHead);
			listView.setPullRefreshEnable(false);
			listView.setAdapter(new ObjectXAdapter.SingleLocalPageAdapter<BankInfo>(){
				@Override
				public View bindView(BankInfo t, int position, View convertView) {
					if(convertView==null){
						convertView = View.inflate(context, R.layout.financial_service_assis_item, null);
					}
					((TextView)convertView.findViewById(android.R.id.title)).setText(position+1+"");
					((MultiFormatTextView)convertView.findViewById(android.R.id.summary))
						.formatText(t.bankName, getCarPrice()+t.getInterest(), (int)t.getPMT(), t.getInterest());
					return convertView;
				}
				@Override
				public List<BankInfo> instanceNewList() throws Exception {
					return getBankInfos();
				}
				@Override
				public void onItemClick(BankInfo t, View view, int position, long id) {
					super.onItemClick(t, view, position, id);
					if(carModel==null){
						return;
					}
					if(t.bankName.contains("招商银行")){
						WebViewFragment.start(context, 
								"https://ccclub.cmbchina.com/fincreditweb/Apply/ApplyDetail.aspx?WT.mc_id=C11ZQWZ140217001");
					}else{
						String info = "%s:\n总共支付：%s元，月供：%s元\n该方案需支付%s元利息";
						info = String.format(info, t.bankName, getCarPrice()+t.getInterest()+"", (int)t.getPMT()+"", t.getInterest()+"");
						startFragment(MyApp.createFragment(SelectCarDetial.class, showCarItem, carModel, info));
					}
				}
			});
			
			return listView;
		}
		private void initListHead(){
			selectCarHead = View.inflate(context, R.layout.financial_service_assistant_selectcar_head, null);
			selectCarHead.findViewById(R.id.select_car_button).setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(final View v) {
					ShowCarsFragment showCarsFragment = MyApp.createFragment(ShowCarsFragment.class);
					showCarsFragment.onSelectItem = new ShowCarsFragment.OnSelectItem() {
						@Override
						public void onSelectCar(ShowCarItem item) {
							setShowCarItem(item);
						}
					};
					addFragment(showCarsFragment);
				}
			});
			selectCarHead.findViewById(R.id.select_car_model_button).setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if(showCarItem==null){
						Toast.makeText(context, "请先选择车型", Toast.LENGTH_SHORT).show();
						return;
					}
					ModelListFragment modelListFragment = MyApp.createFragment(ModelListFragment.class, showCarItem);
					modelListFragment.onSelectItem = new ModelListFragment.OnSelectItem() {
						@Override
						public void onSelectCarModel(CarModel item) {
							setCarModel(item);
						}
					};
					addFragment(modelListFragment);
				}
			});
			selectCarHead.findViewById(R.id.row_pay_months).setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					final String[] monthTexts = new String[months.length];
					for(int i=0,length = monthTexts.length;i<length;i++){
						monthTexts[i] = months[i]+"个月";
					}
					new AlertDialog.Builder(context).setItems(monthTexts, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							setMouthIndex(which);
						}
					}).show();
				}
			});
			selectCarHead.findViewById(R.id.row_first_pay).setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					final String[] payTexts = new String[firstPays.length];
					for(int i=0,length = payTexts.length;i<length;i++){
						payTexts[i] = (int)(100*firstPays[i])+"%";
					}
					new AlertDialog.Builder(context).setItems(payTexts, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							setFirstPay(firstPays[which]);
						}
					}).show();
				}
			});
		}
		int getCarPrice(){
			if(carModel==null) return 0;
			try {
				return Integer.parseInt(carModel.getPrice());
			} catch (Exception e) {
				e.printStackTrace();
			}
			return 0;
		}
		public void setShowCarItem(ShowCarItem item){
			showCarItem = item;
			((CommonRowItem)selectCarHead.findViewById(R.id.select_car_button)).setContent(item==null?"":item.getModel_cn());
			setCarModel(null);
		}
		public void setCarModel(CarModel carModel){
			this.carModel = carModel;
			((CommonRowItem)selectCarHead.findViewById(R.id.select_car_model_button)).setContent(
					carModel==null?"":carModel.getTitle());
			((CommonRowItem)selectCarHead.findViewById(R.id.select_car_price_button)).setContent(
					carModel==null?"0":carModel.getPrice());
			listView.reload();
		}
		public void setFirstPay(float firstPay){
			this.firstPay = firstPay;
			((CommonRowItem)selectCarHead.findViewById(R.id.row_first_pay)).setContent(
					(int)(firstPay*100)+"%");
			listView.reload();
		}
		public void setMouthIndex(int monthIndex){
			this.monthIndex = monthIndex;
			((CommonRowItem)selectCarHead.findViewById(R.id.row_pay_months)).setContent(
					months[monthIndex]+"个月");
			listView.reload();
		}
		
		public ArrayList<BankInfo> getBankInfos(){
			ArrayList<BankInfo> bankInfoList = new ArrayList<FinancialServiceAssistantFragment.CarSelectPage.BankInfo>();
			FuckCar fuckCar = fuckCarInfos.get(showCarItem==null?null:showCarItem.getId());
			if(fuckCar!=null){
				bankInfos[0].rates = fuckCar.dz_rate;
				bankInfos[1].rates = fuckCar.pa_rate;
				bankInfos[2].rates = fuckCar.yq_rate;
				bankInfos[3].rates = fuckCar.zs_rate;
				bankInfos[4].rates = fuckCar.gs_rate;
				bankInfos[5].rates = fuckCar.js_rate;
			}
			for(BankInfo bankInfo : bankInfos){
				if(bankInfo.rates.length>monthIndex){
					bankInfoList.add(bankInfo);
				}
			}
			return bankInfoList;
		}
		BankInfo[] bankInfos = new BankInfo[]{
				new BankInfo("大众金融-标准信贷-等额本息",3.17f,5.17f,6.17f,8.17f,9.17f,10.17f),
				new BankInfo("平安银行-标准信贷-等额本息",2.17f,4.17f,5.17f,7.17f,8.17f,8.17f),
				new BankInfo("一汽金融-标准信贷-等额本息",3.17f,5.17f,6.17f,8.17f,9.17f,10.17f),
				new BankInfo("招商银行-标准信贷-等额本息",0f,3f,5.5f,9.5f),
				new BankInfo("工商银行-标准信贷",0f,1.5f,3.5f,7.5f),
				new BankInfo("建设银行-标准信贷",0,2,4,8),
		};
		class BankInfo{
			String bankName;
			double[] rates;//12，18，24，36，48，60个月
			public BankInfo(String bankName, double... rates) {
				super();
				this.bankName = bankName;
				this.rates = rates;
			}

			private int getInterest(){
				return (int) (getPMT() * months[monthIndex] - getCarPrice()*(1-firstPay));
			}
			private double getPMT(){
				return pmt(rates[monthIndex]/100, months[monthIndex], getCarPrice()*(1-firstPay));
			}
			/**
			* 计算月供
			* @param rate 年利率 年利率除以12就是月利率
			* @param term 贷款期数，单位月
			* @param financeAmount  贷款金额/元
			*/
			private double pmt(double rate, int term, float financeAmount){
				if(rate==0){
					return financeAmount / term;
				}
			    double v = (1+(rate/12)); 
			    double t = (-(term/12)*12); 
			    double result = (financeAmount*(rate/12))/(1-Math.pow(v,t));
			    return result;
			}
		}
	}
	public static class SelectCarDetial extends MyFragment{
		CommonRowItem selectDealerBtn;
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			final ShowCarItem showCarItem = getSerializableExtra(ShowCarItem.class);
			final CarModel carModel = getSerializableExtra(CarModel.class);
			final String planInfo = getSerializableExtra(String.class);
			
			final View view = View.inflate(context, R.layout.financial_service_assistant_selectcar_detail, null);
			selectDealerBtn = (CommonRowItem) view.findViewById(R.id.select_dealer_button);
			selectDealerBtn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					SearchDealerFragment dealerFragment = new SearchDealerFragment();
					dealerFragment.setTargetFragment(SelectCarDetial.this, Request_SelectDealer);
					addFragment(dealerFragment);
				}
			});
			final CommonRowItem buyCarTimeRow = (CommonRowItem) view.findViewById(R.id.select_buy_car_time);
			buyCarTimeRow.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					final String[] items = new String[]{"一周内","一个月内","三个月内","半年内"};
					new AlertDialog.Builder(context).setTitle("请选择").setItems(items, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							buyCarTimeRow.setContent(items[which]);
						}
					}).setNegativeButton(android.R.string.cancel, null).show();
				}
			});
			((CommonRowItem)view.findViewById(R.id.select_car_model_button)).setContent(carModel.getModel_name());
			((CommonRowItem)view.findViewById(R.id.financial_service_assistant_plan)).setContentSize(12);
			((CommonRowItem)view.findViewById(R.id.financial_service_assistant_plan)).setContent(planInfo);
			
			view.findViewById(R.id.commit_button).setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					final String name = ((TextView)view.findViewById(R.id.name_text)).getText().toString();
					final String phone = ((TextView)view.findViewById(R.id.phone_text)).getText().toString();
					final String time = buyCarTimeRow.getContent();
					if(TextUtils.isEmpty(name)){
						Toast.makeText(context, "请输入姓名", Toast.LENGTH_SHORT).show();
						return;
					}
					if(phone.length()!=11 || (!phone.startsWith("13") && !phone.startsWith("14")
							&& !phone.startsWith("15") && !phone.startsWith("17") && !phone.startsWith("18")) ){
						Toast.makeText(context, "请输入正确的11位手机号码", Toast.LENGTH_SHORT).show();
						return;
					}
					if(dealer==null){
						Toast.makeText(context, "请选择经销商", Toast.LENGTH_SHORT).show();
						return;
					}
					if(TextUtils.isEmpty(time)){
						Toast.makeText(context, "请选择预计购车时间", Toast.LENGTH_SHORT).show();
						return;
					}
					String url = "http://faw-vw.allyes.com/index.php?g=api&m=aide&a=add"
							+ "&adminid="+dealer.getId()
							+ "&admintitle="+URLEncoder.encode(dealer.getName())
							+ "&truename="+URLEncoder.encode(name)
							+ "&mobile="+phone
							+ "&modelid="+carModel.getId()
							+ "&modelname="+URLEncoder.encode(carModel.getModel_name())
							+ "&aideid=1"//先固定1
							+ "&aidetitle="+URLEncoder.encode(planInfo)
							+ "&buydate="+URLEncoder.encode(time);
					new ResponseTask<Response>(context, url) {
						@Override
						protected void onPostExecuteSuc(Response result) {
							new AppDialogBuilder(context).setTitle("提交成功。\n感谢您的支持！")
								.setMessage("我们将尽快安排客服人员与您取得联系。").setPositiveButton(null)
								.setOnDismissListener(new DialogInterface.OnDismissListener() {
									@Override
									public void onDismiss(DialogInterface dialog) {
										backStack();
									}
								}).show();
						}
					}.setProgressDialog().execute();
				}
			});
			
			return new MyTopBar(context).setLeftBack().setTitle("填写信息").setContentView(view);
		}
		public static final int Request_SelectDealer = 1;
		Dealer dealer;
		@Override
		public void onActivityResult(int requestCode, int resultCode, Intent data) {
			super.onActivityResult(requestCode, resultCode, data);
			if(resultCode == Activity.RESULT_OK){
				switch(requestCode){
				case Request_SelectDealer :
					this.dealer = (Dealer) data.getSerializableExtra(Dealer.class.getName());
					selectDealerBtn.setContent(dealer.getName());
					break;
				}
			}
		}
		
	}
	
	public static class SelectMoneyPage extends MyFragment{
		private static int[] monthPays = new int[]{1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000, 15000};
		private static int[] firstPays = new int[]{10000, 20000, 30000, 40000, 50000, 60000, 70000, 80000, 90000, 100000, 150000};
		ObjectXListView listView;
		View selectMoneyHead;
		
		ShowCarItem showCarItem;
		int monthPay = 3000;
		int firstPay = 30000;
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			listView = new ObjectXListView(context);
			initListHead();
			listView.addHeaderView(selectMoneyHead);
			listView.setPullRefreshEnable(false);
			listView.setAdapter(new ObjectXAdapter.SingleLocalPageAdapter<ShowCarItem>(){
				@Override
				public View bindView(ShowCarItem t, int position, View view) {
					if(view == null){
						view = View.inflate(context, R.layout.main_showcar_list_item, null);
					}
					((ImageView)view.findViewById(R.id.brand_img)).setImageResource(t.getResId());
					
					LinearLayout nameLayout = (LinearLayout) view.findViewById(R.id.brand_name_layout);
					nameLayout.removeAllViews();
					
					TextView brandCnTv = new TextView(context);
					brandCnTv.setTypeface(Typeface.DEFAULT_BOLD);
					brandCnTv.setTextSize(18);
					TextView brandEnTv = new TextView(context);
					brandEnTv.setTextSize(18);
					brandEnTv.setTextColor(getResources().getColor(android.R.color.darker_gray));
					brandCnTv.setText(t.getModel_cn());
					brandEnTv.setText(t.getModel_en());
					nameLayout.addView(brandCnTv);
					nameLayout.addView(brandEnTv);
					return view;
				}
				@Override
				public List<ShowCarItem> instanceNewList() throws Exception {
					ShowCarItem[] showCarItems = ShowCarItem.SHOW_CAR_ITEMS_Financial;
					ArrayList<ShowCarItem> showCarList = new ArrayList<ShowCarItem>();
					int minPrice = getMinPrice();
					int maxPrice = getMaxPrice();
					for(ShowCarItem showCarItem : showCarItems){
						FuckCar fuckCar = fuckCarInfos.get(showCarItem.getId());
						if(fuckCar.hasPriceIn(minPrice, maxPrice)){
							showCarList.add(showCarItem);
						}
					}
					return showCarList;
				}
				@Override
				public void onItemClick(ShowCarItem t, View view, int position, long id) {
					super.onItemClick(t, view, position, id);
					addFragment(MyApp.createFragment(SelectMoneyDetail.class, t, new int[]{getMinPrice(), getMaxPrice()}));
				}
			});
			setFirstPay(30000);
			setMouthPay(3000);
			return listView;
		}
		int getMinPrice(){
			return firstPay + 12 * monthPay;
		}
		int getMaxPrice(){
			return firstPay + 60 * monthPay;
		}
		private void initListHead(){
			selectMoneyHead = View.inflate(context, R.layout.financial_service_assistant_selectmoney_head, null);
			selectMoneyHead.findViewById(R.id.row_first_pay).setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					final String[] payTexts = new String[firstPays.length];
					for(int i=0,length = payTexts.length;i<length;i++){
						payTexts[i] = firstPays[i]+"元";
					}
					new AlertDialog.Builder(context).setItems(payTexts, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							setFirstPay(firstPays[which]);
						}
					}).show();
				}
			});
			selectMoneyHead.findViewById(R.id.row_month_pay).setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					final String[] monthTexts = new String[monthPays.length];
					for(int i=0,length = monthTexts.length;i<length;i++){
						monthTexts[i] = monthPays[i]+"元";
					}
					new AlertDialog.Builder(context).setItems(monthTexts, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							setMouthPay(monthPays[which]);
						}
					}).show();
				}
			});
		}public void setFirstPay(int firstPay){
			this.firstPay = firstPay;
			((CommonRowItem)selectMoneyHead.findViewById(R.id.row_first_pay)).setContent(firstPay+"元");
			listView.reload();
			refreshMoneyRange();
		}
		public void setMouthPay(int monthIndex){
			this.monthPay = monthIndex;
			((CommonRowItem)selectMoneyHead.findViewById(R.id.row_month_pay)).setContent(monthIndex+"元");
			listView.reload();
			refreshMoneyRange();
		}
		private void refreshMoneyRange(){
			((MultiFormatTextView)selectMoneyHead.findViewById(R.id.financial_service_assistant_money_range))
				.formatText(getMinPrice(), getMaxPrice());
		}
	}
	
	public static class SelectMoneyDetail extends MyFragment{

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			View view = View.inflate(context, R.layout.financial_service_assistant_selectmoney_detail, null);
			
			final ShowCarItem carItem = getSerializableExtra(ShowCarItem.class);
			final int[] minAndMax = getSerializableExtra(int[].class);
			
			((MultiFormatTextView)view.findViewById(R.id.financial_service_assistant_money_range)).formatText(minAndMax[0], minAndMax[1]);
			((ImageView)view.findViewById(R.id.brand_img)).setImageResource(carItem.getResId());
			((MultiFormatTextView)view.findViewById(R.id.brand_name_tv)).formatText(carItem.getModel_cn(), carItem.getModel_en());
			
			ObjectXListView listView = (ObjectXListView) view.findViewById(android.R.id.list);
			listView.setAdapter(new ObjectXAdapter.SingleLocalPageAdapter<CarModel>(){
				@Override
				public View bindView(CarModel t, int position, View convertView) {
					CommonRowItem rowItem = (CommonRowItem) convertView;
					if(rowItem==null){
						rowItem = new CommonRowItem(context);
						rowItem.setHasRightArrow(true);
					}
					rowItem.setTitle(t.getModel_name());
					return rowItem;
				}
				@Override
				public List<CarModel> instanceNewList() throws Exception {
					return fuckCarInfos.get(carItem.getId()).getCarModelsInPrice(minAndMax[0], minAndMax[1]);
				}
				@Override
				public void onItemClick(final CarModel t, View view, int position, long id) {
					super.onItemClick(t, view, position, id);
					new AlertDialog.Builder(context).setItems(new String[]{"金融方案", "参数配置"}, new DialogInterface.OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							if(which==0){
								financialServiceAssistantFragment.radioGroup.check(R.id.radioButton1);
								financialServiceAssistantFragment.carSelectPage.setShowCarItem(carItem);
								financialServiceAssistantFragment.carSelectPage.setCarModel(t);
								backStack();
							}else{
								startFragment(MyApp.createFragment(CarSpecsFragment.class, t));
							}
						}
					}).show();
					
				}
				
			});
			return new MyTopBar(context).setLeftBack().setTitle("金融助手").setContentView(view);
		}
		
	}
	
	
	
	
	
	
	
	
	public static FuckCarInfos fuckCarInfos = null;
	static{
		try {
			fuckCarInfos = new Gson().fromJson(
				IOUtils.toString(MyApp.getAppContext().getAssets().open("fuckCar.json")), FuckCarInfos.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	static class FuckCarInfos{
		FuckCar[] key;
		public FuckCar get(String carId){
			for(FuckCar fuckCar : key){
				if(fuckCar.id!=null && fuckCar.id.equals(carId)){
					return fuckCar;
				}
			}
			return null;
		}
		class FuckCar{
			String name;
			String en;
			String id;
			double[] dz_rate;
			double[] yq_rate;
			double[] gs_rate;
			double[] js_rate;
			double[] pa_rate;
			double[] zs_rate;
			Option[] option;
			class Option{
				String name;
				int id;
				int price;
			}
			List<CarModel> getCarModelsInPrice(int min, int max){
				ArrayList<CarModel> carModels = new ArrayList<CarModel>();
				for(Option op : option){
					if(op.price>=min && op.price<=max){
						carModels.add(new CarModel(op.id+"", op.name, op.price));
					}
				}
				return carModels;
			}
			boolean hasPriceIn(int min, int max){
				for(Option op : option){
					if(op.price>=min && op.price<=max){
						return true;
					}
				}
				return false;
			}
		}
	}
}
