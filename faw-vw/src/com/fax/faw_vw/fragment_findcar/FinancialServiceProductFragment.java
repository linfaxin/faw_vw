package com.fax.faw_vw.fragment_findcar;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.CompoundButton;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.RadioGroup;

import com.fax.faw_vw.MyApp;
import com.fax.faw_vw.MyFragment;
import com.fax.faw_vw.R;
import com.fax.faw_vw.util.ViewUtils;
import com.fax.faw_vw.views.MyTopBar;

//金融产品
public class FinancialServiceProductFragment extends MyFragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.financial_service_product, null);
		RadioGroup radioGroup = (RadioGroup) view.findViewById(R.id.radio_group);
		final ImageView midImage = (ImageView) view.findViewById(R.id.financial_service_product_content);
		radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				CompoundButton checkedBtn = (CompoundButton)group.findViewById(checkedId);
				if(!checkedBtn.isChecked()) return;//avoid check off callback
				switch(checkedId){
				case R.id.radioButton1:
					midImage.setImageResource(R.drawable.financial_service_product_1);
					break;
				case R.id.radioButton2:
					midImage.setImageResource(R.drawable.financial_service_product_2);
					break;
				case R.id.radioButton3:
					midImage.setImageResource(R.drawable.financial_service_product_3);
					break;
				case R.id.radioButton4:
					midImage.setImageResource(R.drawable.financial_service_product_4);
					break;
				case R.id.radioButton5:
					midImage.setImageResource(R.drawable.financial_service_product_5);
					break;
				case R.id.radioButton6:
					midImage.setImageResource(R.drawable.financial_service_product_6);
					break;
				case R.id.radioButton7:
					midImage.setImageResource(R.drawable.financial_service_product_7);
					break;
				case R.id.radioButton8:
					midImage.setImageResource(R.drawable.financial_service_product_8);
					break;
				}
			}
		});
		final View arrowRight = view.findViewById(R.id.arrowRight);
		final View arrowLeft = view.findViewById(R.id.arrowLeft);
		final HorizontalScrollView scrollView = (HorizontalScrollView) view.findViewById(R.id.scrollView);
		scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
			@Override
			public void onScrollChanged() {
				arrowRight.setVisibility(View.VISIBLE);
				arrowLeft.setVisibility(View.VISIBLE);
				
				if(scrollView.getScrollX()>=scrollView.getMaxScrollAmount()-MyApp.convertToDp(6)){
					arrowRight.setVisibility(View.INVISIBLE);
				}
				if(scrollView.getScrollX()<=0){
					arrowLeft.setVisibility(View.INVISIBLE);
				}
			}
		});

		ViewUtils.fitAllTextToPadSize(view);
		return new MyTopBar(context).setLeftBack().setFitLand(true).setTitle("金融产品").setContentView(view);
	}

}
