package com.fax.faw_vw.fragment_findcar;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Pair;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fax.faw_vw.FragmentContain;
import com.fax.faw_vw.MyApp;
import com.fax.faw_vw.MyFragment;
import com.fax.faw_vw.R;
import com.fax.faw_vw.fargment_common.AssetFrameTitleFragment;
import com.fax.faw_vw.model.ImageTextPagePair;
import com.fax.faw_vw.model.TextFramePagePair;
import com.fax.faw_vw.views.MyTopBar;
import com.fax.utils.frameAnim.AssetFrame;
import com.fax.utils.frameAnim.FrameFactory;
import com.fax.utils.view.list.ObjectXAdapter;
import com.fax.utils.view.list.ObjectXListView;

public class BrandServiceFragment extends MyFragment {
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final List<AssetFrame> btnFrames = FrameFactory.createFramesFromAsset(context, "car_used/btns", -1);
		final List<AssetFrame> pageFrames = FrameFactory.createFramesFromAsset(context, "car_used/pages", -1);
		final String[] btns = new String[]{
				"品牌介绍","置换服务","质量检测","质量保证","试乘试驾","车辆定制","车辆美容","保险服务"
		};
		final List<TextFramePagePair> pagePairs = new ArrayList<TextFramePagePair>();
		for(int i=0,length=btns.length;i<length;i++){
			AssetFrame btnFrame = btnFrames.get(i);
			Fragment pageFragment = AssetFrameTitleFragment.createInstance(pageFrames.get(i).getPath(), btns[i]);
			pagePairs.add(new TextFramePagePair(btns[i], btnFrame, pageFragment));
		}
		
		final ObjectXListView listView = new ObjectXListView(context);
		listView.setPullRefreshEnable(false);
		listView.setDrawSelectorOnTop(true);
		listView.setBackgroundColor(Color.WHITE);
		listView.setSelector(R.drawable.common_btn_in_white);
		listView.setDivider(getResources().getDrawable(android.R.color.darker_gray));

		int column = isPortrait()? 2:3;
		listView.setAdapter(new ObjectXAdapter.SingleLocalGridPageAdapter<TextFramePagePair>(column) {
			@Override
			public List<TextFramePagePair> instanceNewList() throws Exception {
				return pagePairs;
			}
			@Override
			protected View bindGridView(ViewGroup contain,final TextFramePagePair t,final int position, View convertView) {
				LinearLayout layout = (LinearLayout) convertView;
				if(convertView==null){
					int padding = (int) MyApp.convertToDp(20);
					TextView tv = new TextView(context);
					tv.setTextSize(15);
					tv.setCompoundDrawablePadding(padding/2);
					tv.setGravity(Gravity.CENTER);
					
					layout = new LinearLayout(context);
					layout.setMinimumHeight(listView.getHeight()/4);
					layout.setPadding(padding, padding, padding, padding);
					layout.setGravity(Gravity.CENTER);
					layout.addView(tv, -2, -2);
				}
				
				TextView tv = (TextView) layout.getChildAt(0);
				tv.setText(t.getText());
				tv.setCompoundDrawablesWithIntrinsicBounds(null, t.decodeDrawable(context), null, null);
				
				return layout;
			}
			@Override
			public void onItemClick(TextFramePagePair t, View view, int position, long id) {
				super.onItemClick(t, view, position, id);
				if(t.getFragment()==null) return;
				FragmentContain.start(getActivity(), t.getFragment());
			}
			@Override
			protected int getDividerHeight() {
				return 1;
			}
		});
	
		return new MyTopBar(context).setLeftBack().setTitle("品牌服务").setContentView(listView);
	}
}
