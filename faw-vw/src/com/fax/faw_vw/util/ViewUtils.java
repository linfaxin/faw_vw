package com.fax.faw_vw.util;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fax.utils.bitmap.BitmapManager;
import com.fax.utils.frameAnim.ScaleSizeDrawable;

import java.util.ArrayList;

/**
 * Created by linfaxin on 2014/8/13 013.
 * Email: linlinfaxin@163.com
 */
public class ViewUtils {
    public static final int ViewAnimDuration = 300;

    public static void expandByMarginTop(final View v, final Animation.AnimationListener l){
        if(v.getVisibility()==View.VISIBLE) return;
        v.setVisibility(View.VISIBLE);
        if(v.getMeasuredHeight()<=0) v.measure(View.MeasureSpec.makeMeasureSpec(-2, View.MeasureSpec.AT_MOST),
                View.MeasureSpec.makeMeasureSpec(-2, View.MeasureSpec.UNSPECIFIED));
        final ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
        params.topMargin = -v.getMeasuredHeight();
        Animation anim = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if(interpolatedTime == 1 && l !=null) l.onAnimationEnd(this);
                params.topMargin = - (int)(v.getHeight() * (1-interpolatedTime) );
                v.requestLayout();
            }
            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };
        anim.setDuration(ViewAnimDuration);
        v.startAnimation(anim);
    }
    public static void collapseByMarginTop(final View v, final Animation.AnimationListener l) {
        if(v.getVisibility()==View.GONE) return;
        final ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
        Animation anim = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    params.topMargin = 0;
                    v.setVisibility(View.GONE);
                    if(l !=null) l.onAnimationEnd(this);
                } else {
                    params.topMargin = - (int)(v.getHeight() * interpolatedTime);
                    v.requestLayout();
                }
            }
            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };
        anim.setDuration(ViewAnimDuration);
        v.startAnimation(anim);
    }
    public static void expandByHeight(final View v, final Animation.AnimationListener l){
        if(v.getVisibility()==View.VISIBLE) return;
        v.setVisibility(View.VISIBLE);
        if(v.getMeasuredHeight()<=0) v.measure(View.MeasureSpec.makeMeasureSpec(-2, View.MeasureSpec.AT_MOST),
                View.MeasureSpec.makeMeasureSpec(-2, View.MeasureSpec.UNSPECIFIED));
        final ViewGroup.LayoutParams params = v.getLayoutParams();
        final int height = v.getMeasuredHeight();
        final int defaultParamsHeight = params.height;//动画结束后恢复param的属性

        params.height = 0;
        v.requestLayout();
        Animation anim = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                params.height = (int)(height * interpolatedTime);
                if(interpolatedTime == 1){
                    if(l!=null) l.onAnimationEnd(this);
                    params.height = defaultParamsHeight;
                }

                v.requestLayout();
            }
            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };
        anim.setDuration(ViewAnimDuration);
        v.startAnimation(anim);
    }
    public static void collapseByHeight(final View v, final Animation.AnimationListener l) {
        if(v.getVisibility()==View.GONE) return;
//        v.setVisibility(View.VISIBLE);
        if(v.getMeasuredHeight()<=0) v.measure(View.MeasureSpec.makeMeasureSpec(-2, View.MeasureSpec.AT_MOST),
                View.MeasureSpec.makeMeasureSpec(-2, View.MeasureSpec.UNSPECIFIED));
        final ViewGroup.LayoutParams params = v.getLayoutParams();
        final int height = v.getMeasuredHeight();
        final int defaultParamsHeight = params.height;//动画结束后恢复param的属性

        Animation anim = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                params.height = (int)(height * (1-interpolatedTime));

                if(interpolatedTime == 1){
                    if(l!=null) l.onAnimationEnd(this);
                    params.height = defaultParamsHeight;
                }

                v.requestLayout();
            }
            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };
        anim.setDuration(ViewAnimDuration);
        v.startAnimation(anim);
    }


    public static abstract class AnimEndListener implements Animation.AnimationListener{
        @Override
        public void onAnimationStart(Animation animation) {
        }
        @Override
        public void onAnimationRepeat(Animation animation) {
        }
    }

    public static void setAllChildEnable(View view, boolean enable) {
        if (view == null) return;
        view.setEnabled(enable);

        if (view instanceof ViewGroup) {
            ViewGroup group = (ViewGroup) view;
            for (int i = 0, count = group.getChildCount(); i < count; i++) {
                setAllChildEnable(group.getChildAt(i), enable);
            }
        }
    }
    public static void setAllChildFocusEnable(View view, boolean enable) {
        if (view == null) return;
        view.setFocusable(enable);

        if (view instanceof ViewGroup) {
            ViewGroup group = (ViewGroup) view;
            for (int i = 0, count = group.getChildCount(); i < count; i++) {
                setAllChildFocusEnable(group.getChildAt(i), enable);
            }
        }
    }

    public static void setAllTextColor(View view, int color) {
        if(view == null) return;
        if(view instanceof TextView){
            ((TextView) view).setTextColor(color);

        }else if(view instanceof ViewGroup){
            ViewGroup group = (ViewGroup) view;
            for(int i=0,count=group.getChildCount(); i<count; i++){
                setAllTextColor(group.getChildAt(i), color);
            }
        }
    }
    public static float getFitPadScale(Context context){
    	DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float wDp = displayMetrics.widthPixels / displayMetrics.density;//屏幕宽度的dp
    	if(wDp>400){
    		//但是布局是为360dp宽的手机设计的，所以缩放TextView
    		float scale = wDp/360;
    		return scale;
    	}
    	return 1;
    }
    public static void fitAllTextToPadSize(View view) {
    	fitAllTextToPadSize(view, false);
    }
    public static void fitAllTextToPadSize(View view, boolean realScale) {
    	fitAllTextToPadSize(view, realScale, false);
    }
    public static void fitAllTextToPadSize(View view, boolean realScale, boolean withDrawables) {
        if(view == null) return;
		float scale = getFitPadScale(view.getContext());
		if(!realScale){
			//为了舒服的显示，只需放大一点就可以了
			scale = 1 + (scale-1)/4;
		}
		scaleAllTextSize(view, scale, withDrawables);
    }
    public static void scaleAllTextSize(View view, float scale, boolean withDrawables) {
        if(view == null) return;
        if(view instanceof TextView){
        	TextView textView = (TextView) view;
        	textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, textView.getTextSize()*scale);
        	if(withDrawables){
        		Drawable[] drawables = textView.getCompoundDrawables();
        		for(int i=0,length=drawables.length;i<length;i++){
        			if(drawables[i]!=null){
        				drawables[i] = new ScaleSizeDrawable(drawables[i], scale, scale);
        			}
        		}
        		textView.setCompoundDrawablesWithIntrinsicBounds(drawables[0], drawables[1], drawables[2], drawables[3]);
        	}

        }else if(view instanceof ViewGroup){
            ViewGroup group = (ViewGroup) view;
            for(int i=0,count=group.getChildCount(); i<count; i++){
            	scaleAllTextSize(group.getChildAt(i), scale, withDrawables);
            }
        }
    }
    public static void setViewDisableByAlpha(View view) {
        if(view == null) return;
        AlphaAnimation animation = new AlphaAnimation(.5f, .5f);
        animation.setDuration(0);
        animation.setFillAfter(true);
        view.startAnimation(animation);
    }
    public static void recycleAndRemoveAllView(ViewGroup group) {
        ArrayList<View> views = new ArrayList<View>();
        getChildrenDeep(group, views);
        group.removeAllViews();
        for(View child : views){
            if(child instanceof ImageView){
                try {
                    BitmapManager.getBitmapFromView(child).recycle();
                } catch (Exception ignore) {
                }
            }
        }
        System.gc();
    }

    public static void getChildrenDeep(View view, ArrayList<View> views){
        if(view == null) return;
        views.add(view);
        if(view instanceof ViewGroup){
            ViewGroup group = (ViewGroup) view;
            for(int i=0,count=group.getChildCount(); i<count; i++){
                getChildrenDeep(group.getChildAt(i), views);
            }
        }
    }

}
