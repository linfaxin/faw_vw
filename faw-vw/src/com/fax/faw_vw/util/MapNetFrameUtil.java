package com.fax.faw_vw.util;

import org.apache.commons.io.FilenameUtils;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.TextView;

import com.fax.faw_vw.MyApp;
import com.fax.utils.bitmap.BitmapManager;
import com.fax.utils.frameAnim.AssetFrame;

public class MapNetFrameUtil {
	public static String getUrl(AssetFrame frame, String dirAfterRes){
		String name = FilenameUtils.getName(frame.getPath());
		return MyApp.ResUrl + dirAfterRes + "/" + name;
	}
	public static String getCarDetailUrl(AssetFrame frame){
		return MyApp.ResUrl + frame.getPath().replaceFirst("car_detail/", "detail_android/").replace(" ", "%20");
	}
	public static void bindImg(View imgView, String url, final View progressBar, final TextView progressTv){
		progressBar.setVisibility(View.VISIBLE);
		progressTv.setVisibility(View.VISIBLE);
		BitmapManager.bindView(imgView, null, null, null, url, new BitmapManager.BitmapLoadingListener() {
			@Override
			public void onBitmapLoading(int progress) {
				progressTv.setText(""+progress);
			}
			@Override
			public void onBitmapLoadFinish(Bitmap bitmap, boolean isLoadSuccess) {
				progressBar.setVisibility(View.GONE);
				progressTv.setVisibility(View.GONE);
			}
		});
	}
}
