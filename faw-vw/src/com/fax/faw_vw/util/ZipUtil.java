package com.fax.faw_vw.util;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

import org.apache.commons.io.IOUtils;

public class ZipUtil {
    public static void unzip(File zipFile, String outputDirectory) throws Exception {
        unzip(new ZipFile(zipFile), new File(outputDirectory));
    }
    public static void unzip(File zipFile, File outputDirectory) throws Exception {
        unzip(new ZipFile(zipFile), outputDirectory);
    }

    /**
     * @param outputDirectory 解压到的目录
     */
    public static void unzip(InputStream inputStream, File outputDirectory) throws Exception {
        ZipInputStream in = new ZipInputStream(inputStream);
        // 获取ZipInputStream中的ZipEntry条目，一个zip文件中可能包含多个ZipEntry，
        // 当getNextEntry方法的返回值为null，则代表ZipInputStream中没有下一个ZipEntry，
        // 输入流读取完成；
        ZipEntry entry = in.getNextEntry();
        outputDirectory.mkdirs();
        while (entry != null) {
            if (entry.isDirectory()) {
                new File(outputDirectory + File.separator + entry.getName()).mkdirs();

            } else {
                File outFile = new File(outputDirectory + File.separator + entry.getName());
                outFile.getParentFile().mkdirs();

                FileOutputStream out = new FileOutputStream(outFile);
                BufferedOutputStream bos = new BufferedOutputStream(out);
                byte[] buffer = new byte[10 * 1024];
                int length;
                while ((length = in.read(buffer)) != -1) {
                    bos.write(buffer, 0, length);
                }
                bos.close();
                out.close();
            }
            // 读取下一个ZipEntry
            entry = in.getNextEntry();
        }
        in.close();
    }
    
    public static void unzip(ZipFile zipFile, File outputDirectory) throws Exception{
    	for(ZipEntry entry : Collections.list(zipFile.entries())){
    		File outFile = new File(outputDirectory, entry.getName());
    		if(outFile.length() == entry.getSize()) continue;
            outFile.getParentFile().mkdirs();
            
            FileOutputStream out = new FileOutputStream(outFile);
            IOUtils.copy(zipFile.getInputStream(entry), out);
    	}
    }
}
