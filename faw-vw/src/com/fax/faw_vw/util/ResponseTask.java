package com.fax.faw_vw.util;

import org.apache.http.client.methods.HttpRequestBase;

import android.content.Context;

import com.fax.faw_vw.model.Response;
import com.fax.utils.task.GsonAsyncTask;
import com.google.gson.Gson;

public abstract class ResponseTask<T extends Response> extends GsonAsyncTask<T> {

	public ResponseTask(Context context, HttpRequestBase request) {
		super(context, request);
	}

	public ResponseTask(Context context, String url) {
		super(context, url);
	}

	@Override
	protected T instanceObject(String json) throws Exception {
		Response temp = new Gson().fromJson(json, Response.class);
		if(!temp.isSuc()){
			setToast(null, temp.getMessage());
			return null;
		}
		return super.instanceObject(json);
	}

}
