package com.fax.faw_vw.util;

import com.fax.faw_vw.model.Response;
import com.fax.faw_vw.model.StringBodyResponse;
import com.google.gson.Gson;

import android.content.Context;

public class BodyWrapResponseTask<T extends Response> extends ResponseTask<T> {

	public BodyWrapResponseTask(Context context, String url) {
		super(context, url);
	}

	@Override
	protected void onPostExecuteSuc(T result) {
		
	}

	@Override
	protected T instanceObject(String json) throws Exception {
		StringBodyResponse temp = new Gson().fromJson(json, StringBodyResponse.class);
		if(!temp.isSuc()){
			setToast(null, temp.getMessage());
			return null;
		}
		return super.instanceObject(temp.getBody());
	}
}
