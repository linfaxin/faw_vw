package com.fax.faw_vw.fragment_more;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fax.faw_vw.FragmentContain;
import com.fax.faw_vw.MyApp;
import com.fax.faw_vw.MyFragment;
import com.fax.faw_vw.R;
import com.fax.faw_vw.model.FAQVideoList;
import com.fax.faw_vw.model.FAQVideoList.FAQVideo;
import com.fax.faw_vw.model.ImageTextPagePair;
import com.fax.faw_vw.model.ShowCarItem;
import com.fax.faw_vw.views.MyTopBar;
import com.fax.utils.view.list.ObjectXAdapter;
import com.fax.utils.view.list.ObjectXListView;

//快捷菜单
public class CarFAQFragment extends MyFragment {	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final ShowCarItem showCarItem = getSerializableExtra(ShowCarItem.class);
		
		final ObjectXListView listView = new ObjectXListView(context);
		listView.setPullRefreshEnable(false);
		int padding = (int) MyApp.convertToDp(10);
		listView.setDivider(new ColorDrawable(Color.TRANSPARENT));
		listView.setDividerHeight(padding);
		listView.setPadding(padding, padding, padding, padding);
		
		listView.setAdapter(new ObjectXAdapter.SingleLocalPageAdapter<FAQVideo>() {
			@Override
			public View bindView(final FAQVideo t, int position, View convertView) {
				TextView tv = (TextView) convertView;
				if(tv==null){
					tv = new TextView(context);
					int padding = (int) MyApp.convertToDp(12);
					tv.setPadding(padding, padding, padding, padding);
					tv.setBackgroundResource(R.drawable.common_border_btn_bg);
					tv.setGravity(Gravity.CENTER_VERTICAL);
				}
				
				tv.setText(t.getBt());
				return tv;
			}
			@Override
			public void onItemClick(FAQVideo t, View view, int position, long id) {
				super.onItemClick(t, view, position, id);
				FragmentContain.start(getActivity(), CarFAQDetailFragment.createInstance(t, showCarItem));
			}
			@Override
			public List<FAQVideo> instanceNewList() throws Exception {
				return Arrays.asList(FAQVideoList.getInstance().getFAQVideoByCar(showCarItem));
			}
		});
		return new MyTopBar(context).setLeftBack().setFitLand(true)
				.setTitle(showCarItem.getModel_cn()).setContentView(listView);
	}

}
