package com.fax.faw_vw.fragment_more;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.ValueCallback;
import android.widget.EditText;
import android.widget.Toast;

import com.fax.faw_vw.FragmentContain;
import com.fax.faw_vw.MyApp;
import com.fax.faw_vw.MyFragment;
import com.fax.faw_vw.R;
import com.fax.faw_vw.fargment_common.ChooseCityFragment;
import com.fax.faw_vw.model.QueryIllegalInfo;
import com.fax.faw_vw.views.MyTopBar;

/**违章查询页面 */
public class QueryIllegalFragment extends MyFragment {

	EditText plateNumber;
	EditText engineNumber;
	EditText vehicleIdNumber;
	EditText cityName;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		 View view = inflater.inflate(R.layout.more_query_illegal, container, false);
		MyTopBar topBar = (MyTopBar) new MyTopBar(context).setLeftBack().setFitLand(true)
				.setTitle("违章查询").setContentView(view);
		plateNumber=(EditText) view.findViewById(R.id.plateNumber);
		engineNumber=(EditText) view.findViewById(R.id.engineNumber);
		vehicleIdNumber=(EditText) view.findViewById(R.id.vehicleIdNumber);
		cityName=(EditText) view.findViewById(R.id.cityName);
		
		QueryIllegalInfo queryIllegalInfo = getSerializableExtra(QueryIllegalInfo.class);
		if(queryIllegalInfo!=null){
			plateNumber.setText(queryIllegalInfo.getPlateNumber());
			engineNumber.setText(queryIllegalInfo.getEngineNumber());
			vehicleIdNumber.setText(queryIllegalInfo.getVehicleIdNumber());
			cityName.setText(queryIllegalInfo.getCityName());
			view.findViewById(android.R.id.title).setVisibility(View.GONE);
		}
		view.findViewById(R.id.commit_button).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(TextUtils.isEmpty(plateNumber.getText())){
					Toast.makeText(context, "请输入您的车牌号！", Toast.LENGTH_SHORT).show();
					return;
				}
				if(TextUtils.isEmpty(engineNumber.getText())){
					Toast.makeText(context, "请输入您的发动机号！", Toast.LENGTH_SHORT).show();
					return;
				}
				if(TextUtils.isEmpty(vehicleIdNumber.getText())){
					Toast.makeText(context, "请输入您的车架号！", Toast.LENGTH_SHORT).show();
					return;
				}
				if(TextUtils.isEmpty(cityName.getText())){
					Toast.makeText(context, "请输入城市！", Toast.LENGTH_SHORT).show();
					return;
				}
				QueryIllegalIndexFragment.checkCityCode(context, cityName.getText().toString(), new ValueCallback<String>() {
					@Override
					public void onReceiveValue(String cityCode) {
						if(cityCode==null){
							Toast.makeText(context, "城市不正确或未开通", Toast.LENGTH_SHORT).show();
							return;
						}
						
						QueryIllegalInfo info=new QueryIllegalInfo();
						info.setPlateNumber(plateNumber.getText().toString().trim());
						info.setEngineNumber(engineNumber.getText().toString().trim());
						info.setVehicleIdNumber(vehicleIdNumber.getText().toString().trim());
						info.setCityName(cityName.getText().toString().trim());
						info.setSaveTime(System.currentTimeMillis());
						Intent data = MyApp.createIntent(info);
						
						backStack(data);
						
					}
				});
				
			}
		});
		cityName.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				FragmentContain.start(QueryIllegalFragment.this, ChooseCityFragment.class, Request_SwitchCity);
			}
		});
		//数据绑定、提交
		return topBar;
	}
    public final static int Request_SwitchCity = 1;
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode == Activity.RESULT_OK && requestCode == Request_SwitchCity){
			String city = data.getStringExtra(ChooseCityFragment.Extra_City);
			cityName.setText(city);
		}
	}
}
