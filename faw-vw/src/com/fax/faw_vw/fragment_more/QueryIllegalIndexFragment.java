package com.fax.faw_vw.fragment_more;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map.Entry;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.webkit.ValueCallback;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fax.faw_vw.FragmentContain;
import com.fax.faw_vw.MyApp;
import com.fax.faw_vw.MyFragment;
import com.fax.faw_vw.R;
import com.fax.faw_vw.model.IllegalCityResponse;
import com.fax.faw_vw.model.QueryIllegalInfo;
import com.fax.faw_vw.model.Illegal;
import com.fax.faw_vw.model.IllegalInfo;
import com.fax.faw_vw.util.ResponseTask;
import com.fax.faw_vw.views.MyTopBar;
import com.fax.utils.http.HttpUtils;
import com.fax.utils.task.ResultAsyncTask;
import com.fax.utils.view.list.ObjectXAdapter;
import com.fax.utils.view.list.ObjectXListView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/** 违章查询首页 */
public class QueryIllegalIndexFragment extends MyFragment {
	ObjectXListView listView;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View view = inflater.inflate(R.layout.more_query_illegal_index, container, false);
		
		listView = (ObjectXListView)view.findViewById(android.R.id.list);
		listView.setPullRefreshEnable(false);
		listView.setPullLoadEnable(false);
		listView.setOverScrollLoadEnable(false);
		listView.setAdapter(new ObjectXAdapter.SingleLocalPageAdapter<QueryIllegalInfo>() {
			@Override
			public View bindView(QueryIllegalInfo info, int position, View view) {
				if(view == null){
					view = View.inflate(context, R.layout.more_query_illegalindex_list_item, null);
				}
				LinearLayout nameLayout = (LinearLayout) view.findViewById(R.id.brand_query_illegal_layout);
				((TextView)nameLayout.findViewById(R.id.car_fullname)).setText(info.getPlateNumber());
				return view;
			}
			@Override
			public void onItemClick(final QueryIllegalInfo p, View view, int position, long id) {
				super.onItemClick(p, view, position, id);
				showQueryIllegalDetail(p);
			}
			@Override
			public List<QueryIllegalInfo> instanceNewList() throws Exception {
				ArrayList<QueryIllegalInfo> queryIllegalInfos = new ArrayList<QueryIllegalInfo>();
				Gson gson = new Gson();
				for(Entry<String, ?> entry : getQueryHistorySP().getAll().entrySet()){
					try {
						QueryIllegalInfo illegalInfo = gson.fromJson(String.valueOf(entry.getValue()),
								QueryIllegalInfo.class);
						illegalInfo.setSaveTime(Long.parseLong(entry.getKey()));
						queryIllegalInfos.add(illegalInfo);
					} catch (Exception e) {
					}
				}
				Collections.sort(queryIllegalInfos);
				return queryIllegalInfos;
			}
			@Override
			public void onLoadFinish(List<QueryIllegalInfo> allList) {
				super.onLoadFinish(allList);
				if(allList!=null && allList.size()>0){
					view.findViewById(android.R.id.title).setVisibility(View.VISIBLE);
				}
			}
		});
		
		  view.findViewById(R.id.add_button).setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						FragmentContain.start(QueryIllegalIndexFragment.this, QueryIllegalFragment.class, Request_AddQueryInfo);
					}
		   });
		// 数据绑定、提交
		return new MyTopBar(context).setLeftBack().setFitLand(true)
				.setTitle("违章查询").setContentView(view);
	}

	private static final int Request_AddQueryInfo = 1;
	private static final int Request_ModifyQueryInfo = 2;
	private static final int Request_QueryDetail = 3;
	public static final String Extra_IsDelete = "isDelete";
	public static final String Extra_IsModify = "isModify";
	private QueryIllegalInfo lastClickInfo;
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode == Activity.RESULT_OK){
			if (requestCode == Request_AddQueryInfo) {
				QueryIllegalInfo queryIllegalInfo = (QueryIllegalInfo) data.getSerializableExtra(QueryIllegalInfo.class.getName());
				getQueryHistorySP().edit().putString(queryIllegalInfo.getSaveTime()+"", new Gson().toJson(queryIllegalInfo)).apply();
				
			}
			
			if (requestCode == Request_ModifyQueryInfo) {
				if(lastClickInfo==null) return;
				String key = lastClickInfo.getSaveTime()+"";
				QueryIllegalInfo queryIllegalInfo = (QueryIllegalInfo) data.getSerializableExtra(QueryIllegalInfo.class.getName());
				getQueryHistorySP().edit().putString(key, new Gson().toJson(queryIllegalInfo)).apply();
				
			}
			
			if (requestCode == Request_QueryDetail) {
				if(lastClickInfo==null) return;
				String key = lastClickInfo.getSaveTime()+"";
				if(data.getBooleanExtra(Extra_IsDelete, false)){
					getQueryHistorySP().edit().remove(key).apply();
				}else{
					FragmentContain.start(QueryIllegalIndexFragment.this, QueryIllegalFragment.class, 
							MyApp.createIntent(lastClickInfo), Request_ModifyQueryInfo);
				}
				
			}
			
			listView.reload();
		}
	}

	private SharedPreferences getQueryHistorySP(){
		return context.getSharedPreferences("QueryIllegalList", Context.MODE_PRIVATE);
	}
	
	private static IllegalCityResponse illegalCityResponse;
	protected static void checkCityCode(final Context context, final String cityName,
			final ValueCallback<String> cityCodeCall){
		if(illegalCityResponse!=null){
			cityCodeCall.onReceiveValue(illegalCityResponse.getCityCode(cityName));
		}else{
			new ResponseTask<IllegalCityResponse>(context, 
					"http://v.juhe.cn/wz/citys?key=4f48c0df822f2bc8478b7781b10fd140&format=2") {
				@Override
				protected void onPostExecuteSuc(IllegalCityResponse result) {
					illegalCityResponse = result;
					cityCodeCall.onReceiveValue(illegalCityResponse.getCityCode(cityName));
				}
			}.setProgressDialog().execute();
		}
	}
	
	private void showQueryIllegalDetail(final QueryIllegalInfo queryIllegalInfo){
		checkCityCode(context, queryIllegalInfo.getCityName(), new ValueCallback<String>() {
			@Override
			public void onReceiveValue(final String cityCode) {
				if(cityCode==null){
					Toast.makeText(context, "城市错误或不支持", Toast.LENGTH_SHORT).show();
					return;
				}

				String IllegalUrl="http://v.juhe.cn/wz/query";
				ArrayList<NameValuePair> pairs=new ArrayList<NameValuePair>();
				pairs.add(new BasicNameValuePair("hphm", queryIllegalInfo.getPlateNumber()));
				pairs.add(new BasicNameValuePair("engineno", queryIllegalInfo.getEngineNumber()));
				pairs.add(new BasicNameValuePair("classno", queryIllegalInfo.getVehicleIdNumber()));
				pairs.add(new BasicNameValuePair("city", cityCode));
				pairs.add(new BasicNameValuePair("key", "4f48c0df822f2bc8478b7781b10fd140"));
			    String url = IllegalUrl + "?" + URLEncodedUtils.format(pairs, "UTF-8");
				new ResponseTask<IllegalInfo>(context, url) {
					@Override
					protected void onPostExecuteSuc(IllegalInfo illegal) {
						FragmentContain.start(QueryIllegalIndexFragment.this, QueryIllegalInfoFragment.class, 
								MyApp.createIntent(illegal), Request_QueryDetail);
						lastClickInfo = queryIllegalInfo;
					}
				}.setProgressDialog().execute();
			}
		});
	}
}
