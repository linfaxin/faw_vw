package com.fax.faw_vw.fragment_more;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fax.faw_vw.FragmentContain;
import com.fax.faw_vw.MyApp;
import com.fax.faw_vw.MyFragment;
import com.fax.faw_vw.R;
import com.fax.faw_vw.fargment_common.AssetFrameTitleFragment;
import com.fax.faw_vw.fargment_common.ResImageTitleFragment;
import com.fax.faw_vw.model.FAQVideoList;
import com.fax.faw_vw.model.ImageTextPagePair;
import com.fax.faw_vw.model.ShowCarItem;
import com.fax.faw_vw.model.TextFramePagePair;
import com.fax.faw_vw.model.FAQVideoList.FAQVideo;
import com.fax.faw_vw.model.FAQVideoList.FAQVideo.Option;
import com.fax.faw_vw.views.MyTopBar;
import com.fax.utils.frameAnim.AssetFrame;
import com.fax.utils.frameAnim.FrameFactory;
import com.fax.utils.frameAnim.ZipBitmapFrame;
import com.fax.utils.task.DownloadTask;
import com.fax.utils.task.ResultAsyncTask;
import com.fax.utils.view.MultiFormatTextView;
import com.fax.utils.view.list.ObjectXAdapter;
import com.fax.utils.view.list.ObjectXListView;

public class CarFAQDetailFragment extends MyFragment{
	public static CarFAQDetailFragment createInstance(FAQVideo fVideo, ShowCarItem showCarItem){
		return MyApp.createFragment(CarFAQDetailFragment.class, fVideo, showCarItem);
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//		final String question = getSerializableExtra(String.class);
		final ShowCarItem showCarItem = getSerializableExtra(ShowCarItem.class);
		final FAQVideo fVideo = getSerializableExtra(FAQVideo.class);
		
		final ObjectXListView listView = new ObjectXListView(context);
		listView.setPullRefreshEnable(false);
//		listView.setDrawSelectorOnTop(true);
//		listView.setBackgroundColor(Color.WHITE);
		listView.setSelector(R.drawable.common_btn_in_white);
		listView.setDivider(getResources().getDrawable(android.R.color.darker_gray));

		int column = isPortrait()? 1:2;
		listView.setAdapter(new ObjectXAdapter.SingleLocalGridPageAdapter<Option>(column) {
			@Override
			public List<Option> instanceNewList() throws Exception {
				return fVideo.getOption();
			}
			@Override
			protected View bindGridView(ViewGroup contain,final Option o,final int position, View convertView) {
				MultiFormatTextView tv = (MultiFormatTextView) convertView;
				if(convertView==null){
					int padding = (int) MyApp.convertToDp(10);
					tv = new MultiFormatTextView(context);
					tv.setTextSize(15);
					tv.setCompoundDrawablePadding(padding);
					tv.setPadding(padding, padding, padding, padding);
					tv.setGravity(Gravity.CENTER_VERTICAL|Gravity.LEFT);
				}
				String st = o.getSt();
				if(TextUtils.isEmpty(st) || "0".equals(st)){
					tv.setTextMulti(fVideo.getBt());
				}else{
					tv.setTextMulti(fVideo.getBt()+"\n//S12"+ o.getSt());
				}
				tv.setCompoundDrawablesWithIntrinsicBounds(o.getImage(fVideo).decodeDrawable(context, DisplayMetrics.DENSITY_XHIGH),
						null, null, null);
				return tv;
			}
			@Override
			public void onItemClick(Option o, View view, int position, long id) {
				final String resUrl = "http://faw-vw.allyes.com/res/ask/"+URLEncoder.encode(o.getMp4());
				if(MyApp.isWifiConnect()) openItem(resUrl);
				else{
					new ResultAsyncTask<Integer>(context) {
						@Override
						protected void onPostExecuteSuc(Integer result) {
							if(result!=null){
								String fileSizeMsg = result<=0 ? "" : "("+DownloadTask.sizeToString(result)+")";
								new AlertDialog.Builder(context).setTitle("下载中心")
								.setMessage("即将开始观看"+fVideo.getBt()+fileSizeMsg+"，建议您使用WIFI。土豪随意!^_^")
								.setNegativeButton(android.R.string.cancel, null)
								.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										openItem(resUrl);
									}
								}).show();
							}
						}
						@Override
						protected Integer doInBackground(Void... params) {
							try {
								return new URL(resUrl).openConnection().getContentLength();
							} catch (Exception e) {
								e.printStackTrace();
							}
							return 0;
						}
					}.setProgressDialog().execute();
				}
			}
			public void openItem(String resUrl) {
				Intent intent = new Intent(Intent.ACTION_VIEW).setDataAndType(Uri.parse(resUrl), "video/*");
		        startActivity(intent);
			}
			@Override
			protected int getDividerHeight() {
				return 1;
			}
		});
	
		return new MyTopBar(context).setLeftBack().setTitle(showCarItem.getModel_cn()).setContentView(listView);
	}
}
