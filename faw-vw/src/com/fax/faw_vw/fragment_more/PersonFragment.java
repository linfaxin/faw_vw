package com.fax.faw_vw.fragment_more;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fax.faw_vw.MyFragment;
import com.fax.faw_vw.fragment_person.PersonHomeFragment;
import com.fax.faw_vw.fragment_person.PersonLoginFragment;
import com.fax.faw_vw.model.LoginResponse;
import com.fax.faw_vw.views.MyTopBar;

/**个人中心 */
public class PersonFragment extends MyFragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if(LoginResponse.getCache()!=null){
			replaceFragment(new PersonHomeFragment());
		}else{
			replaceFragment(new PersonLoginFragment());
		}
		return new MyTopBar(context).setLeftBack().setTitle("个人中心");
	}

}
