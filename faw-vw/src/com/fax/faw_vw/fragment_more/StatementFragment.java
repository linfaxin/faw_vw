package com.fax.faw_vw.fragment_more;

import java.io.IOException;

import org.apache.commons.io.IOUtils;

import android.os.Bundle;
import android.text.method.MovementMethod;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.fax.faw_vw.MyApp;
import com.fax.faw_vw.MyFragment;
import com.fax.faw_vw.views.MyTopBar;

//免责声明
public class StatementFragment extends MyFragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		int padding = (int) MyApp.convertToDp(10);
		
		TextView textView = new TextView(context);
		textView.setTextSize(16);
		try {
			textView.setText(IOUtils.toString(getResources().getAssets().open("statement.txt")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		TextView endTv = new TextView(context);
		endTv.setTextSize(16);
		endTv.setGravity(Gravity.RIGHT);
		endTv.setText("一汽-大众汽车有限公司");
		endTv.setPadding(0, padding, 0, padding);
		
		ScrollView scrollView = new ScrollView(context);
		LinearLayout ll = new LinearLayout(context);
		ll.setOrientation(LinearLayout.VERTICAL);
		ll.setPadding(padding, padding, padding, padding);
		ll.addView(textView, -1, -2);
		ll.addView(endTv, -1, -2);
		scrollView.addView(ll);
		return new MyTopBar(context).setLeftBack().setTitle("免责声明").setContentView(scrollView);
	}

}
