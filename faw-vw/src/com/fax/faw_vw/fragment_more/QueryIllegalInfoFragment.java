package com.fax.faw_vw.fragment_more;


import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fax.faw_vw.MyFragment;
import com.fax.faw_vw.R;
import com.fax.faw_vw.model.IllegalInfo;
import com.fax.faw_vw.model.IllegalInfo.IllegalInfoList.IllegalInfoItem;
import com.fax.faw_vw.views.MyTopBar;
import com.fax.utils.view.list.ObjectXAdapter;
import com.fax.utils.view.list.ObjectXListView;

/**违章信息页面 */
public class QueryIllegalInfoFragment extends MyFragment {
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		 final IllegalInfo illegal = getSerializableExtra(IllegalInfo.class);
		 View view = View.inflate(context, R.layout.more_query_illegal_list_detail, null);
		 final ObjectXListView listView = (ObjectXListView) view.findViewById(android.R.id.list);
		 listView.setPullLoadEnable(false);
		 listView.setPullRefreshEnable(false);
		 listView.setAdapter(new ObjectXAdapter.SingleLocalPageAdapter<IllegalInfo.IllegalInfoList.IllegalInfoItem>() {
			@Override
			public View bindView(IllegalInfo.IllegalInfoList.IllegalInfoItem t, int position, View view) {
				 if(view==null){
					 view = View.inflate(context, R.layout.more_query_illegal_info, null);
				 }
				 ((TextView)view.findViewById(R.id.date)).setText(t.getDate());
				 ((TextView)view.findViewById(R.id.area)).setText(t.getArea());
				 ((TextView)view.findViewById(R.id.act)).setText(t.getAct());
				 ((TextView)view.findViewById(R.id.money)).setText(t.getMoney());
				return view;
			}
			@Override
			public List<IllegalInfo.IllegalInfoList.IllegalInfoItem> instanceNewList() throws Exception {
				try {
					return illegal.getIllegalInfoList().getLists();
				} catch (Exception e) {
				}
				return new ArrayList<IllegalInfo.IllegalInfoList.IllegalInfoItem>();
			}
			@Override
			public void onLoadFinish(List<IllegalInfoItem> allList) {
				super.onLoadFinish(allList);
				if(allList.size()==0){
					listView.setFootHint("无违章纪录");
				}
			}
		});
		 view.findViewById(R.id.modify_button).setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				backStack(new Intent().putExtra(QueryIllegalIndexFragment.Extra_IsModify, true));
			}
		});
		 return new MyTopBar(context).setLeftBack()
				 .setRightBtn("删除", new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						backStack(new Intent().putExtra(QueryIllegalIndexFragment.Extra_IsDelete, true));
					}
				})
				.setTitle("违章查询").setContentView(view);
	}
}
