package com.fax.faw_vw.fargment_common;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ScrollView;

import com.fax.faw_vw.MyApp;
import com.fax.faw_vw.MyFragment;
import com.fax.faw_vw.views.FitWidthImageView;
import com.fax.faw_vw.views.MyTopBar;
import com.fax.utils.frameAnim.AssetFrame;
import com.fax.utils.frameAnim.FrameAnimation;

public class ResImageTitleFragment extends MyFragment {
	public static ResImageTitleFragment createInstance(int res, String title){
		return (ResImageTitleFragment) MyApp.createFragment(ResImageTitleFragment.class, res, title);
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		int res = getSerializableExtra(Integer.class);
		String title = getSerializableExtra(String.class);
		ImageView imageView = new FitWidthImageView(context);
		imageView.setImageResource(res);
		
		ScrollView scrollView = new ScrollView(context);
		scrollView.addView(imageView, -1, -2);
		return new MyTopBar(context).setLeftBack().setTitle(title).setContentView(scrollView);
	}

}
