package com.fax.faw_vw.fargment_common;

import java.io.Serializable;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.MeasureSpec;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.fax.faw_vw.MyApp;
import com.fax.faw_vw.MyFragment;
import com.fax.faw_vw.views.FitWidthImageView;
import com.fax.faw_vw.views.MyTopBar;
import com.fax.utils.bitmap.BitmapManager;
import com.fax.utils.bitmap.ProgressImageView;
import com.fax.utils.frameAnim.AssetFrame;
import com.fax.utils.frameAnim.FrameAnimation;

public class NetImageTitleFragment extends MyFragment {
	public static NetImageTitleFragment createInstance(String url, String title){
		return (NetImageTitleFragment) MyApp.createFragment(NetImageTitleFragment.class, (Serializable)new String[]{url, title});
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		String[] pair = getSerializableExtra(String[].class);
		String url = pair[0];
		String title = pair[1];
		ImageView imageView = new FitWidthImageView(context);
		final ProgressBar progressBar = new ProgressBar(context);
		final TextView progressTv = new TextView(context);
		progressBar.setMax(100);
		BitmapManager.bindView(imageView,null, null, null, url, new BitmapManager.BitmapLoadingListener() {
			@Override
			public void onBitmapLoading(int progress) {
				progressTv.setText(progress+"");
			}
			@Override
			public void onBitmapLoadFinish(Bitmap bitmap, boolean isLoadSuccess) {
				progressBar.setVisibility(View.GONE);
				progressTv.setVisibility(View.GONE);
			}
		} );
		
		ScrollView scrollView = new ScrollView(context);
		scrollView.addView(imageView, -1, -2);
		FrameLayout frame = new FrameLayout(context);
		frame.addView(scrollView);
		frame.addView(progressBar, new FrameLayout.LayoutParams(-2, -2, Gravity.CENTER));
		frame.addView(progressTv, new FrameLayout.LayoutParams(-2, -2, Gravity.CENTER));
		return new MyTopBar(context).setLeftBack().setTitle(title).setContentView(frame);
	}

}
