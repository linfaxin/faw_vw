package com.fax.faw_vw.fargment_common;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fax.faw_vw.FragmentContain;
import com.fax.faw_vw.FragmentContainLandscape;
import com.fax.faw_vw.MyApp;
import com.fax.faw_vw.MyFragment;
import com.fax.faw_vw.R;
import com.fax.faw_vw.fragment_360.Show360FrameFragment;
import com.fax.faw_vw.fragments_car.CarDetailFragment;
import com.fax.faw_vw.fragments_car.CarDownloadFragment;
import com.fax.faw_vw.fragments_car.PersonalizedChooseCar;
import com.fax.faw_vw.model.ShowCarItem;
import com.fax.faw_vw.model.ShowCarItemRes.CarItemChild;
import com.fax.faw_vw.views.MyTopBar;
import com.fax.utils.view.list.ObjectXAdapter;
import com.fax.utils.view.list.ObjectXListView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
/**产品展示 页卡 */
public class ChooseCarsFragment extends MyFragment {
    /** 选择处理器 */
    public OnSelectItem onSelectItem;
    Toast waitToast;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final Context context = getActivity();
		final Class<? extends Fragment> gotoClass = (Class<? extends Fragment>) getSerializableExtra(Class.class);
		MyTopBar topbar = new MyTopBar(context);
		final ObjectXListView listView = new ObjectXListView(context);
		listView.setPullRefreshEnable(false);
		topbar.setLeftBack().setTitle("车型选择").setContentView(listView);
		String title = getSerializableExtra(String.class);
		if(!TextUtils.isEmpty(title)){
			topbar.setTitle(title);
		}
		int padding = (int) MyApp.convertToDp(8);
		listView.setPadding(padding, padding, padding, padding);
		int column = isPortrait()? 2:3;
		listView.setAdapter(new ObjectXAdapter.SingleLocalGridPageAdapter<ShowCarItem>(column) {
			int[] ImgRes = new int[]{
					R.drawable.showcar_list2_cc,
					R.drawable.showcar_list2_magotan,
					R.drawable.showcar_list2_new_sagitar,
					R.drawable.showcar_list2_sagitar,
					R.drawable.showcar_list2_golf,
					R.drawable.showcar_list2_bora,
					R.drawable.showcar_list2_jetta,	
			};
			@Override
			public View bindGridView(ViewGroup contain, ShowCarItem t, int position, View view) {
				if(view == null){
					view = View.inflate(context, R.layout.main_showcar_list_item, null);
					view.findViewById(R.id.brand_name_layout).setVisibility(View.GONE);
				}
				((ImageView)view.findViewById(R.id.brand_img)).setImageResource(ImgRes[position]);
				return view;
			}
			@Override
			public List<ShowCarItem> instanceNewList() throws Exception {
				return new ArrayList<ShowCarItem>(Arrays.asList(ShowCarItem.SHOW_CAR_ITEMS_MAIN));
			}
			@Override
			public void onItemClick(ShowCarItem t, View view, int position, long id) {
				super.onItemClick(t, view, position, id);
				if (gotoClass != null) {//有向前进的Fragment
					if(t.equals(ShowCarItem.SHOW_CAR_ITEM_SAGITAR_NEW) && gotoClass != CarDownloadFragment.class){
						int[] xy = new int[2];
						view.getLocationInWindow(xy);
						if(waitToast!=null) waitToast.cancel();
						waitToast = Toast.makeText(context, "敬请期待", Toast.LENGTH_SHORT);
						waitToast.setGravity(Gravity.TOP, 0, xy[1]-view.getHeight()/2);
						waitToast.show();
						return;
					}
					if(gotoClass == Show360FrameFragment.class){
						FragmentContainLandscape.start(getActivity(), MyApp.createFragment(gotoClass, t));
					}else FragmentContain.start(getActivity(), MyApp.createFragment(gotoClass, t));
					
				}else if (onSelectItem != null) {
                    backStack();
                    onSelectItem.onSelectCar(t);
                    
                } else {
                    FragmentContain.start(getActivity(), MyApp.createFragment(CarDetailFragment.class, t));
                }
			}
		});

		return topbar;
	}
    @Override
	public void onDetach() {
		super.onDetach();
		if(waitToast!=null){
			waitToast.cancel();
		}
	}
	public interface OnSelectItem {
        public void onSelectCar(ShowCarItem item);
    }
}
