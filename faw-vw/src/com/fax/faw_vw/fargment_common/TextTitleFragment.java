package com.fax.faw_vw.fargment_common;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.fax.faw_vw.MyApp;
import com.fax.faw_vw.MyFragment;
import com.fax.faw_vw.views.FitWidthImageView;
import com.fax.faw_vw.views.MyTopBar;
import com.fax.utils.frameAnim.AssetFrame;
import com.fax.utils.frameAnim.FrameAnimation;

public class TextTitleFragment extends MyFragment {
	public static TextTitleFragment createInstance(String content, String title){
		return (TextTitleFragment) MyApp.createFragment(TextTitleFragment.class, 0,
				new String[]{content, title});
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		String[] pathAndTitle = getSerializableExtra(String[].class);
		String content = pathAndTitle[0];
		String title = pathAndTitle[1];
		TextView tv = new TextView(context);
		tv.setTextSize(16);
		tv.setText(content);
		int padding = (int) MyApp.convertToDp(12);
		tv.setPadding(padding, padding, padding, padding);
		
		ScrollView scrollView = new ScrollView(context);
		scrollView.addView(tv, -1, -2);
		return new MyTopBar(context).setLeftBack().setTitle(title).setContentView(scrollView);
	}

}
