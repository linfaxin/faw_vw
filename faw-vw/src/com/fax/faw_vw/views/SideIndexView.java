package com.fax.faw_vw.views;

import com.fax.faw_vw.R;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;


/**
 * Created by linfaxin on 2014/8/25 025.
 * Email: linlinfaxin@163.com
 */
public class SideIndexView extends View {

    public SideIndexView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SideIndexView(Context context) {
        super(context);
        init();
    }

    public SideIndexView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }
    private void init(){
        bgColor = getContext().getResources().getColor(R.color.alpha_black);
        textColor = getContext().getResources().getColor(android.R.color.darker_gray);
        textSize = 12 * getContext().getResources().getDisplayMetrics().density;
    }
    public void setShowingIndexArray(String... showingIndexArray){
        this.showingIndexArray = showingIndexArray;
    }
    public void setIndexArray(String... indexArray){
        this.indexArray = indexArray;
        try {
            invalidate();
        } catch (Exception ignore) {
            postInvalidate();
        }
    }

    OnIndexChangedListener mListener;
    String[] showingIndexArray = new String[]{"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z",};
    String[] indexArray;
    int choosedShowingIndex = -1;
    Paint paint = new Paint();
    int bgColor;
    int textColor = Color.DKGRAY;
    float textSize = 12;

    boolean inChoose = false;


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

//        if (inChoose) {//触控时有背景
//            canvas.drawColor(bgColor);
//        }

        int height = getHeight() - getPaddingTop() - getPaddingBottom();
        int width = getWidth();
        int singleHeight = height / showingIndexArray.length;
        //循环绘制字母
        for (int i = 0; i < showingIndexArray.length; i++) {
            paint.setTextSize(textSize);
            paint.setColor(textColor);
//            paint.setTypeface(Typeface.DEFAULT_BOLD);

            paint.setAntiAlias(true);
            if (i == choosedShowingIndex && inChoose) {
                //被选中了改变文字颜色
                paint.setColor(Color.parseColor("#333333"));
//                paint.setFakeBoldText(true);
            }
            float xPos = width / 2 - paint.measureText(showingIndexArray[i]) / 2;
            float yPos = singleHeight * i + singleHeight + getPaddingTop();

            canvas.drawText(showingIndexArray[i], xPos, yPos, paint);
            paint.reset();
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        final int action = event.getAction();
        final float y = event.getY();
        //计算出第几个
        int c = (int) ((y-getPaddingTop()) / (getHeight()-getPaddingTop()-getPaddingBottom()) * showingIndexArray.length);
        switch (action) {
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_MOVE:
                inChoose = true;
                chooseIndex(c);
                break;
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
                inChoose = false;
                if(mListener != null) mListener.onChooseCancel();
                invalidate();
                break;
        }
        return true;
    }
    private void chooseIndex(int showingIndex){
        if (choosedShowingIndex != showingIndex && showingIndex >= 0 && showingIndex < showingIndexArray.length) {
        	if(indexArray==null){
        		indexArray = showingIndexArray;
        	}
            int chooseIndex = -1;
            String showingWord = showingIndexArray[showingIndex];
            for(int i=0,length=indexArray.length;i<length;i++){
                if(indexArray[i].compareTo(showingWord)>=0){
                    chooseIndex = i;
                    break;
                }
            }
            //回调
            choosedShowingIndex = showingIndex;
            invalidate();
            if(mListener != null && chooseIndex>=0 && chooseIndex<indexArray.length)
                mListener.onChooseIndexChanged(chooseIndex, indexArray[chooseIndex], showingWord);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return super.onTouchEvent(event);
    }

    //方便Activity类调用，用于显示
    public void setOnIndexChangedListener(OnIndexChangedListener mListener) {
        this.mListener = mListener;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int dWidth = (int) (textSize + 16 * getContext().getResources().getDisplayMetrics().density);
        int vWidth = dWidth + getPaddingLeft() + getPaddingRight();
        setMeasuredDimension(vWidth, getDefaultSize(getSuggestedMinimumHeight(), heightMeasureSpec));
    }
    public interface OnIndexChangedListener {
        public void onChooseIndexChanged(int position, String chooseWord, String showingWord);
        public void onChooseCancel();
    }
}
