package com.fax.faw_vw.views.drawableclose;


import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ImageSpan;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.RadioButton;
import android.widget.TextView;

/**
 * Created by linfaxin on 2014/7/26 026.
 * Email: linlinfaxin@163.com
 */
public class DrawableCloseRadioBtn extends RadioButton{
    public DrawableCloseRadioBtn(Context context) {
        super(context);
    }

    public DrawableCloseRadioBtn(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DrawableCloseRadioBtn(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    Rect textBound = new Rect();
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        String text = getText().toString();
        getPaint().getTextBounds(text, 0, text.length(), textBound);
        //dr.mDrawableLeft, dr.mDrawableTop, dr.mDrawableRight, dr.mDrawableBottom
        Drawable[] drawables = getCompoundDrawables();
        Drawable drawableLeft = drawables[0];
        if(drawableLeft!=null){
            int padLeft = (getMeasuredWidth() - textBound.width())/2 - drawableLeft.getIntrinsicWidth() - getCompoundDrawablePadding();
            setPadding(padLeft , getPaddingTop(), padLeft + drawableLeft.getIntrinsicWidth(), getPaddingBottom());
        }

        Drawable drawableRight = drawables[2];
        if(drawableRight!=null){
            int padRight = (getMeasuredWidth() - textBound.width())/2 - drawableRight.getIntrinsicWidth() - getCompoundDrawablePadding();
            setPadding(padRight + drawableRight.getIntrinsicWidth() , getPaddingTop(), padRight, getPaddingBottom());
        }
    }
}
