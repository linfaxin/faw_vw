package com.fax.faw_vw.views.drawableclose;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ImageSpan;

/**
 * Created by linfaxin on 2014/9/5 005.
 * Email: linlinfaxin@163.com
 */
public class DrawableClose {
    public interface DrawableCloseView{
        public Drawable[] getCompoundDrawables();
        public int[] getDrawableState();

        public CharSequence getText();

        public int getCompoundDrawablePadding();

        public void setCompoundDrawables(Drawable o, Drawable drawable, Drawable o1, Drawable drawable1);

        public void setText(CharSequence c);
    }

    Drawable drawableLeft;
    Drawable drawableRight;
    DrawableCloseView view;
    public DrawableClose(DrawableCloseView view){
        this.view = view;
        init();
    }

    protected void drawableStateChanged() {
        int[] myDrawableState = view.getDrawableState();
        if (drawableLeft != null) {
            // Set the state of the Drawable
            drawableLeft.setState(myDrawableState);
        }
        if (drawableRight != null) {
            // Set the state of the Drawable
            drawableRight.setState(myDrawableState);
        }
    }

    private void init(){
        //dr.mDrawableLeft, dr.mDrawableTop, dr.mDrawableRight, dr.mDrawableBottom
        Drawable[] drawables = view.getCompoundDrawables();
        drawableLeft = drawables[0];
        drawableRight = drawables[2];
        SpannableString spannable = new SpannableString(" "+view.getText()+" ");

        if(drawableLeft!=null){
            int pad = view.getCompoundDrawablePadding();
            drawableLeft.setBounds(-pad, 0, drawableLeft.getIntrinsicWidth()-pad, drawableLeft.getIntrinsicHeight());
            spannable.setSpan(new CenterImageSpan(drawableLeft, ImageSpan.ALIGN_BASELINE), 0, 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        if(drawableRight!=null){
            int pad = view.getCompoundDrawablePadding();
            drawableRight.setBounds(pad, 0, drawableRight.getIntrinsicWidth()+pad, drawableRight.getIntrinsicHeight());
            spannable.setSpan(new CenterImageSpan(drawableRight, ImageSpan.ALIGN_BASELINE), spannable.length()-1, spannable.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }

        view.setCompoundDrawables(null, drawables[1], null, drawables[3]);
        view.setText(spannable);
    }

    public static class CenterImageSpan extends ImageSpan {

        public CenterImageSpan(Drawable d) {
            super(d);
        }

        public CenterImageSpan(Drawable d, int verticalAlignment) {
            super(d, verticalAlignment);
        }
        @Override
        public void draw(Canvas canvas, CharSequence text,
                         int start, int end, float x,
                         int top, int y, int bottom, Paint paint) {
            Drawable b = getDrawable();
            canvas.save();

            int transY = bottom - b.getBounds().centerY() - paint.getFontMetricsInt(null)/2;

            canvas.translate(x, transY);
            b.draw(canvas);
            canvas.restore();
        }
    }
}
