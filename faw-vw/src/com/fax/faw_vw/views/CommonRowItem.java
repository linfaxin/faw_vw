package com.fax.faw_vw.views;

import com.fax.faw_vw.R;
import com.fax.utils.view.MultiFormatTextView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class CommonRowItem extends LinearLayout {
	@SuppressLint("NewApi")
	public CommonRowItem(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(attrs);
	}

	public CommonRowItem(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(attrs);
	}

	public CommonRowItem(Context context) {
		super(context);
		init(null);
	}
	
	private void init(AttributeSet attrs){
		if(attrs!=null){
			TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.CommonRowItem);
			int layoutRes = typedArray.getResourceId(R.styleable.CommonRowItem_layout, R.layout.common_row_item);
			String title = typedArray.getString(R.styleable.CommonRowItem_title);
			String summary = typedArray.getString(R.styleable.CommonRowItem_summary);
			String content = typedArray.getString(R.styleable.CommonRowItem_content);
			boolean hasRightArrow = typedArray.getBoolean(R.styleable.CommonRowItem_rightArrow, false);
			typedArray.recycle();
			
			LayoutInflater.from(getContext()).inflate(layoutRes, this);
			setTitle(title);
			setSummary(summary);
			setContent(content);
			setHasRightArrow(hasRightArrow);
		}else{
			LayoutInflater.from(getContext()).inflate(R.layout.common_row_item, this);
		}
	}
	public void setLayoutRes(int layoutRes) {
		String title = getTitle();
		String summary = getSummary();
		String content = getContent();
		boolean hasRightArrow = isHasRightDrawable();
		LayoutInflater.from(getContext()).inflate(layoutRes, this);
		setTitle(title);
		setSummary(summary);
		setContent(content);
		setHasRightArrow(hasRightArrow);
	}

	public String getTitle() {
		return ((TextView)findViewById(android.R.id.title)).getText().toString();
	}

	public void setTitle(String title) {
		((TextView)findViewById(android.R.id.title)).setText(title);
	}

	public String getSummary() {
		return ((TextView)findViewById(android.R.id.summary)).getText().toString();
	}

	public void setSummary(String summary) {
		if(TextUtils.isEmpty(summary)){
			findViewById(android.R.id.summary).setVisibility(View.GONE);
		}else{
			((TextView)findViewById(android.R.id.summary)).setText(summary);
		}
	}

	public String getContent() {
		return ((TextView)findViewById(android.R.id.content)).getText().toString();
	}

	public void setContent(String content) {
		if(TextUtils.isEmpty(content)) return;
		((MultiFormatTextView)findViewById(android.R.id.content)).setTextMulti(content);
	}
	public void setContentSize(int sizeInSp){
		((TextView)findViewById(android.R.id.content)).setTextSize(12);
	}

	public boolean isHasRightDrawable() {
		TextView contentTv = (TextView)findViewById(android.R.id.content);
		return contentTv.getCompoundDrawables()[2]!=null;
	}

	public void setHasRightArrow(boolean hasRightArrow) {
		TextView contentTv = (TextView)findViewById(android.R.id.content);
		int res = hasRightArrow ? R.drawable.common_ic_arrow_right_2 : 0;
		contentTv.setCompoundDrawablesWithIntrinsicBounds(0, 0, res, 0);	
	}
	
}
