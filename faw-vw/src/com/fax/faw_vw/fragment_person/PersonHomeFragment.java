package com.fax.faw_vw.fragment_person;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fax.faw_vw.MainActivity;
import com.fax.faw_vw.MyFragment;
import com.fax.faw_vw.R;
import com.fax.faw_vw.model.LoginResponse;
import com.fax.faw_vw.views.AppDialogBuilder;
import com.fax.faw_vw.views.MyTopBar;

public class PersonHomeFragment extends MyFragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View view = View.inflate(context, R.layout.more_person_home, null);
		LoginResponse loginInfo = LoginResponse.getCache();
		((TextView)view.findViewById(R.id.name_text)).setText(loginInfo.getBody().getUserName());
		((TextView)view.findViewById(R.id.person_home_care_car_text)).setText(loginInfo.getBody().getWillBrandTitle());
		((TextView)view.findViewById(R.id.person_home_driver_info_text)).setText(loginInfo.getBody().getVin());

		view.findViewById(R.id.person_home_goto_driver_verify).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				//车主认证
				addFragment(new PersonDriverVerifyFragment());
			}
		});
		String vin = loginInfo.getBody().getVin();
		if(TextUtils.isEmpty(vin)){
			((View)view.findViewById(R.id.person_home_driver_info_text).getParent()).setVisibility(View.GONE);
		}else{
			view.findViewById(R.id.person_home_goto_driver_verify).setVisibility(View.GONE);
		}
		
		view.findViewById(R.id.person_home_goto_edit).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				//基本信息
				addFragment(new PersonEditInfoFragment());
			}
		});
		view.findViewById(R.id.person_home_exit).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				//退出
				new AppDialogBuilder(context).setTitle("提示").setMessage("确定要退出登入?")
					.setNegativeButton(null).setPositiveButton(new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							LoginResponse.clearCache();
							startActivity(new Intent(context, MainActivity.class));
						}
					}).show();
			}
		});
		return new MyTopBar(context).setLeftBack().setTitle("个人中心").setContentView(view);
	}

}
