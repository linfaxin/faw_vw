package com.fax.faw_vw.fragment_person;

import java.net.URLEncoder;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fax.faw_vw.MyFragment;
import com.fax.faw_vw.R;
import com.fax.faw_vw.model.LoginResponse;
import com.fax.faw_vw.util.ResponseTask;
import com.fax.faw_vw.views.AppDialogBuilder;
import com.fax.faw_vw.views.MyTopBar;

public class PersonLoginFragment extends MyFragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View view = View.inflate(context, R.layout.more_person_login, null);
		
		view.findViewById(R.id.commit_button).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				final String name = ((TextView)view.findViewById(R.id.name_text)).getText().toString();
				final String passwd = ((TextView)view.findViewById(R.id.passwd_text)).getText().toString();
				String url = "http://faw-vw.allyes.com/index.php?g=api&m=user&a=login"
						+ "&username=" + URLEncoder.encode(name)
						+ "&password=" + passwd;
				new ResponseTask<LoginResponse>(context, url) {
					@Override
					protected void onPostExecuteSuc(LoginResponse response) {
						response.saveCache();
						replaceFragment(new PersonHomeFragment());
						new AppDialogBuilder(context).setTitle("登入成功")
							.setPositiveButton(null).show();
					}
				}.setProgressDialog().execute();
			}
		});
		view.findViewById(R.id.login_forget_passwd).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				addFragment(new PersonPasswordForgetFragment());
			}
		});
		view.findViewById(R.id.login_forget_reg).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				addFragment(new PersonRegFragment());
			}
		});
		return new MyTopBar(context).setLeftBack().setTitle("个人中心").setContentView(view);
	}

}
