package com.fax.faw_vw.fragment_person;

import java.net.URLEncoder;
import java.util.ArrayList;

import org.apache.commons.io.IOUtils;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.fax.faw_vw.MyApp;
import com.fax.faw_vw.MyFragment;
import com.fax.faw_vw.R;
import com.fax.faw_vw.fragments_main.ShowCarsFragment;
import com.fax.faw_vw.model.CityInfo;
import com.fax.faw_vw.model.CityInfoResponse;
import com.fax.faw_vw.model.LoginResponse;
import com.fax.faw_vw.model.ProvinceList;
import com.fax.faw_vw.model.Response;
import com.fax.faw_vw.model.ShowCarItem;
import com.fax.faw_vw.model.ProvinceList.Province;
import com.fax.faw_vw.util.BodyWrapResponseTask;
import com.fax.faw_vw.util.ResponseTask;
import com.fax.faw_vw.views.FirstHideSpinnerAdapter;
import com.fax.faw_vw.views.MySpinnerAdapter;
import com.fax.faw_vw.views.MyTopBar;
import com.fax.utils.task.HttpAsyncTask;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class PersonEditInfoFragment extends MyFragment {

	Spinner provinceSpinner;
	Spinner citySpinner;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View view = View.inflate(context, R.layout.more_person_edit_info, null);
		final LoginResponse loginResponse = LoginResponse.getCache();
		((TextView)view.findViewById(R.id.name_text)).setText(loginResponse.getBody().getUserName());
		((TextView)view.findViewById(R.id.phone_text)).setText(loginResponse.getBody().getMobile());
		((TextView)view.findViewById(R.id.address_text)).setText(loginResponse.getBody().getAddress());
		((TextView)view.findViewById(R.id.email_text)).setText(loginResponse.getBody().getEmail());
//		((TextView)view.findViewById(R.id.provinces_text)).setText(loginResponse.getBody().get);
//		((TextView)view.findViewById(R.id.city_text)).setText(loginResponse.getBody().get);
		provinceSpinner = (Spinner) view.findViewById(R.id.province_spinner);
		citySpinner = (Spinner) view.findViewById(R.id.city_spinner);
		Spinner buyCarPlanSpinner = ((Spinner) view.findViewById(R.id.person_edit_buy_plan_btn));
		buyCarPlanSpinner.setAdapter(new MySpinnerAdapter<String>(new String[] { "否","是" }) {
			@Override
			public TextView getView(int position, View convertView, ViewGroup parent) {
				TextView tv = super.getView(position, convertView, parent);
				tv.setGravity(Gravity.RIGHT);
				return tv;
			}
		});
		buyCarPlanSpinner.setSelection(loginResponse.getBody().getBuyPlan());
		initProvinceCitySpinner(view, loginResponse.getBody().getProvinceID(), loginResponse.getBody().getCityID());
		view.findViewById(R.id.person_edit_star_car).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(final View v) {
				ShowCarsFragment showCarsFragment = new ShowCarsFragment();
				showCarsFragment.onSelectItem = new ShowCarsFragment.OnSelectItem() {
					@Override
					public void onSelectCar(ShowCarItem item) {
						showCarItem = item;
						((TextView)v).setText(item.getModel_cn());
					}
				};
				addFragment(showCarsFragment);
			}
		});
		String willBrandId = loginResponse.getBody().getWillBrandID();
		if(!TextUtils.isEmpty(willBrandId)){
			for(ShowCarItem item : ShowCarItem.SHOW_CAR_ITEMS_MAIN){
				if(willBrandId.equalsIgnoreCase(item.getId())){
					showCarItem = item;
					((TextView)view.findViewById(R.id.person_edit_star_car)).setText(showCarItem.getModel_cn());
					break;
				}
			}
		}
		
		view.findViewById(R.id.commit_button).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				final String name = ((TextView)view.findViewById(R.id.name_text)).getText().toString();
				final String mobile = ((TextView)view.findViewById(R.id.phone_text)).getText().toString();
				final String email = ((TextView)view.findViewById(R.id.email_text)).getText().toString();
				final Province province = (Province)provinceSpinner.getSelectedItem();
				final CityInfo cityInfo = (CityInfo)citySpinner.getSelectedItem();
				final String address = ((TextView)view.findViewById(R.id.address_text)).getText().toString();
				final int buyPlan = ((Spinner) view.findViewById(R.id.person_edit_buy_plan_btn)).getSelectedItemPosition();
				final String url = "http://faw-vw.allyes.com/index.php?g=api&m=user&a=update"
						+ "&nicename=" + URLEncoder.encode(name)
						+ "&mobile=" + URLEncoder.encode(mobile)
						+ "&sex=1"
						+ "&email=" + URLEncoder.encode(email)
						+ "&code=" + loginResponse.getBody().getLoginCode()
						+ (province!=null?"&provinceid=" + province.getAreaCode():"")
						+ (province!=null?"&provincetitle=" + URLEncoder.encode(province.getName()):"")
						+ (cityInfo!=null?"&cityid=" + cityInfo.getAreaCode():"")
						+ (cityInfo!=null?"&citytitle=" + URLEncoder.encode(cityInfo.getAreaName()):"")
						+ "&address=" + URLEncoder.encode(address)
						+ "&buyplan=" + buyPlan
						+ (showCarItem!=null?"&willbrandid=" + showCarItem.getId():"")
						+ (showCarItem!=null?"&willbrandtitle=" + URLEncoder.encode(showCarItem.getModel_cn()):"");
				new ResponseTask<Response>(context, url) {
					@Override
					protected void onPostExecuteSuc(Response result) {
						Toast.makeText(context, "保存成功", Toast.LENGTH_SHORT).show();
						loginResponse.getBody().setNiceName(name);
						loginResponse.getBody().setMobile(mobile);
						loginResponse.getBody().setEmail(email);
						if (province!=null) {
							loginResponse.getBody().setProvinceID(province.getAreaCode());
							loginResponse.getBody().setProvinceTitle(province.getName());
						}
						if (cityInfo!=null) {
							loginResponse.getBody().setCityID(cityInfo.getAreaCode());
							loginResponse.getBody().setCityTitle(cityInfo.getAreaName());
						}
						loginResponse.getBody().setAddress(address);
						loginResponse.getBody().setBuyPlan(buyPlan+"");
						if(showCarItem!=null){
							loginResponse.getBody().setWillBrandID(showCarItem.getId());
							loginResponse.getBody().setWillBrandTitle(showCarItem.getModel_cn());
						}
						loginResponse.saveCache();
						getActivity().finish();
						startFragment(new PersonHomeFragment());
					}
				}.setProgressDialog().execute();
			}
		});
		return new MyTopBar(context).setLeftBack().setTitle("个人资料").setContentView(view);
	}
	ShowCarItem showCarItem;
	
	private FirstHideSpinnerAdapter emptyAdapter = new FirstHideSpinnerAdapter(new String[]{null}){
		@Override
		public TextView getView(int position, View convertView, ViewGroup parent) {
			TextView tv = super.getView(position, convertView, parent);
			tv.setGravity(Gravity.RIGHT|Gravity.CENTER_VERTICAL);
			return tv;
		}
	};
	private void initProvinceCitySpinner(View view, String provinceID, final String cityId) {
		try {
			String json = IOUtils.toString(getActivity().getAssets().open("cityConfig.json"));
			ProvinceList provinceList = new Gson().fromJson(json, ProvinceList.class);
			provinceSpinner = (Spinner) view.findViewById(R.id.province_spinner);
			citySpinner = (Spinner) view.findViewById(R.id.city_spinner);
			//初始化citySpinner的状态
			citySpinner.setAdapter(emptyAdapter);
			citySpinner.setClickable(false);
			
			provinceSpinner.setAdapter(new FirstHideSpinnerAdapter(provinceList.getData()){
				@Override
				public TextView getView(int position, View convertView, ViewGroup parent) {
					TextView tv = super.getView(position, convertView, parent);
					tv.setGravity(Gravity.RIGHT);
					return tv;
				}
			});
			provinceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
				@Override
				public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
					final Province province = (Province)provinceSpinner.getItemAtPosition(position);
					if(province == null) return;
					//开始异步加载城市列表
					citySpinner.setOnItemSelectedListener(null);
					citySpinner.setAdapter(emptyAdapter);
					new BodyWrapResponseTask<CityInfoResponse>(context, MyApp.getCityListUrl(province)) {
						@Override
						protected void onPostExecuteSuc(CityInfoResponse response) {
							ArrayList<CityInfo> result = response.getBody();
							citySpinner.setClickable(true);
							citySpinner.setAdapter(new MySpinnerAdapter<CityInfo>(result){
								@Override
								public TextView getView(int position, View convertView, ViewGroup parent) {
									TextView tv = super.getView(position, convertView, parent);
									tv.setGravity(Gravity.RIGHT);
									return tv;
								}
							});
							if(firstChange){
								firstChange = false;
								if(!TextUtils.isEmpty(cityId)){
									for(int i=0,size=result.size();i<size;i++){
										CityInfo cityInfo = result.get(i);
										if(cityInfo.getAreaCode().equals(cityId)){
											citySpinner.setSelection(i);
										}
									}
								}
							}
						}
					}.setProgressDialog().execute();
				}
				boolean firstChange = true;
				@Override
				public void onNothingSelected(AdapterView<?> parent) {
				}
			});
			if(!TextUtils.isEmpty(provinceID)){
				for(int i=0,size=provinceList.getData().size();i<size;i++){
					Province province = provinceList.getData().get(i);
					if(province.getAreaCode().equals(provinceID)){
						provinceSpinner.setSelection(i+1);
					}
				}
			}
		} catch (Exception e) {
		}
	}
}
