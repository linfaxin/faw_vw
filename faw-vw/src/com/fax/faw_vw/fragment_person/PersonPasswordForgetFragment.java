package com.fax.faw_vw.fragment_person;

import java.net.URLEncoder;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fax.faw_vw.MyFragment;
import com.fax.faw_vw.R;
import com.fax.faw_vw.model.Response;
import com.fax.faw_vw.util.ResponseTask;
import com.fax.faw_vw.views.AppDialogBuilder;
import com.fax.faw_vw.views.MyTopBar;

public class PersonPasswordForgetFragment extends MyFragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View view = View.inflate(context, R.layout.more_person_forget, null);
		view.findViewById(R.id.commit_button).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				final String name = ((TextView)view.findViewById(R.id.name_text)).getText().toString();
				String url = "http://faw-vw.allyes.com/index.php?g=api&m=user&a=resetpwd&username="
						+ URLEncoder.encode(name);
				new ResponseTask<Response>(context, url) {
					@Override
					protected void onPostExecuteSuc(Response result) {
						if(name.contains("@")){
							new AppDialogBuilder(context).setTitle("请前往您的邮箱\n重置密码")
								.setPositiveButton(new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										backStack();
									}
								}).show();
						}else{
							addFragment(new PersonPasswordForgetPhoneFragment()
									.putSerializableExtra(name));
						}
					}
				}.setProgressDialog().execute();
			}
		});
		return new MyTopBar(context).setLeftBack().setTitle("忘记密码").setContentView(view);
	}
}
