package com.fax.faw_vw.fragment_person;

import java.net.URLEncoder;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fax.faw_vw.MainActivity;
import com.fax.faw_vw.MyFragment;
import com.fax.faw_vw.R;
import com.fax.faw_vw.model.LoginResponse;
import com.fax.faw_vw.model.Response;
import com.fax.faw_vw.util.ResponseTask;
import com.fax.faw_vw.views.AppDialogBuilder;
import com.fax.faw_vw.views.MyTopBar;

public class PersonDriverVerifyFragment extends MyFragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View view = View.inflate(context, R.layout.more_person_driver_verify, null);
		view.findViewById(R.id.commit_button).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				final LoginResponse loginResponse = LoginResponse.getCache();
				String name = ((TextView)view.findViewById(R.id.name_text)).getText().toString();
				final String vinText = ((TextView)view.findViewById(R.id.code_text)).getText().toString();
				String url = "http://faw-vw.allyes.com/index.php?g=api&m=user&a=certify"
						+ "&truename=" + URLEncoder.encode(name)
						+ "&vin=" + URLEncoder.encode(vinText)
						+ "&code=" + loginResponse.getBody().getLoginCode();
				new ResponseTask<Response>(context, url) {
					@Override
					protected void onPostExecuteSuc(Response result) {
						new AppDialogBuilder(context).setTitle("认证成功").setMessage("请进入个人中心查看")
							.setPositiveButton(null)
							.setOnDismissListener(new DialogInterface.OnDismissListener() {
								@Override
								public void onDismiss(DialogInterface dialog) {
									loginResponse.getBody().setVin(vinText);
									loginResponse.saveCache();
									getActivity().finish();
									startFragment(new PersonHomeFragment());
								}
							}).show();
					}
				}.setProgressDialog().execute();
			}
		});
		return new MyTopBar(context).setLeftBack().setTitle("车主认证").setContentView(view);
	}
}
