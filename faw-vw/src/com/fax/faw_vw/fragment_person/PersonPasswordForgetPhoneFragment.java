package com.fax.faw_vw.fragment_person;

import java.net.URLEncoder;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.fax.faw_vw.MyFragment;
import com.fax.faw_vw.R;
import com.fax.faw_vw.model.Response;
import com.fax.faw_vw.util.ResponseTask;
import com.fax.faw_vw.views.AppDialogBuilder;
import com.fax.faw_vw.views.MyTopBar;

public class PersonPasswordForgetPhoneFragment extends MyFragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final String name = getSerializableExtra(String.class);
		final View view = View.inflate(context, R.layout.more_person_reset_passwd_phone, null);
		((TextView)view.findViewById(R.id.phone_text)).setText(name);
		
		view.findViewById(R.id.commit_button).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String code = ((TextView)view.findViewById(R.id.code_text)).getText().toString();
				String passwd = ((TextView)view.findViewById(R.id.passwd_text)).getText().toString();
				String passwdConfirm = ((TextView)view.findViewById(R.id.passwd_confirm_text)).getText().toString();
				String url = "http://faw-vw.allyes.com/index.php?g=api&m=user&a=updatepwd"
						+ "&username=" + URLEncoder.encode(name)
						+ "&code=" + URLEncoder.encode(code)
						+ "&newpassword=" + passwd
						+ "&verifypassword=" + passwdConfirm;
				new ResponseTask<Response>(context, url) {
					@Override
					protected void onPostExecuteSuc(Response result) {
						Toast.makeText(context, "密码重置成功", Toast.LENGTH_SHORT).show();
						getActivity().finish();
						startFragment(new PersonLoginFragment());
					}
				}.setProgressDialog().execute();
			}
		});
		return new MyTopBar(context).setLeftBack().setTitle("重置密码").setContentView(view);
	}
}
