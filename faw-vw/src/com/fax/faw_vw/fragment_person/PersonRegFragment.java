package com.fax.faw_vw.fragment_person;

import java.net.URLEncoder;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.fax.faw_vw.MainActivity;
import com.fax.faw_vw.MyFragment;
import com.fax.faw_vw.R;
import com.fax.faw_vw.fargment_common.TextTitleFragment;
import com.fax.faw_vw.model.LoginResponse;
import com.fax.faw_vw.util.ResponseTask;
import com.fax.faw_vw.views.AppDialogBuilder;
import com.fax.faw_vw.views.MyTopBar;

public class PersonRegFragment extends MyFragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View view = View.inflate(context, R.layout.more_person_reg, null);
		((RadioGroup)view.findViewById(R.id.radio_group)).setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				if(!((CompoundButton)group.findViewById(checkedId)).isChecked()){
					return;
				}
				TextView nameTv = (TextView) view.findViewById(R.id.reg_text_phone_or_email);
				TextView nameEdit = (TextView) view.findViewById(R.id.name_text);
				switch(checkedId){
				case R.id.radioButton1:
					//邮箱
					nameTv.setText("*邮箱");
					nameEdit.setInputType(InputType.TYPE_CLASS_TEXT|InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
					break;
				case R.id.radioButton2:
					//手机
					nameTv.setText("*手机");
					nameEdit.setInputType(InputType.TYPE_CLASS_PHONE);
					break;
				}
				nameEdit.setText("");
			}
		});
		view.findViewById(R.id.commit_button).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(!((CheckBox)view.findViewById(R.id.reg_agreement_cb)).isChecked()){
					Toast.makeText(context, "请同意注册协议", Toast.LENGTH_SHORT).show();
					return;
				}
				TextView nameEdit = (TextView) view.findViewById(R.id.name_text);
				String name = nameEdit.getText().toString();
				if(nameEdit.getInputType() == InputType.TYPE_CLASS_PHONE){
					if(name.length()!=11 || (!name.startsWith("13") && !name.startsWith("14")
							&& !name.startsWith("15") && !name.startsWith("17") && !name.startsWith("18"))){
						Toast.makeText(context, "请输入正确的11位手机号码", Toast.LENGTH_SHORT).show();
						return;
					}
				}else{
					if(!name.contains("@")){
						Toast.makeText(context, "请输入正确的邮箱", Toast.LENGTH_SHORT).show();
						return;
					}
				}
				final String passwd = ((TextView)view.findViewById(R.id.passwd_text)).getText().toString();
				final String passwdConfirm = ((TextView)view.findViewById(R.id.passwd_confirm_text)).getText().toString();
				if(!passwd.equals(passwdConfirm)){
					Toast.makeText(context, "两次输入密码不一致", Toast.LENGTH_SHORT).show();
					return;
				}
				String url = "http://faw-vw.allyes.com/index.php?g=api&m=user&a=register"
						+ "&username=" + URLEncoder.encode(name)
						+ "&password=" + passwd;
				new ResponseTask<LoginResponse>(context, url) {
					@Override
					protected void onPostExecuteSuc(LoginResponse response) {
						response.saveCache();
						final PersonHomeFragment personHomeFragment = new PersonHomeFragment();
						replaceFragment(personHomeFragment);
						new AppDialogBuilder(context).setTitle("恭喜注册成功")
							.addTitleRightMore(R.drawable.app_dialog_ic_close, new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									startActivity(new Intent(context, MainActivity.class)
											.putExtra(RadioGroup.class.getName(), R.id.bottom_bar_home));
								}
							})
							.setPositiveButton("完善个人资料", new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									addFragment(personHomeFragment.getFragmentManager(),new PersonEditInfoFragment());
								}
							}).setNegativeButton("进行车主认证", new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									addFragment(personHomeFragment.getFragmentManager(),new PersonDriverVerifyFragment());
								}
							}).show();
					}
				}.setProgressDialog().execute();
			}
		});
		view.findViewById(R.id.reg_goto_agreement_btn).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startFragment(TextTitleFragment.createInstance("一汽-大众汽车有限公司保证所有个人数据都将按照隐私保护规定进行处理。在您许可的情况下，您在联系表格中所提供的数据将被保留和进行相应处理。\n"
						+ "本人在此同意此表格的信息可由一汽-大众汽车有限公司及一汽-大众授权经销商共享，并可由一汽-大众授权经销商用于其正常的商业活动，包括但不限于根据本人所提供的信息，通过电话，手机，电子邮件，信件及其它合理方式与本人联系。", "注册协议"));
			}
		});
		view.findViewById(R.id.reg_goto_login_btn).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				backStack();//返回登陆页
			}
		});
		return new MyTopBar(context).setLeftBack().setTitle("账号注册").setContentView(view);
	}

}
