package com.fax.faw_vw.fragments_main;

import java.net.URLEncoder;
import java.util.Arrays;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fax.faw_vw.FragmentContain;
import com.fax.faw_vw.MyApp;
import com.fax.faw_vw.MyFragment;
import com.fax.faw_vw.R;
import com.fax.faw_vw.fargment_common.ChooseCarsFragment;
import com.fax.faw_vw.fargment_common.WebViewFragment;
import com.fax.faw_vw.fragment_more.CarFAQFragment;
import com.fax.faw_vw.fragment_more.FeedbackFragment;
import com.fax.faw_vw.fragment_more.IncrementServiceFragment;
import com.fax.faw_vw.fragment_more.OnlineQAFragment;
import com.fax.faw_vw.fragment_more.PersonFragment;
import com.fax.faw_vw.fragment_more.QueryIllegalIndexFragment;
import com.fax.faw_vw.fragment_more.SettingFragment;
import com.fax.faw_vw.fragment_more.StatementFragment;
import com.fax.faw_vw.fragment_person.PersonHomeFragment;
import com.fax.faw_vw.fragments_car.CarDownloadFragment;
import com.fax.faw_vw.model.ImageTextPagePair;
import com.fax.faw_vw.model.LoginResponse;
import com.fax.faw_vw.views.AppDialogBuilder;
import com.fax.faw_vw.views.MyTopBar;
import com.fax.utils.view.list.ObjectXAdapter;
import com.fax.utils.view.list.ObjectXListView;
import com.umeng.analytics.MobclickAgent;
/**更多 页卡 */
public class MoreFragment extends MyFragment {
	static Fragment showAfterMarketDialog = new Fragment();
	private static ImageTextPagePair wenJuanPair = new ImageTextPagePair(R.drawable.main_more_manyidu, "客户满意度调研"){
		@Override
		public Fragment getFragment(final Context context) {
			LoginResponse loginResponse = LoginResponse.getCache();
			if(loginResponse==null || loginResponse.getBody()==null){
				new AppDialogBuilder(context).setTitle("请您登陆\n并进行车主认证")
				.setPositiveButton(new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						FragmentContain.start((Activity)context, PersonFragment.class);
					}
				}).show();
				return null;
			}
			if(TextUtils.isEmpty(loginResponse.getBody().getVin())){
				new AppDialogBuilder(context).setTitle("请您登陆\n并进行车主认证")
				.setPositiveButton(new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						FragmentContain.start((Activity)context, PersonFragment.class);
					}
				}).show();
				return null;
			}
			String url = "http://faw-vw.allyes.com/wenjuan2015/?"
					+ "name="+ (loginResponse==null?"":loginResponse.getBody().getUserName())
					+"&&cjh="+ (loginResponse==null?"":loginResponse.getBody().getVin())
					+"&&brandid="+ (loginResponse==null?"":loginResponse.getBody().getBrand_ID())
					+"&&mpnum="+ (loginResponse==null?"":loginResponse.getBody().getMobile())
					+"&&email=" + (loginResponse==null?"":loginResponse.getBody().getEmail());
			return WebViewFragment.createFragment(url, "一汽-大众客户满意度调研", true);
		}
		
	};
	ImageTextPagePair[] pagePairs = new ImageTextPagePair[]{
			new ImageTextPagePair(R.drawable.main_more_aftermarket, "一汽-大众服务", showAfterMarketDialog),
			new ImageTextPagePair(R.drawable.main_more_person, "个人中心", PersonFragment.class),
			new ImageTextPagePair(R.drawable.main_more_query_illegal, "违章查询", MyApp.createFragment(QueryIllegalIndexFragment.class)),
			new ImageTextPagePair(R.drawable.main_more_extra, "增值服务",  MyApp.createFragment(IncrementServiceFragment.class)),
			new ImageTextPagePair(R.drawable.main_more_online_service, "在线客服", MyApp.createFragment(OnlineQAFragment.class)),
			new ImageTextPagePair(R.drawable.main_more_feedback, "意见反馈", MyApp.createFragment(FeedbackFragment.class)),
			wenJuanPair,
			new ImageTextPagePair(R.drawable.main_more_faq, "常见功能指南视频", MyApp.createFragment(ChooseCarsFragment.class, CarFAQFragment.class, "常见功能指南视频")),
			new ImageTextPagePair(R.drawable.main_more_setting, "系统设置", MyApp.createFragment(SettingFragment.class)),
			new ImageTextPagePair(R.drawable.main_more_downcenter, "下载中心", MyApp.createFragment(ChooseCarsFragment.class, CarDownloadFragment.class, "下载中心")),
			new ImageTextPagePair(R.drawable.main_more_statement, "免责声明", MyApp.createFragment(StatementFragment.class)),
	};
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = View.inflate(context, R.layout.main_more, null);
		final ObjectXListView listView = (ObjectXListView) view.findViewById(android.R.id.list);
		listView.setPullRefreshEnable(false);
		listView.setDrawSelectorOnTop(true);
		listView.setBackgroundColor(Color.WHITE);
		listView.setSelector(R.drawable.common_btn_in_white);
		listView.setDivider(getResources().getDrawable(android.R.color.darker_gray));
		listView.setAdapter(new ObjectXAdapter.SingleLocalGridPageAdapter<ImageTextPagePair>(2) {
			@Override
			public List<ImageTextPagePair> instanceNewList() throws Exception {
				return Arrays.asList(pagePairs);
			}
			@Override
			protected View bindGridView(ViewGroup contain,final ImageTextPagePair t,final int position, View convertView) {
				LinearLayout layout = (LinearLayout) convertView;
				if(convertView==null){
					int padding = (int) MyApp.convertToDp(20);
					TextView tv = new TextView(context);
					tv.setTextSize(15);
					tv.setCompoundDrawablePadding(padding/2);
					tv.setGravity(Gravity.CENTER);
					
					layout = new LinearLayout(context);
					layout.setMinimumHeight(listView.getHeight()/4);
					layout.setPadding(padding/2, padding, padding, padding);
					layout.setGravity(Gravity.CENTER);
					layout.addView(tv, -2, -2);
				}
				
				TextView tv = (TextView) layout.getChildAt(0);
				tv.setText(t.getText());
				tv.setCompoundDrawablesWithIntrinsicBounds(0, t.getImgResId(), 0, 0);
				
				return layout;
			}
			@Override
			public void onItemClick(ImageTextPagePair t, View view, int position, long id) {
				super.onItemClick(t, view, position, id);
				MobclickAgent.onEvent(context, "android_More_"+t.getText());
				Fragment fragment = t.getFragment(context);
				if(fragment==null) return;
				if(fragment == showAfterMarketDialog){
					new AppDialogBuilder(context).setTitle("请前往应用商店下载\n\"一汽-大众服务\"APP").addTitleRightCloseDialog()
						.setPositiveButton(new OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								//点击确定先直接关闭
//								Uri uri = Uri.parse("market://search?q=pname:"+URLEncoder.encode("一汽-大众服务"));           
//							    Intent it = new Intent(Intent.ACTION_VIEW, uri);           
//							    try {
//									startActivity(it);
//								} catch (Exception e) {
//									e.printStackTrace();
//								}
							}
						}).show();
					return;
				}
				FragmentContain.start(getActivity(), fragment);
			}
			@Override
			protected int getDividerHeight() {
				return 1;
			}
		});

		
		return new MyTopBar(context).setTitle("更多").setContentView(view);
	}
}
