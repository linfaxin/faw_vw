package com.fax.faw_vw.fragments_main;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fax.faw_vw.FragmentContain;
import com.fax.faw_vw.MyFragment;
import com.fax.faw_vw.R;
import com.fax.faw_vw.fragment_dealer.SearchDealerFragment;
import com.fax.faw_vw.fragment_findcar.BrandServiceFragment;
import com.fax.faw_vw.fragment_findcar.FinancialServiceFragment;
import com.fax.faw_vw.fragments_car.MarketFragment;
import com.fax.faw_vw.fragments_car.NewsFragment;
import com.fax.faw_vw.fragments_car.OnlineOrderCarFragment;
import com.fax.faw_vw.views.AppDialogBuilder;
import com.fax.faw_vw.views.MyTopBar;

public class UsedCarShowroomFragment extends MyFragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.used_car_showroom, container, false);
		view.findViewById(R.id.used_car_showroom_brand_service).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				//品牌服务
				FragmentContain.start(getActivity(), BrandServiceFragment.class);
			}
		});
		
		View.OnClickListener pleaseDownAppClick = new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				new AppDialogBuilder(context).setTitle("请前往应用商店下载\n\"一汽-大众服务\"APP").addTitleRightCloseDialog()
					.setPositiveButton(null).show();
			}
		};
		view.findViewById(R.id.used_car_showroom_ok_car).setOnClickListener(pleaseDownAppClick);
		view.findViewById(R.id.used_car_showroom_used_service).setOnClickListener(pleaseDownAppClick);
		
		return new MyTopBar(context).setLeftBack().setTitle("二手车展厅").setContentView(view);
	}
}
