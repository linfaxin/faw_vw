package com.fax.faw_vw.fragment_dealer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fax.faw_vw.FragmentContain;
import com.fax.faw_vw.MyApp;
import com.fax.faw_vw.MyFragment;
import com.fax.faw_vw.R;
import com.fax.faw_vw.fragments_car.BookDriveFragment;
import com.fax.faw_vw.fragments_car.MarketFragment;
import com.fax.faw_vw.model.Dealer;
import com.fax.faw_vw.views.AppDialogBuilder;
import com.fax.faw_vw.views.MyTopBar;
import com.fax.utils.view.MultiFormatTextView;
import com.fax.utils.view.list.ObjectXAdapter;
import com.fax.utils.view.list.ObjectXListView;

/**经销商详情页 */
public class SearchDealerDetailFragment extends MyFragment {
	public static final int Request_SelectDealer = 1;
	TextView dealer_fullname;
	TextView dealer_address;
	TextView dealer_selltel;
	TextView money;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View view = inflater.inflate(R.layout.search_dealer_detail, container, false);
		final Dealer dealer = getSerializableExtra(Dealer.class);
        ((TextView)view.findViewById(R.id.dealer_fullname)).setText(dealer.getFullname());
        ((TextView)view.findViewById(R.id.dealer_address)).setText(dealer.getAddress());
        ((TextView)view.findViewById(R.id.dealer_selltel)).setText(dealer.getSelltel());
        view.findViewById(R.id.home_order_drive_btn).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				FragmentManager fm = getFragmentManager();
				for(Fragment fragment: fm.getFragments()){
					if(fragment instanceof BookDriveFragment){//存在预约的页面，那就把数据返回给这个页面
						fragment.onActivityResult(BookDriveFragment.Request_SelectDealer, Activity.RESULT_OK, MyApp.createIntent(dealer));
						fm.popBackStackImmediate();
						fm.popBackStackImmediate();
						fm.popBackStackImmediate();
						return;
					}
				}
				addFragment(MyApp.createFragment(BookDriveFragment.class, dealer));
			}
		});
        view.findViewById(R.id.dealer_info_content_navi_btn).setOnClickListener(new View.OnClickListener() {
        	@Override
        	public void onClick(View v) {
        		new AppDialogBuilder(context).setIcon(R.drawable.ic_launcher_with_shadow)
        			.setTitle(new MultiFormatTextView(context).convertToMulti("//S22您即将离开//\n众里寻车"))
        			.setPositiveButton(android.R.string.cancel, null)
        			.setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							try{
				        		Uri uri = Uri.parse("geo:"+dealer.getLatLng().latitude+","+dealer.getLatLng().longitude
				        				+"?q="+dealer.getFullname()+"+"+dealer.getSnippet()+"+"+dealer.getAddress());
				        		Intent intent = new Intent(Intent.ACTION_VIEW,uri);
				        		startActivity(intent);
			        		}catch(Exception e){
			        			Toast.makeText(context, "请先安装地图软件！", Toast.LENGTH_LONG).show();
			        		}
						}
					}).show();
        		
        	}
        });
        view.findViewById(R.id.dealer_goto_market).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				addFragment(MyApp.createFragment(MarketFragment.class, "优惠信息"));
			}
		});
		 return new MyTopBar(getActivity()).setLeftBack()
	                .setTitle("预约试驾").setContentView(view);
	}
}



