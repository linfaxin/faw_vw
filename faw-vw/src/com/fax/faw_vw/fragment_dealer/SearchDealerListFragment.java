package com.fax.faw_vw.fragment_dealer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fax.faw_vw.MyApp;
import com.fax.faw_vw.MyFragment;
import com.fax.faw_vw.R;
import com.fax.faw_vw.fragment_more.QueryIllegalInfoFragment;
import com.fax.faw_vw.model.Dealer;
import com.fax.faw_vw.views.CommonRowItem;
import com.fax.faw_vw.views.MyTopBar;
import com.fax.utils.view.list.ObjectXAdapter;
import com.fax.utils.view.list.ObjectXListView;

/**经销商列表 */
public class SearchDealerListFragment extends MyFragment {
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final ArrayList<Dealer> dealers = getSerializableExtra(ArrayList.class);
		
		ObjectXListView listView = new ObjectXListView(context);
		listView.setPullRefreshEnable(false);
		listView.setDivider(new ColorDrawable(Color.TRANSPARENT));
		listView.setDividerHeight((int) MyApp.convertToDp(10));
		listView.setAdapter(new ObjectXAdapter.SingleLocalPageAdapter<Dealer>() {
			@Override
			public View bindView(Dealer dealer, int position, View view) {
				if(view == null){
					view = View.inflate(context, R.layout.search_dealer_list_item, null);
					((CommonRowItem)view.findViewById(R.id.dealer_fullname)).setContentSize(12);
					((CommonRowItem)view.findViewById(R.id.dealer_address)).setContentSize(12);
					((CommonRowItem)view.findViewById(R.id.dealer_selltel)).setContentSize(12);
				}

				((CommonRowItem)view.findViewById(R.id.dealer_fullname)).setContent(dealer.getFullname());
				((CommonRowItem)view.findViewById(R.id.dealer_address)).setContent(dealer.getAddress());
				((CommonRowItem)view.findViewById(R.id.dealer_selltel)).setContent(dealer.getSelltel());
				return view;
			}
			@Override
			public void onItemClick(Dealer d, View view, int position, long id) {
				super.onItemClick(d, view, position, id);
				addFragment(MyApp.createFragment(SearchDealerDetailFragment.class, d));
			}
			@Override
			public List<Dealer> instanceNewList() throws Exception {
				return dealers;
			}
		});

		return new MyTopBar(getActivity()).setLeftBack()
                .setTitle("经销商").setContentView(listView);
	}
}



