package com.fax.faw_vw.fragment_360;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

import org.apache.commons.io.IOUtils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fax.faw_vw.FragmentContain;
import com.fax.faw_vw.FragmentContainLandscape;
import com.fax.faw_vw.MyApp;
import com.fax.faw_vw.MyFragment;
import com.fax.faw_vw.R;
import com.fax.faw_vw.fragment_360.PackedFileLoader.FrameInfo;
import com.fax.faw_vw.fragment_360.PackedFileLoader.FrameInfo.KeyPoint;
import com.fax.faw_vw.fragment_more.CarFAQDetailFragment;
import com.fax.faw_vw.fragment_more.CarFAQFragment;
import com.fax.faw_vw.model.ShowCarItem;
import com.fax.faw_vw.util.OpenFileUtil;
import com.fax.faw_vw.util.SimpleDirectionGesture;
import com.fax.faw_vw.views.FitWidthImageView;
import com.fax.faw_vw.views.ListPopupWindow;
import com.fax.faw_vw.views.clickshow.ClickShowTextView;
import com.fax.utils.frameAnim.FrameAnimListener;
import com.fax.utils.frameAnim.FrameAnimation;
import com.fax.utils.task.ResultAsyncTask;
import com.fax.utils.view.photoview.PhotoView;

public class Show360FrameFragment extends MyFragment {
	PackedFileLoader fileLoader;
	ArrayList<FrameInfo> frameInfos;
	ImageView progressPoint;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		View logoTopbar = getActivity().findViewById(R.id.common_logo_topbar);
		int padding = (int) MyApp.convertToDp(12);
		logoTopbar.setPadding(0, padding, 0, padding);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		ViewGroup view = (ViewGroup) inflater.inflate(R.layout.show360_key_frames, container, false);
		final ImageView imageView = (ImageView) view.findViewById(android.R.id.background);
		final FrameLayout frontLay = (FrameLayout) view.findViewById(R.id.show360_key_frames_front_point);
		final ShowCarItem carItem = getSerializableExtra(ShowCarItem.class);

		view.findViewById(R.id.back_btn).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				getActivity().finish();
			}
		});
		
		fileLoader = PackedFileLoader.getInstance(context, carItem);
		if(fileLoader==null){
			MyApp.look360Car(context, carItem);
			return view;
		}
		if(!(context instanceof FragmentContainLandscape)){//如果不是横向的屏幕，那么强制
			Fragment fragment = MyApp.createFragment(Show360FrameFragment.class, carItem);
			FragmentContainLandscape.start((Activity) context, fragment);
			getActivity().finish();
			return view;
		}
		//首次进入360页面操作提示
		if(!MyApp.hasKeyOnce("tip_360")){
			final View tip = View.inflate(context, R.layout.tip_360, null);
			view.addView(tip, new FrameLayout.LayoutParams(-1, -1));
			tip.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					((ViewGroup)tip.getParent()).removeView(tip);
				}
			});
		}
		
		view.setOnTouchListener(new SimpleDirectionGesture(view){
            @Override
            public void onFling(int direction) {
            	if(direction == SimpleDirectionGesture.Direction_Left){
            		playNextFrames();
            	}else if(direction == SimpleDirectionGesture.Direction_Right){
            		playLastFrames();
            	}
            }
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if(infoView != null && event.getAction() == MotionEvent.ACTION_UP){
					frontLay.removeView(infoView);
				}
				return super.onTouch(v, event);
			}
        });
		frameInfos = fileLoader.getFrameInfoList();
		imageView.setImageDrawable(frameInfos.get(0).frames.get(0).decodeDrawable(context));
		final View detailBtn = view.findViewById(R.id.detail_btn);
		detailBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				final ListPopupWindow listPopup = new ListPopupWindow(context);
				listPopup.setAnchorView(v);
				listPopup.setAdapter(new BaseAdapter() {
					@SuppressLint("NewApi")
					@Override
					public View getView(final int position, View convertView, ViewGroup parent) {
						TextView imageView = new ClickShowTextView(context);
						imageView.setGravity(Gravity.CENTER);
						imageView.setBackgroundResource(position==0?R.drawable.show360_menu_0:R.drawable.show360_menu_1);
						if(preShowIndex==3 && position==0){
							if(VERSION.SDK_INT>=11) imageView.setAlpha(0);
						}
						
						if(position==0){
							if(preShowIndex == 2){
								imageView.setText("　　全部内景");
							}else{
								imageView.setText("　　更多颜色");
							}	
						}else{
							imageView.setText("　　功能视频");
						}
						
						imageView.setOnClickListener(new View.OnClickListener(){
							@Override
							public void onClick(View v) {
								if(position==0){
									//查看详细情况
									if(preShowIndex == 2){
										//查看全景
										FragmentContainLandscape.start(getActivity(), MyApp.createFragment(PanoFragment.class, getArguments()));
									}else if(preShowIndex!=3){
										addFragment(MyApp.createFragment(Touch360Fragment.class, getArguments()));
									}
								}else{
									startFragment(MyApp.createFragment(CarFAQFragment.class, carItem));
								}
								listPopup.dismiss();
							}
							
						});
						return imageView;
					}
					@Override
					public long getItemId(int position) {
						return 0;
					}
					@Override
					public Object getItem(int position) {
						return null;
					}
					@Override
					public int getCount() {
						return 2;
					}
				});
				listPopup.showAsMenuList();
			}
		});
		progressPoint = (ImageView) view.findViewById(android.R.id.icon);
		maxProgress = (frameInfos.get(frameInfos.size()-1).getKeyFrame() - 1);
		LinearLayout linear = (LinearLayout) view.findViewById(android.R.id.summary);
		for(int i=0,size=frameInfos.size(); i<size; i++){
			TextView tv = new TextView(context);
			tv.setText(frameInfos.get(i).title);
			tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);
			tv.setSingleLine();
			tv.setShadowLayer(2, 2, 2, Color.WHITE);
			if(i==0) tv.setGravity(Gravity.LEFT);
			else if(i==size-1) tv.setGravity(Gravity.RIGHT);
			else tv.setGravity(Gravity.CENTER);
			linear.addView(tv, new LinearLayout.LayoutParams(0, -2, 1));
		}
		
		frameAnimation = new FrameAnimation(imageView);
		frameAnimation.setFrameAnimListener(new FrameAnimListener() {
			@Override
			public void onPlaying(FrameAnimation frameAnimation, int frameIndex) {
				setProgress(frameInfos.get(showingIndex).getKeyFrame() + frameIndex );
			}
			@Override
			public void onFinish(FrameAnimation frameAnimation) {
				showRedPointToFront(frontLay, imageView, frameInfos.get(preShowIndex));
			}
			@Override
			public void onStart(FrameAnimation animation) {
				frontLay.removeAllViews();
			}
		});
		
		view.postDelayed(new Runnable() {
			@Override
			public void run() {
				showRedPointToFront(frontLay, imageView, frameInfos.get(preShowIndex));
			}
		}, 100);
		return view;
	}
	
	int maxProgress;
	private void setProgress(int progress){
		if(progress>maxProgress) progress = maxProgress;
		int left = progress * (((View)progressPoint.getParent()).getWidth()-progressPoint.getWidth()) / maxProgress ;
		((MarginLayoutParams)progressPoint.getLayoutParams()).leftMargin = left;
		progressPoint.requestLayout();
	}
	
	int showingIndex;
	int preShowIndex;//在帧动画开头位置
	FrameAnimation frameAnimation;
	private void playNextFrames(){
		if(frameAnimation.isFinish()){
			if( preShowIndex < frameInfos.size()-1){
				showingIndex = preShowIndex;
				preShowIndex++;
				
				frameAnimation.setFrames(frameInfos.get(showingIndex).frames);
				frameAnimation.start();
			}
		}
	}
	
	private void playLastFrames(){
		if(frameAnimation.isFinish()){
			if( preShowIndex > 0 ){
				preShowIndex--;
				showingIndex = preShowIndex;
				
				frameAnimation.setFrames(frameInfos.get(showingIndex).frames);
				frameAnimation.reverseFrames();
				frameAnimation.start();
			}
		}
	}

	private void showRedPointToFront(FrameLayout front, ImageView imageView, FrameInfo frameInfo){
		if(front.getWidth()!=imageView.getWidth() || front.getHeight()!=imageView.getHeight()){
			front.getLayoutParams().width = imageView.getWidth();
			front.getLayoutParams().height = imageView.getHeight();
			front.requestLayout();
		}
		int animDelay = 0;
		for(KeyPoint keyPoint : frameInfo.getKeyPoints()){
			View btn = addRedPointToFront(front, imageView, keyPoint);
			Animation anim = AnimationUtils.loadAnimation(context, R.anim.small_to_normal);
			anim.setStartOffset(animDelay);
			btn.startAnimation(anim);
			animDelay+=200;
		}
	}
	ImageView infoView;
	Integer carImgWidth,carImgHeight;
	private View addRedPointToFront(final FrameLayout front,final ImageView imageView,final KeyPoint keyPoint){
		ImageButton btn = new ImageButton(context);
		btn.setImageResource(R.drawable.common_ic_red_point);
		btn.setBackgroundResource(R.drawable.common_btn_in_white);
		
//		if(imageView.getDrawable()==null) return btn;
		final int size = (int) MyApp.convertToDp(20);
		if(carImgWidth==null || carImgHeight==null){
			carImgWidth = imageView.getDrawable().getIntrinsicWidth();
			carImgHeight = imageView.getDrawable().getIntrinsicHeight();
		}
		final int x = (int) (1000f/568 * keyPoint.getHot_x() * imageView.getWidth() / carImgWidth - size);
		final int y = (int) (1000f/568 * keyPoint.getHot_y() * imageView.getHeight() / carImgHeight - size);
		
		FrameLayout.LayoutParams param = new FrameLayout.LayoutParams(size*2, size*2, Gravity.LEFT|Gravity.TOP);
		param.topMargin = y;
		param.leftMargin = x;
		front.addView(btn, param);
		
		btn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(infoView != null) front.removeView(infoView);
				infoView = new FitWidthImageView(context);
				try {
					BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(), 
							BitmapFactory.decodeStream(fileLoader.readKeyPointImageFile(keyPoint)));
					bitmapDrawable.setBounds(0, 0, 
							(int)MyApp.convertToDp(bitmapDrawable.getIntrinsicWidth())*2/3 , 
							(int)MyApp.convertToDp(bitmapDrawable.getIntrinsicHeight())*2/3);
					infoView.setImageDrawable(bitmapDrawable);
					FrameLayout.LayoutParams param = new FrameLayout.LayoutParams(
							(int) bitmapDrawable.getBounds().width(),
//							-2,
							-2, Gravity.LEFT|Gravity.TOP);
					int topMargin = 0;
					if(keyPoint.getImage_y()!=0){
						topMargin = (int) (keyPoint.getImage_y() * imageView.getHeight() / imageView.getDrawable().getIntrinsicHeight() + size/2);
					}else{
						topMargin = (int) (y - bitmapDrawable.getBounds().height() * 6 / 10 );
					}
					param.topMargin = topMargin;
					
					int leftMargin = 0;
					if(keyPoint.getImage_x()!=0){
						leftMargin = (int) (1000f / 568 * keyPoint.getImage_x() * imageView.getWidth() / imageView.getDrawable().getIntrinsicWidth() - size);
					}else{
						leftMargin = x + size * 2 / 3;
					}
					param.leftMargin = leftMargin;
					
					front.addView(infoView, param);
					infoView.startAnimation(AnimationUtils.loadAnimation(context, R.anim.fade_in_to_left));
					infoView.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							if(infoView != null) front.removeView(infoView);
							if("mp4".equals(keyPoint.type)){
								final File mp4File = new File(context.getExternalCacheDir(), keyPoint.getFilename());
								if(mp4File.exists()){
									startActivity(OpenFileUtil.getOpenIntent(mp4File.getPath()));
								}else{
									new ResultAsyncTask<Boolean>(context) {
										@Override
										protected void onPostExecuteSuc(Boolean result) {
											startActivity(OpenFileUtil.getOpenIntent(mp4File.getPath()));
										}
										@Override
										protected void onPostExecuteFail(Boolean result) {
											mp4File.delete();
										}
										@Override
										protected Boolean doInBackground(Void... params) {
											try {
												IOUtils.copy(fileLoader.readKeyPointFile(keyPoint), new FileOutputStream(mp4File));
												return true;
											} catch (Exception e) {
												e.printStackTrace();
											}
											return false;
										}
									}.setProgressDialog().execute();
								}
							}else if("jpg".equalsIgnoreCase(keyPoint.type)){
								final PhotoView photoView = new PhotoView(context);
								final Runnable removePhotoRun = new Runnable() {
									boolean runed = false;
									@Override
									public void run() {
										if(runed) return;
										runed = true;
										Animation a = AnimationUtils.loadAnimation(context, R.anim.dismiss_tobottom);
										photoView.startAnimation(a);
										a.setAnimationListener(new Animation.AnimationListener() {
											@Override
											public void onAnimationStart(Animation animation) {
											}
											@Override
											public void onAnimationRepeat(Animation animation) {
											}
											@Override
											public void onAnimationEnd(Animation animation) {
												((ViewGroup)photoView.getParent()).removeView(photoView);
											}
										});
									}
								};
								try {
									photoView.setImageBitmap(BitmapFactory.decodeStream(fileLoader.readKeyPointFile(keyPoint)));
									photoView.setFocusable(true);
									photoView.setFocusableInTouchMode(true);
									photoView.requestFocus();
									
									photoView.setOnKeyListener(new View.OnKeyListener() {
										@Override
										public boolean onKey(View v, int keyCode, KeyEvent event) {
											if(keyCode==KeyEvent.KEYCODE_BACK){
												removePhotoRun.run();
											}
											return true;
										}
									});
									photoView.setOnClickListener(new View.OnClickListener() {
										@Override
										public void onClick(View v) {
											removePhotoRun.run();
										}
									});
									getActivity().addContentView(photoView, new FrameLayout.LayoutParams(-1, -1));
									photoView.startAnimation(AnimationUtils.loadAnimation(context, R.anim.show_totop));
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						}
					});
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		return btn;
	}

	@Override
	public void onDetach() {
		super.onDetach();
		if(fileLoader!=null) fileLoader.recycle();
	}
}




