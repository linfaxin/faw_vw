package com.fax.faw_vw;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.fax.faw_vw.views.FitWidthImageView;
import com.fax.utils.frameAnim.AssetFrame;
import com.fax.utils.frameAnim.Frame;
import com.fax.utils.frameAnim.FrameAnimListener;
import com.fax.utils.frameAnim.FrameAnimation;
import com.fax.utils.frameAnim.FrameFactory;
import com.fax.utils.view.pager.PointIndicator;
import com.fax.utils.view.pager.SamePagerAdapter;
import com.fax.utils.view.pager.ViewsPagerAdapter;
import com.umeng.analytics.MobclickAgent;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

public class StartActivity extends BaseActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		MobclickAgent.updateOnlineConfig(this);
		
		List<AssetFrame> frames = FrameFactory.createFramesFromAsset(this, "intro/logo_anim", 20);
		if(frames.size()>0 && !MyApp.hasKeyOnce("intro_1115")){
			ImageView logoAnim = new ImageView(this);	
			logoAnim.setScaleType(ScaleType.FIT_XY);
			setContentView(logoAnim);
			FrameAnimation fa = new FrameAnimation(logoAnim, frames);
			fa.start();
			fa.setFrameAnimListener(new FrameAnimListener() {
				@Override
				public void onStart(FrameAnimation animation) {
				}
				@Override
				public void onPlaying(FrameAnimation animation, int frameIndex) {
				}
				@Override
				public void onFinish(FrameAnimation animation) {
					setContentView(R.layout.intro_first);
					final ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
			        final List<AssetFrame> intros = FrameFactory.createFramesFromAsset(StartActivity.this, "intro/pages", 0);
			        viewPager.setAdapter(new SamePagerAdapter<AssetFrame>(intros) {
						@Override
						public View getView(AssetFrame t, int position, View convertView) {
							ImageView imageView = (ImageView) convertView;
							if(imageView == null){
								imageView = new ImageView(StartActivity.this);
								imageView.setScaleType(ScaleType.FIT_XY);
							}
							FrameAnimation.setFrameToView(imageView, t);
							return imageView;
						}
					});

			        final View startBtn = findViewById(R.id.intro_btn_start);
			        startBtn.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {					
							startActivity(new Intent(StartActivity.this, MainActivity.class));
						}
					});
					
			        PointIndicator pointIndicator = (PointIndicator) findViewById(R.id.point_indicator);
			        pointIndicator.setColorNormal(Color.DKGRAY);
			        pointIndicator.setColorChecked(getResources().getColor(R.color.dark_blue));
			        pointIndicator.bindViewPager(viewPager, new ViewPager.OnPageChangeListener() {
						@Override
						public void onPageSelected(int position) {
							if(position==viewPager.getAdapter().getCount()-1){
								startBtn.setVisibility(View.VISIBLE);
								Animation anim = AnimationUtils.loadAnimation(StartActivity.this, R.anim.fade_in);
								anim.setStartOffset(1000);
								anim.setDuration(anim.getDuration()*2);
								startBtn.startAnimation(anim);
							} else{
								startBtn.setVisibility(View.INVISIBLE);
							}
						}
						@Override
						public void onPageScrolled(int arg0, float offset, int arg2) {
						}
						@Override
						public void onPageScrollStateChanged(int state) {
						}
					});
				}
			});
		}else{
			 startActivity(new Intent(StartActivity.this, MainActivity.class));
		}
	}

}
