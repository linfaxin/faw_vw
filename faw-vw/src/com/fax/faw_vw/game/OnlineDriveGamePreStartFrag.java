package com.fax.faw_vw.game;

import java.io.File;
import java.net.URL;
import java.util.zip.ZipFile;

import org.apache.commons.io.FilenameUtils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;

import com.fax.faw_vw.FragmentContainLandscape;
import com.fax.faw_vw.MyApp;
import com.fax.faw_vw.MyFragment;
import com.fax.faw_vw.R;
import com.fax.faw_vw.fragment_360.Show360FrameFragment;
import com.fax.faw_vw.model.ShowCarItem;
import com.fax.faw_vw.util.ZipUtil;
import com.fax.utils.frameAnim.AssetFrame;
import com.fax.utils.frameAnim.FrameAnimation;
import com.fax.utils.frameAnim.FrameFactory;
import com.fax.utils.task.DownloadTask;
import com.fax.utils.task.ResultAsyncTask;
import com.fax.utils.view.pager.SamePagerAdapter;

public class OnlineDriveGamePreStartFrag extends MyFragment {
	private static ShowCarItem[] CHOOSE_CAR_ITEMS = new ShowCarItem[]{
			ShowCarItem.SHOW_CAR_ITEM_CC,
			ShowCarItem.SHOW_CAR_ITEM_BORA,
			ShowCarItem.SHOW_CAR_ITEM_GOLF,
			ShowCarItem.SHOW_CAR_ITEM_JETTA,
			ShowCarItem.SHOW_CAR_ITEM_MAGOTAN,
			ShowCarItem.SHOW_CAR_ITEM_SAGITAR,
		};

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final ShowCarItem showCarItem = getSerializableExtra(ShowCarItem.class);
		View view;
		if(showCarItem!=null){//已选中了车型
			view = inflater.inflate(R.layout.online_drive_game_pre_start, container, false);
			view.findViewById(android.R.id.button1).setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					startGame(showCarItem);
				}
			});
			
		}else{//没有选中车型，再次选一遍
			view = inflater.inflate(R.layout.online_drive_game_choose_car, container, false);
			final ViewPager viewPager = (ViewPager) view.findViewById(R.id.view_pager);
			viewPager.setAdapter(new SamePagerAdapter<AssetFrame>(FrameFactory.createFramesFromAsset(context, "online_drive_game/choose_cars", -1)) {
				@Override
				public View getView(AssetFrame t, int position, View convertView) {
					if(convertView==null){
						convertView = new ImageView(context);
					}
					FrameAnimation.setFrameToView(convertView, t);
					return convertView;
				}
				@Override
				protected AssetFrame getItemAtPosition(int position) {
					return super.getItemAtPosition(position % CHOOSE_CAR_ITEMS.length);
				}
				@Override
				public int getCount() {
					return Integer.MAX_VALUE;
				}
			});
			final TextView carName = (TextView) view.findViewById(android.R.id.text1);
			viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
				@Override
				public void onPageSelected(int position) {
					carName.setText(CHOOSE_CAR_ITEMS[position% CHOOSE_CAR_ITEMS.length].getModel_cn());
				}
				@Override
				public void onPageScrolled(int arg0, float arg1, int arg2) {
				}
				@Override
				public void onPageScrollStateChanged(int arg0) {
				}
			});
			viewPager.setCurrentItem(Integer.MAX_VALUE/CHOOSE_CAR_ITEMS.length/2 * CHOOSE_CAR_ITEMS.length);

			view.findViewById(R.id.online_drive_game_ic_arrow_left).setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					viewPager.setCurrentItem(viewPager.getCurrentItem()-1);
				}
			});
			view.findViewById(R.id.online_drive_game_ic_arrow_right).setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					viewPager.setCurrentItem(viewPager.getCurrentItem()+1);
				}
			});
			view.findViewById(android.R.id.button1).setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					startGame(CHOOSE_CAR_ITEMS[viewPager.getCurrentItem()% CHOOSE_CAR_ITEMS.length]);
				}
			});
		}

		view.findViewById(android.R.id.closeButton).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				backStack();
			}
		});
		return view;
	}
	
	private void startGame(final ShowCarItem showCarItem){
		final Runnable openRun = new Runnable() {
			@Override
			public void run() {
				getActivity().finish();
				startActivity(new Intent(context, OnlineDriveGameActivity.class)
							.setData(Uri.fromFile(unzipDir))
							.putExtra(ShowCarItem.class.getName(), showCarItem));
			}
		};
		checkResDownloaded(context, openRun);
	}

	private static String url = MyApp.ResUrl + "game/game_android.zip";
	private static File file = new File(MyApp.getAppContext().getExternalCacheDir(), "game_android.zip");
	private static File unzipDir = new File(MyApp.getAppContext().getExternalCacheDir(), "game_android");
	public static void checkResDownloaded(final Context context, final Runnable runAfterDown){
		if(file.exists() && !file.isDirectory() && file.length()>0 ){
			try {
				//check 解压的目录还在
				ZipUtil.unzip(file, unzipDir);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			runAfterDown.run();
		}else{
			file.delete();
			new ResultAsyncTask<Integer>(context) {
				@Override
				protected void onPostExecuteSuc(Integer result) {
					if(result!=null){
						String fileSizeMsg = result<=0 ? "" : "("+DownloadTask.sizeToString(result)+")";
						String message  = "您即将开始下载试驾游戏，请点击确认或取消";
						if(!MyApp.isWifiConnect()){
							message = "即将开始下载试驾游戏" + fileSizeMsg + "，建议您使用WIFI。土豪随意!^_^";
						}
						new AlertDialog.Builder(context).setTitle("试驾游戏")
								.setMessage(message)
								.setNegativeButton(android.R.string.cancel, null)
								.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										final File dlFile = new File(file.getPath()+".dl");
										new DownloadTask(context, url, dlFile, true) {
											@Override
											protected File doInBackground(Void... params) {
												File dlFile = super.doInBackground(params);
												new Handler(Looper.getMainLooper()).post(new Runnable() {
													@Override
													public void run() {
														getProgressDialog().setMessage("正在解压...");
													}
												});
												try {
													ZipUtil.unzip(dlFile, unzipDir);
												} catch (Exception e) {
													e.printStackTrace();
													return null;
												}
												return dlFile;
											}
											@Override
											protected void onPostExecuteSuc(File result) {
												dlFile.renameTo(file);
												runAfterDown.run();
											}
										}.setProgressDialog().execute();
										
									}
								}).show();
					}
				}
				@Override
				protected Integer doInBackground(Void... params) {
					try {
						return new URL(url).openConnection().getContentLength();
					} catch (Exception e) {
						e.printStackTrace();
					}
					return 0;
				}
			}.setProgressDialog().execute();
			
		}
	}

}
