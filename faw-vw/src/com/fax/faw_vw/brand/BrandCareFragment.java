package com.fax.faw_vw.brand;

import java.util.ArrayList;
import java.util.List;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.textservice.TextInfo;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fax.faw_vw.FragmentContain;
import com.fax.faw_vw.MyApp;
import com.fax.faw_vw.MyFragment;
import com.fax.faw_vw.R;
import com.fax.faw_vw.fargment_common.AssetFrameTitleFragment;
import com.fax.faw_vw.fargment_common.NetImageTitleFragment;
import com.fax.faw_vw.fargment_common.ResImageTitleFragment;
import com.fax.faw_vw.model.ImageTextPagePair;
import com.fax.faw_vw.model.TextFramePagePair;
import com.fax.faw_vw.views.MyTopBar;
import com.fax.utils.frameAnim.AssetFrame;
import com.fax.utils.frameAnim.FrameFactory;
import com.fax.utils.view.list.ObjectXAdapter;
import com.fax.utils.view.list.ObjectXListView;

public class BrandCareFragment extends MyFragment {
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final List<ImageTextPagePair> pagePairs = new ArrayList<ImageTextPagePair>();
		pagePairs.add(new ImageTextPagePair(R.drawable.brand_care_cba, "中国男子篮球职业联赛", 
				NetImageTitleFragment.createInstance(MyApp.ResUrl + "brand/brand_1.jpg", "中国男子篮球职业联赛")));
		pagePairs.add(new ImageTextPagePair(R.drawable.brand_care_crc, "中国汽车拉力锦标赛", BrandCareCRCFragment.class));
		
		
		final ObjectXListView listView = new ObjectXListView(context);
		listView.setPullRefreshEnable(false);
		listView.setDrawSelectorOnTop(true);
		listView.setBackgroundColor(Color.WHITE);
		listView.setSelector(R.drawable.common_btn_in_white);
		listView.setDivider(getResources().getDrawable(android.R.color.darker_gray));

		listView.setAdapter(new ObjectXAdapter.SingleLocalGridPageAdapter<ImageTextPagePair>(2) {
			@Override
			public List<ImageTextPagePair> instanceNewList() throws Exception {
				return pagePairs;
			}
			@Override
			protected View bindGridView(ViewGroup contain,final ImageTextPagePair t,final int position, View convertView) {
				LinearLayout layout = (LinearLayout) convertView;
				if(convertView==null){
					int padding = (int) MyApp.convertToDp(20);
					TextView tv = new TextView(context);
					tv.setTextSize(12);
					tv.setCompoundDrawablePadding(padding/2);
					tv.setGravity(Gravity.CENTER);
					
					layout = new LinearLayout(context);
					layout.setMinimumHeight(listView.getHeight()/4);
					layout.setPadding(padding, padding, padding, padding);
					layout.setGravity(Gravity.CENTER);
					layout.addView(tv, -2, -2);
				}
				
				TextView tv = (TextView) layout.getChildAt(0);
				tv.setText(t.getText());
				tv.setCompoundDrawablesWithIntrinsicBounds(0, t.getImgResId(), 0, 0);
				
				return layout;
			}
			@Override
			public void onItemClick(ImageTextPagePair t, View view, int position, long id) {
				super.onItemClick(t, view, position, id);
				if(t.getFragment()==null) return;
				FragmentContain.start(getActivity(), t.getFragment());
			}
			@Override
			protected int getDividerHeight() {
				return 1;
			}
		});
	
		return new MyTopBar(context).setLeftBack().setTitle("品牌关爱").setContentView(listView);
	}
}
