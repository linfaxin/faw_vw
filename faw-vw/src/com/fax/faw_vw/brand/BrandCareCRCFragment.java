package com.fax.faw_vw.brand;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FilenameUtils;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fax.faw_vw.FragmentContain;
import com.fax.faw_vw.MyApp;
import com.fax.faw_vw.MyFragment;
import com.fax.faw_vw.R;
import com.fax.faw_vw.fargment_common.AssetFrameTitleFragment;
import com.fax.faw_vw.fargment_common.NetImageTitleFragment;
import com.fax.faw_vw.fargment_common.ResImageTitleFragment;
import com.fax.faw_vw.model.ImageTextPagePair;
import com.fax.faw_vw.model.TextFramePagePair;
import com.fax.faw_vw.views.MyTopBar;
import com.fax.utils.frameAnim.AssetFrame;
import com.fax.utils.frameAnim.FrameFactory;
import com.fax.utils.view.MultiFormatTextView;
import com.fax.utils.view.list.ObjectXAdapter;
import com.fax.utils.view.list.ObjectXListView;

public class BrandCareCRCFragment extends MyFragment{
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final List<AssetFrame> btnFrames = FrameFactory.createFramesFromAsset(context, "brand/care/crc_btns", -1);
		final List<AssetFrame> pageFrames = FrameFactory.createFramesFromAsset(context, "brand/care/crc_pages", -1);
		final String[] btns = new String[]{
				"一骑绝尘//S12\n第七代高尔夫战车CRC龙游站强势封王",
				"渐入佳境//S12\n一汽-大众车队怀柔站斩获全场亚军",
				"CRC激战张掖//S12\n一汽-大众车队登顶颠覆",
				"CRC汝城站落幕//S12\n一汽-大众车队卫冕两驱组冠军",
				"CRC贵州金沙开战//S12\n一汽-大众携高尔夫重装上阵"
		};
		final List<TextFramePagePair> pagePairs = new ArrayList<TextFramePagePair>();
		for(int i=0,length=btns.length;i<length;i++){
			AssetFrame btnFrame = btnFrames.get(i);
			String name = FilenameUtils.getName(pageFrames.get(i).getPath());
			Fragment pageFragment = NetImageTitleFragment.createInstance(MyApp.ResUrl+"brand/"+name, "中国汽车拉力锦标赛");
			pagePairs.add(new TextFramePagePair(btns[i], btnFrame, pageFragment));
		}
		
		final ObjectXListView listView = new ObjectXListView(context);
		listView.setPullRefreshEnable(false);
//		listView.setDrawSelectorOnTop(true);
//		listView.setBackgroundColor(Color.WHITE);
		listView.setSelector(R.drawable.common_btn_in_white);
		listView.setDivider(getResources().getDrawable(android.R.color.darker_gray));

		int column = isPortrait()? 1:2;
		listView.setAdapter(new ObjectXAdapter.SingleLocalGridPageAdapter<TextFramePagePair>(column) {
			@Override
			public List<TextFramePagePair> instanceNewList() throws Exception {
				return pagePairs;
			}
			@Override
			protected View bindGridView(ViewGroup contain,final TextFramePagePair t,final int position, View convertView) {
				MultiFormatTextView tv = (MultiFormatTextView) convertView;
				if(convertView==null){
					int padding = (int) MyApp.convertToDp(10);
					tv = new MultiFormatTextView(context);
					tv.setTextSize(15);
					tv.setCompoundDrawablePadding(padding);
					tv.setPadding(padding, padding, padding, padding);
					tv.setGravity(Gravity.CENTER_VERTICAL|Gravity.LEFT);
				}
				tv.setTextMulti(t.getText());
				tv.setCompoundDrawablesWithIntrinsicBounds(t.decodeDrawable(context), null, null, null);
				return tv;
			}
			@Override
			public void onItemClick(TextFramePagePair t, View view, int position, long id) {
				super.onItemClick(t, view, position, id);
				if(t.getFragment()==null) return;
				FragmentContain.start(getActivity(), t.getFragment());
			}
			@Override
			protected int getDividerHeight() {
				return 1;
			}
		});
	
		return new MyTopBar(context).setLeftBack().setTitle("中国汽车拉力锦标赛").setContentView(listView);
	}
}
