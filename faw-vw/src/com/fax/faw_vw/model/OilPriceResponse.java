package com.fax.faw_vw.model;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

import org.apache.commons.io.IOUtils;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;


public class OilPriceResponse implements Serializable {
	private String resultcode;
	private String reason;
	private ArrayList<OilPrice> result;
	public boolean isResultOK(){
		return "200".equals(resultcode);
	}
	public String getReason() {
		return reason;
	}
	public OilPrice getOilPrice(Context context, String cityName){
		if(result==null) return null;
		Province[] provinces = null;
		try {
			provinces = new Gson().fromJson(IOUtils.toString(context.getAssets().open("provinces.json")),
					Province[].class);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		String provinceName = "上海";
		for(Province province : provinces){
			if(cityName.contains(province.getName()) || province.getName().contains(cityName)){
				provinceName = province.getName();
				break;
			};
			for(String cityNameIn : province.getCities()){
				if(cityName.contains(cityNameIn) || cityNameIn.contains(cityName)){
					provinceName = province.getName();
					break;
				}
			}
		}
		for(OilPrice oilPrice : result){
			if(oilPrice.getCity().contains(provinceName) || provinceName.contains(oilPrice.getCity())){
				return oilPrice;
			}
		}
		return null;
	}
	public static class OilPrice{
		String city;
		String b90;
		String b93;
		String b97;
		String b0;
		public String getCity() {
			return city;
		}
		public String getB90() {
			return b90;
		}
		public String getB93() {
			return b93;
		}
		public String getB97() {
			return b97;
		}
		public String getB0() {
			return b0;
		}
	}

}
