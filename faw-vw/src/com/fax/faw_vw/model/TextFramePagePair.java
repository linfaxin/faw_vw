package com.fax.faw_vw.model;

import com.fax.utils.frameAnim.BasicBitmapFrame;
import com.fax.utils.frameAnim.Frame;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.TypedValue;

public class TextFramePagePair {
	String text;
	BasicBitmapFrame frame;
	boolean isLandscape;
	Fragment fragment;
//	Class<? extends Activity> activityClass;
	public TextFramePagePair(String text) {
		this.text = text;
	}
	public TextFramePagePair(String text, BasicBitmapFrame frame, Fragment fragment) {
		this.text = text;
		this.frame = frame;
		this.fragment = fragment;
	}
	public TextFramePagePair(String text, BasicBitmapFrame frame, Fragment fragment, boolean isLandscape) {
		this.text = text;
		this.frame = frame;
		this.fragment = fragment;
		this.isLandscape = isLandscape;
	}
	public Drawable decodeDrawable(Context context) {
		return frame.decodeDrawable(context, DisplayMetrics.DENSITY_XHIGH);
	}
	public Drawable decodeDrawable(Context context, int inDensity) {
		return frame.decodeDrawable(context, inDensity);
	}
	public Fragment getFragment() {
		return fragment;
	}
	public boolean isLandscape() {
		return isLandscape;
	}
	public String getText() {
		return text;
	}
}
