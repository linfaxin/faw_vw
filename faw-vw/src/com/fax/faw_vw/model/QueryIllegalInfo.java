package com.fax.faw_vw.model;

import java.io.Serializable;


public class QueryIllegalInfo implements Comparable<QueryIllegalInfo>, Serializable{
	String cityName;
	String engineNumber;
	String plateNumber;
	String vehicleIdNumber;
	long saveTime;
	
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String toString(){
		return plateNumber;
	}
	public String getEngineNumber() {
		return engineNumber;
	}
	public void setEngineNumber(String engineNumber) {
		this.engineNumber = engineNumber;
	}
	public String getPlateNumber() {
		return plateNumber;
	}
	public void setPlateNumber(String plateNumber) {
		this.plateNumber = plateNumber;
	}
	public String getVehicleIdNumber() {
		return vehicleIdNumber;
	}
	public void setVehicleIdNumber(String vehicleIdNumber) {
		this.vehicleIdNumber = vehicleIdNumber;
	}
	public void setSaveTime(long saveTime) {
		this.saveTime = saveTime;
	}
	public long getSaveTime(){
		return saveTime;
	}
	@Override
	public int compareTo(QueryIllegalInfo another) {
		return (int) (another.saveTime - saveTime);
	}
}
