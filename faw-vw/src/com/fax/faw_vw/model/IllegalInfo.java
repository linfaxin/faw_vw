package com.fax.faw_vw.model;

import java.io.Serializable;
import java.util.ArrayList;


public class IllegalInfo extends Response{
	String province;
	String city;
	String hphm;
	String hpzl;
	private IllegalInfoList result;

	public IllegalInfoList getIllegalInfoList() {
		return result;
	}

	public class IllegalInfoList implements Serializable{
		private String province;
		private String city;
		private String hphm;
		private ArrayList<IllegalInfoItem> lists;
		
		public String getProvince() {
			return province;
		}

		public String getCity() {
			return city;
		}

		public String getHphm() {
			return hphm;
		}

		public ArrayList<IllegalInfoItem> getLists() {
			return lists;
		}

		public class IllegalInfoItem implements Serializable{
	
			private String date;
			private String area;
			private String act;
			private String code;
			private String fen;
			private String money;
			private String handled;
			private String longitude;
			private String latitude;
			private String PayNo;
			private String CollectOrgan;
			public String getDate() {
				return date;
			}
			public void setDate(String date) {
				this.date = date;
			}
			public String getArea() {
				return area;
			}
			public void setArea(String area) {
				this.area = area;
			}
			public String getAct() {
				return act;
			}
			public void setAct(String act) {
				this.act = act;
			}
			public String getCode() {
				return code;
			}
			public void setCode(String code) {
				this.code = code;
			}
			public String getFen() {
				return fen;
			}
			public void setFen(String fen) {
				this.fen = fen;
			}
			public String getMoney() {
				return money;
			}
			public void setMoney(String money) {
				this.money = money;
			}
			public String getHandled() {
				return handled;
			}
			public void setHandled(String handled) {
				this.handled = handled;
			}
			public String getLongitude() {
				return longitude;
			}
			public void setLongitude(String longitude) {
				this.longitude = longitude;
			}
			public String getLatitude() {
				return latitude;
			}
			public void setLatitude(String latitude) {
				this.latitude = latitude;
			}
			public String getPayNo() {
				return PayNo;
			}
			public void setPayNo(String payNo) {
				PayNo = payNo;
			}
			public String getCollectOrgan() {
				return CollectOrgan;
			}
			public void setCollectOrgan(String collectOrgan) {
				CollectOrgan = collectOrgan;
			}
			
		}
		
	}
}
