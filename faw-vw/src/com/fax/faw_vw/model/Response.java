package com.fax.faw_vw.model;

import java.io.Serializable;

import android.text.TextUtils;

public class Response implements Serializable{
	int Success;
	String Message;

	String resultcode;
	String reason;
	
	public int getSuccess() {
		return Success;
	}
	public boolean isSuc(){
		return Success!=0 || "200".equals(resultcode);
	}
	public String getMessage() {
		if(TextUtils.isEmpty(Message)) return reason;
		return Message;
	}

}
