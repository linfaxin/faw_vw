package com.fax.faw_vw.model;

import java.util.ArrayList;

import android.text.TextUtils;

public class IllegalCityResponse extends Response{
	ArrayList<CityList> result;
	public String getCityCode(String cityName){
		if(result==null || TextUtils.isEmpty(cityName)) return null;
		for(CityList cityList : result){
			for(CityList.CityInfo cityInfo : cityList.citys){
				if(cityName.startsWith(cityInfo.city_name)){
					return cityInfo.city_code;
				}
			}
		}
		return null;
	}
	
	private static class CityList{
		String province;
		String province_code;
		ArrayList<CityInfo> citys;
		private static class CityInfo{
			String city_name;
			String city_code;
			String abbr;
		}
	}
}
