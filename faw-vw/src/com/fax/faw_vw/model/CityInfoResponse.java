package com.fax.faw_vw.model;

import java.util.ArrayList;

public class CityInfoResponse extends Response {
	ArrayList<CityInfo> Body;

	public ArrayList<CityInfo> getBody() {
		return Body;
	}

	public void setBody(ArrayList<CityInfo> body) {
		Body = body;
	}
	
}
