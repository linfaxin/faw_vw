package com.fax.faw_vw.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Province implements Serializable{
	String name;
	ArrayList<String> cities;
	public String getName(){
		return name;
	}
	public ArrayList<String> getCities(){
		return cities;
	}
}
