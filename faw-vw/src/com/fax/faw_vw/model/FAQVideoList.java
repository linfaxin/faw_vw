package com.fax.faw_vw.model;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.zip.ZipFile;

import org.apache.commons.io.FileUtils;

import com.fax.faw_vw.MyApp;
import com.fax.utils.frameAnim.ZipBitmapFrame;
import com.google.gson.Gson;


public class FAQVideoList implements Serializable{
	private static FAQVideoList faqVideoList;
	private static ZipFile faqZip;
	public static FAQVideoList getInstance(){
		if(faqVideoList==null){
			File faqZipFile = new File(MyApp.getAppContext().getExternalCacheDir(), "faq.zip");
			try {
//				if(faqZipFile.length()<=0){
					FileUtils.copyInputStreamToFile(MyApp.getAppContext().getAssets().open("faq.zip"), faqZipFile);
//				}
				faqZip = new ZipFile(faqZipFile);
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			try {
				faqVideoList = new Gson().fromJson(new InputStreamReader(faqZip.getInputStream(faqZip.getEntry("fuckask.json"))),
						FAQVideoList.class);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return faqVideoList;
	}
	
	FAQVideo[] cc;
	FAQVideo[] golf;
	FAQVideo[] sagitar;
	FAQVideo[] magotan;
	FAQVideo[] bora;
	FAQVideo[] jetta;
	public FAQVideo[] getFAQVideoByCar(ShowCarItem carItem){
		if(carItem == null){
			return new FAQVideo[0];
		}
		if(carItem.equals(ShowCarItem.SHOW_CAR_ITEM_CC)){
			return cc;
		}
		if(carItem.equals(ShowCarItem.SHOW_CAR_ITEM_GOLF)){
			return golf;
		}
		if(carItem.equals(ShowCarItem.SHOW_CAR_ITEM_SAGITAR)){
			return sagitar;
		}
		if(carItem.equals(ShowCarItem.SHOW_CAR_ITEM_MAGOTAN)){
			return magotan;
		}
		if(carItem.equals(ShowCarItem.SHOW_CAR_ITEM_BORA)){
			return bora;
		}
		if(carItem.equals(ShowCarItem.SHOW_CAR_ITEM_JETTA)){
			return jetta;
		}
		return cc;
	}
	
	public FAQVideo[] getCc() {
		return cc;
	}


	public FAQVideo[] getGolf() {
		return golf;
	}


	public FAQVideo[] getSagitar() {
		return sagitar;
	}


	public FAQVideo[] getMagotan() {
		return magotan;
	}


	public FAQVideo[] getBora() {
		return bora;
	}


	public FAQVideo[] getJetta() {
		return jetta;
	}


	public static class FAQVideo implements Serializable{
		String bt;
		ArrayList<Option> option;
		
		public String getBt() {
			return bt;
		}

		public ArrayList<Option> getOption() {
			return option;
		}

		public static class Option implements Serializable{
			String st;
			String image;
			String mp4;
			public String getSt() {
				return st;
			}
			public ZipBitmapFrame getImage(FAQVideo fVideo) {
				return new ZipBitmapFrame(faqZip, fVideo.bt+"/"+image);
			}
			public String getMp4() {
				return mp4;
			}
		}
	}
}
