package com.fax.faw_vw.model;

public class CityInfo {
	String PROVINCE_ID;
	String CITY_NAME;
	String CITY_ID;

	public String getAreaName() {
		return CITY_NAME;
	}

	public String getAreaCode() {
		return CITY_ID;
	}

	public String toString(){
		return CITY_NAME;
	}
}
