package com.fax.faw_vw.model;

import java.io.Serializable;

import com.fax.faw_vw.MyApp;
import com.google.gson.Gson;

public class LoginResponse extends Response {
//	    "Success": 1,
//	    "Message": "",
//	    "Body": {
//    "UserName": "test_android",
//    "PassWord": "eRk31WaV8q10MS96PiTBHKTv34IvA+PCXTLhlNNxVrg=",
//    "UserID": 4722,
//    "Email": "android@test.com",
//    "Mobile": "test_android",
//    "NiceName": "test_android",
//    "Sex": "1",
//    "TrueName": null,
//    "Vin": null,
//    "Brand_ID": null,
//    "Model_ID": null,
//    "Status": null,
//    "LoginCode": "GIT1kLWOaU",
//    "LastLoginTime": 1421085784,
//    "LastLoginIP": "124.77.167.5",
//    "ID": "46",
//    "TotalCredit": 0,
//    "ProvinceID": "1009001",
//    "ProvinceTitle": "上海",
//    "CityID": "1009001001",
//    "CityTitle": "上海市",
//    "Address": "",
//    "BuyPlan": "0",
//    "WillBrandID": "10",
//    "WillBrandTitle": "宝来"
//	    }
	Body Body;
	
	private static final String CacheKey = "LoginPerson";
	public static void clearCache(){
		MyApp.getDefaultSp().edit().remove(CacheKey).apply();
	}
	public static LoginResponse getCache(){
		String json = MyApp.getDefaultSp().getString(CacheKey, null);
		if(json==null) return null;
		return new Gson().fromJson(json, LoginResponse.class);
	}
	public void saveCache(){
		MyApp.getDefaultSp().edit().putString(CacheKey, new Gson().toJson(this)).apply();
	}
	public Body getBody() {
		if(Body==null) Body = new Body();
		return Body;
	}

	public static class Body implements Serializable{
		String UserName;
		String PassWord;
		int UserID;
		String Email;
		String Mobile;
		String NiceName;
		String Sex;
		String TrueName;
		String Vin;
		String Brand_ID;
		String Model_ID;
		String Status;
		String LoginCode;
		int TotalCredit;
		String ProvinceID;
		String ProvinceTitle;
		String CityID;
		String CityTitle;
		String Address;
		String BuyPlan;
		String WillBrandID;
		String WillBrandTitle;
		public String getUserName() {
			return UserName;
		}
		public String getPassWord() {
			return PassWord;
		}
		public int getUserID() {
			return UserID;
		}
		public String getEmail() {
			return Email;
		}
		public String getMobile() {
			return Mobile;
		}
		public String getNiceName() {
			return NiceName;
		}
		public String getSex() {
			return Sex;
		}
		public String getTrueName() {
			return TrueName;
		}
		public String getVin() {
			return Vin;
		}
		public void setVin(String vin){
			Vin = vin;
		}
		public String getBrand_ID() {
			return Brand_ID;
		}
		public String getModel_ID() {
			return Model_ID;
		}
		public String getStatus() {
			return Status;
		}
		public String getLoginCode() {
			return LoginCode;
		}
		public int getTotalCredit() {
			return TotalCredit;
		}
		public String getProvinceID() {
			return ProvinceID;
		}
		public void setProvinceID(String provinceID) {
			ProvinceID = provinceID;
		}
		public String getProvinceTitle() {
			return ProvinceTitle;
		}
		public void setProvinceTitle(String provinceTitle) {
			ProvinceTitle = provinceTitle;
		}
		public String getCityID() {
			return CityID;
		}
		public void setCityID(String cityID) {
			CityID = cityID;
		}
		public String getCityTitle() {
			return CityTitle;
		}
		public void setCityTitle(String cityTitle) {
			CityTitle = cityTitle;
		}
		public String getAddress() {
			return Address;
		}
		public void setAddress(String address) {
			Address = address;
		}
		public int getBuyPlan() {
			try {
				return Integer.parseInt(BuyPlan);
			} catch (Exception e) {
			}
			return 0;
		}
		public void setBuyPlan(String buyPlan) {
			BuyPlan = buyPlan;
		}
		public String getWillBrandID() {
			return WillBrandID;
		}
		public void setWillBrandID(String willBrandID) {
			WillBrandID = willBrandID;
		}
		public String getWillBrandTitle() {
			return WillBrandTitle;
		}
		public void setWillBrandTitle(String willBrandTitle) {
			WillBrandTitle = willBrandTitle;
		}
		public void setUserName(String userName) {
			UserName = userName;
		}
		public void setPassWord(String passWord) {
			PassWord = passWord;
		}
		public void setUserID(int userID) {
			UserID = userID;
		}
		public void setEmail(String email) {
			Email = email;
		}
		public void setMobile(String mobile) {
			Mobile = mobile;
		}
		public void setNiceName(String niceName) {
			NiceName = niceName;
		}
		public void setSex(String sex) {
			Sex = sex;
		}
		public void setTrueName(String trueName) {
			TrueName = trueName;
		}
		public void setBrand_ID(String brand_ID) {
			Brand_ID = brand_ID;
		}
		public void setModel_ID(String model_ID) {
			Model_ID = model_ID;
		}
		public void setStatus(String status) {
			Status = status;
		}
		public void setLoginCode(String loginCode) {
			LoginCode = loginCode;
		}
		public void setTotalCredit(int totalCredit) {
			TotalCredit = totalCredit;
		}
	}
}
