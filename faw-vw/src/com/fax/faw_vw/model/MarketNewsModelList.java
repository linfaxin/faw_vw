package com.fax.faw_vw.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

//市场活动，媒体声音 通用的JavaBean
public class MarketNewsModelList extends Respon{
	Body Body;
	class Body{
		ArrayList<NewsModel> list;
	}
	public ArrayList<NewsModel> getModels() {
		if(Body.list==null) return new ArrayList<MarketNewsModelList.NewsModel>();
		return Body.list;
	}
	
	/**
	"TITLE": "",
	"SUMMARY": "",
	"NEWS_TYPE": "",
	"DEALER_ID": "-1",
	"SERIES_IDS": "",
	"CONTENT": "",
	"BEGIN_TIME": "",
	"END_TIME": "",
	"BIG_PICTURE": "",
	"THUM_PICTURE": "",
	"UPDATE_TIME": "2014/10/20 14:30:37",
	"DELETE_FLAG": "False",
	"ID": "48333",
	"IS_TOP": "False",
	"STATUS": "1",
	"SOURCE_TYPE": "0",
	"NEWS_BIG_TYPE": "1",
	"CITY_ID": "-1"
	 */
	public class NewsModel implements Serializable{
		String TITLE;
        String SUMMARY;
        String CONTENT;
        String UPDATE_TIME;
		String THUM_PICTURE;
		public String getTITLE() {
			return TITLE;
		}
		public String getSUMMARY() {
			return SUMMARY;
		}
		public String getCONTENT() {
			return "<!DOCTYPE html><html>"+
                    "<head>"+
                    "<meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\">"+
                    "<title></title>"+
                    "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0\" />"+
                    "<meta name=\"apple-mobile-web-app-capable\" content=\"yes\" />"+
                    "<meta name=\"apple-mobile-web-app-status-bar-style\" content=\"black\" />"+
                    "</head>"+
                    "<style type=\"text/css\">"+
                    "body {"+
                    "background-color:#FFFFFF;"+
                    "margin-left: 10px;"+
                    "margin-top: 10px;"+
                    "margin-right: 10px;"+
                    "margin-bottom: 10px;}"+
                    "img{width:95%% !important; height: auto !important;}"+
                    "h1{text-align: left; font-size: 18px}"+
                    "h2{text-align: left; font-size: 12px; color:#000000}"+
                    "h3{text-align: left; font-size: 12px; color:#000000}"+
                    "</style>"+
                    "<body>"+
                    "<div style='width:300px'>"+
                    "<h1>"+TITLE+"</h1><h3>"+UPDATE_TIME+"<h3>"+ CONTENT +
                    "</div>"+
                    "</body>"+
                    "</html>";
//			return "<html>"
//					+ "<head>"
//					+ "<title>"+TITLE+"</title>"
//					+ "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>"
//					+ "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=10.0, minimum-scale=1, user-scalable=no\"/>"
//					+ "</head>"
//					+ "<body>" 
//					+ "<h3>" + TITLE +"</h3></br>"
//					+ UPDATE_TIME +"</br>"
//					+ doc.html() 
//					+"</body>"
//					+ "</html>";
		}
		public String getUPDATE_TIME() {
			try {
				return UPDATE_TIME.split(" ")[0];
			} catch (Exception e) {
			}
			return UPDATE_TIME;
		}
		public String getTHUM_PICTURE() {
			return THUM_PICTURE;
		}
		
	}
}
