package com.fax.faw_vw.model;

import java.io.Serializable;

import com.amap.api.maps2d.model.LatLng;

public class Dealer implements Serializable{
	String DEALER_ID;
	String DEALER_NAME;
	String ADDRESS;
	String SALES_PHONE;
	String LONGITUDE;
	String LATITUDE;

	public LatLng getLatLng(){
		try {
			double lng = Double.valueOf(LONGITUDE);
			double lat = Double.valueOf(LATITUDE);
			if(lng>lat){
				return new LatLng(lat, lng);
			}
			return new LatLng(lng, lat);//fix 服务器经纬度写反了
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	public String getSnippet(){
		return "地址："+ADDRESS+"\n电话："+SALES_PHONE;
	}

	public String getId() {
		return DEALER_ID;
	}

	public String getName() {
		return DEALER_NAME;
	}

	public String getFullname() {
		return DEALER_NAME;
	}

	public String getAddress() {
		return ADDRESS;
	}

	public String getSelltel() {
		return SALES_PHONE;
	}
	
}
