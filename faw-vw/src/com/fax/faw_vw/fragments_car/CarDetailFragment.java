package com.fax.faw_vw.fragments_car;

import com.fax.faw_vw.FragmentContainLandscape;
import com.fax.faw_vw.MainActivity;
import com.fax.faw_vw.MyApp;
import com.fax.faw_vw.MyFragment;
import com.fax.faw_vw.R;
import com.fax.faw_vw.fragment_dealer.SearchDealerFragment;
import com.fax.faw_vw.fragment_findcar.FinancialServiceFragment;
import com.fax.faw_vw.game.OnlineDriveGameActivity;
import com.fax.faw_vw.game.OnlineDriveGamePreStartFrag;
import com.fax.faw_vw.model.ShowCarItemRes;
import com.fax.faw_vw.model.ShowCarItem;
import com.fax.faw_vw.util.SimpleDirectionGesture;
import com.fax.faw_vw.util.ViewUtils;
import com.fax.faw_vw.views.FitWidthImageView;
import com.fax.faw_vw.views.MyTopBar;
import com.umeng.analytics.MobclickAgent;

import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import android.view.View.MeasureSpec;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class CarDetailFragment extends MyFragment {
	
	public void checkTipShow() {
		//首次进入选车型的提示
		if(!MyApp.hasKeyOnce("tip_car_detail")){
			final View tip = View.inflate(context, R.layout.tip_car_detail, null);
			getActivity().addContentView(tip, new FrameLayout.LayoutParams(-1, -1));
			tip.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					((ViewGroup)tip.getParent()).removeView(tip);
				}
			});
			
			ShowCarItemRes res = (carItemChild!=null ? carItemChild.getItemRes() : showCarItem.getDetailRes());
			if(res.getResId360()<=0){
				tip.findViewById(R.id.tip_img_360).setVisibility(View.INVISIBLE);
			}
		}
	}

	ShowCarItem showCarItem;
	ShowCarItemRes.CarItemChild carItemChild;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.showcar_detail, container, false);
		ViewUtils.fitAllTextToPadSize(view, false, true);
		showCarItem = getSerializableExtra(ShowCarItem.class);
		carItemChild = getSerializableExtra(ShowCarItemRes.CarItemChild.class);//选中的一个子项
		ShowCarItemRes.CarItemChild[] resChildren = showCarItem.getDetailRes().getItemChildren();//速腾的子/父页面这个值不为空
		
		ShowCarItemRes res = (carItemChild!=null ? carItemChild.getItemRes() : showCarItem.getDetailRes());
		
		LinearLayout headContain = (LinearLayout) view.findViewById(R.id.showcar_detail_head_contain);
		if(resChildren!=null && carItemChild==null){//有子项的父页面用
			headContain.removeAllViews();
			for(int i=0,size=resChildren.length;i<size;i++){
				final ShowCarItemRes.CarItemChild carItemChild = resChildren[i];
				ImageView imgView = new FitWidthImageView(context);
//				imgView.setAdjustViewBounds(true);
				imgView.setImageResource(carItemChild.getImgResId());
				imgView.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						replaceFragment(MyApp.createFragment(CarDetailFragment.class, showCarItem, carItemChild));
					}
				});
				headContain.addView(imgView, new LinearLayout.LayoutParams(0, -1, 1));
				if(isLandscape()){
//					imgView.setPadding((int) MyApp.convertToDp(20), 0, 0, 0);
					((MarginLayoutParams)imgView.getLayoutParams()).leftMargin = 
							(int) (ViewUtils.getFitPadScale(context)*MyApp.convertToDp(60/size));
					headContain.setBackgroundColor(0xffdbdadf);
					headContain.setGravity(Gravity.BOTTOM);
				}
			}
			
		}else{//其他正常样式
			fillHeadItem(view.findViewById(R.id.showcar_detail_head_1), res.getHeads()[0]);
			fillHeadItem(view.findViewById(R.id.showcar_detail_head_2), res.getHeads()[1]);
			fillHeadItem(view.findViewById(R.id.showcar_detail_head_3), res.getHeads()[2]);
			fillHeadItem(view.findViewById(R.id.showcar_detail_head_4), res.getHeads()[3]);
			fillHeadItem(view.findViewById(R.id.showcar_detail_head_5), res.getHeads()[4]);
			
			if(res.getHeads()[1]==null && res.getHeads()[2]==null)
				((View)view.findViewById(R.id.showcar_detail_head_2).getParent()).setVisibility(View.GONE);
			if(res.getHeads()[3]==null && res.getHeads()[4]==null)
				((View)view.findViewById(R.id.showcar_detail_head_4).getParent()).setVisibility(View.GONE);
			
			if(carItemChild!=null){//是车型子页
				ImageView backTip = new ImageView(context);
				backTip.setBackgroundResource(R.drawable.common_btn_in_black);
				backTip.setImageResource(R.drawable.showcar_detail_sagitar_back_bt);
				backTip.setScaleType(ScaleType.FIT_XY);

				int padLeft = (int) MyApp.convertToDp(20);
				LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(-2, -1);
				params.leftMargin = - padLeft;
				backTip.setPadding(padLeft, 0, 0, 0);
				headContain.addView(backTip, params);
				backTip.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						replaceFragment(MyApp.createFragment(CarDetailFragment.class, showCarItem));
					}
				});
			}
		}
		
		//360车型
		ImageView round360Image = ((ImageView)view.findViewById(R.id.showcar_detail_360_img));
		if(res.getResId360()>0){
			round360Image.setImageResource(res.getResId360());
			round360Image.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					MyApp.look360Car(context, showCarItem);
					MobclickAgent.onEvent(context, "android_carlist_"+showCarItem.getModelResName()+"_360");
				}
			});
		}else{
			round360Image.setVisibility(View.GONE);
		}
		
		
		//配置表
		view.findViewById(R.id.showcar_detail_bt0).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				addFragment(ModelListFragment.newInstance(showCarItem, CarSpecsFragment.class));
				MobclickAgent.onEvent(context, "android_carlist_"+showCarItem.getModelResName()+"_Devicetable");
			}
		});
		//预约试驾
		view.findViewById(R.id.showcar_detail_bt1).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				addFragment(MyApp.createFragment(BookDriveFragment.class, showCarItem));
				MobclickAgent.onEvent(context, "android_carlist_"+showCarItem.getModelResName()+"_Appointmentdrive");
			}
		});
		//车型对比
		view.findViewById(R.id.showcar_detail_bt2).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				addFragment(MyApp.createFragment(CarSpecsCompareFragment.class, showCarItem));
				MobclickAgent.onEvent(context, "android_carlist_"+showCarItem.getModelResName()+"_Compare");
			}
		});
		//在线试驾
		view.findViewById(R.id.showcar_detail_bt3).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				MobclickAgent.onEvent(context, "android_carlist_"+showCarItem.getModelResName()+"_Game");
				
				if(showCarItem.equals(ShowCarItem.SHOW_CAR_ITEM_SAGITAR_NEW)){
					int[] xy = new int[2];
					v.getLocationInWindow(xy);
					Toast toast = Toast.makeText(context, "敬请期待", Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.TOP|Gravity.LEFT, xy[0]+v.getWidth()/2, xy[1]-v.getHeight()/2);
					toast.show();
					return;
				}
				OnlineDriveGamePreStartFrag.checkResDownloaded(context, new Runnable() {
					public void run() {
						FragmentContainLandscape.start(getActivity(), OnlineDriveGamePreStartFrag.class, MyApp.createIntent(showCarItem), 0);
					}
				});
			}
		});
		//经销商查询
		view.findViewById(R.id.showcar_detail_bt4).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				addFragment(MyApp.createFragment(SearchDealerFragment.class, showCarItem));
				MobclickAgent.onEvent(context, "android_carlist_"+showCarItem.getModelResName()+"_Dealer");
			}
		});
		//金融服务
		view.findViewById(R.id.showcar_detail_bt5).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				addFragment(MyApp.createFragment(FinancialServiceFragment.class, showCarItem));
				MobclickAgent.onEvent(context, "android_carlist_"+showCarItem.getModelResName()+"_Finance");
			}
		});
		//市场活动
		view.findViewById(R.id.showcar_detail_bt6).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				addFragment(MyApp.createFragment(MarketFragment.class, showCarItem));
				MobclickAgent.onEvent(context, "android_carlist_"+showCarItem.getModelResName()+"_Marketing");
			}
		});
		//媒体声音
		view.findViewById(R.id.showcar_detail_bt7).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				addFragment(MyApp.createFragment(NewsFragment.class, showCarItem));
				MobclickAgent.onEvent(context, "android_carlist_"+showCarItem.getModelResName()+"_Mediavoice");
			}
		});
		//报价索取
		view.findViewById(R.id.showcar_detail_bottom_btn1).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				addFragment(MyApp.createFragment(RequestPriceFragment.class, showCarItem));
				MobclickAgent.onEvent(context, "android_carlist_"+showCarItem.getModelResName()+"_Quotation");
			}
		});
		//下载中心
		view.findViewById(R.id.showcar_detail_bottom_btn2).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				addFragment(MyApp.createFragment(CarDownloadFragment.class, showCarItem));
				MobclickAgent.onEvent(context, "android_carlist_"+showCarItem.getModelResName()+"_Download");
			}
		});
		
		String title=showCarItem.getModel_cn();
		if(carItemChild!=null){//是车型子页
			title = carItemChild.getName().toUpperCase();
		}
		checkTipShow();
		return new MyTopBar(getActivity()).setLeftBack()
				.setContentView(view).setTitle(title);
	}
	
	private void fillHeadItem(final View headItem, final ShowCarItemRes.ShowCarDetailHead detailHead){
		if(detailHead == null){
			headItem.setVisibility(View.GONE);
		}else{
			((ImageView)headItem.findViewById(android.R.id.background)).setImageResource(detailHead.getResId());
			((TextView)headItem.findViewById(android.R.id.title)).setText(detailHead.getTitle());
			
			headItem.setOnTouchListener(new SimpleDirectionGesture(headItem) {
				@Override
				public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
					if(distanceX>ViewConfiguration.get(context).getScaledTouchSlop() && distanceX>distanceY)
						headItem.getParent().requestDisallowInterceptTouchEvent(true);
					return super.onScroll(e1, e2, distanceX, distanceY);
				}
				@Override
				public void onFling(int direction) {//返回车型的父页面
					if(carItemChild!=null && direction == SimpleDirectionGesture.Direction_Left){
						replaceFragment(MyApp.createFragment(CarDetailFragment.class, showCarItem));
					}
				}
				@Override
				public boolean onSingleTapConfirmed(MotionEvent e) {
					MobclickAgent.onEvent(context, "android_carlist_"+showCarItem.getModelResName()+"_"+detailHead.getTitle());
					FragmentTransaction ft = getFragmentManager().beginTransaction();
					ft.setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out);
					addFragment(ft, MyApp.createFragment(ShowCarHeadDetailFragment.class, detailHead, carItemChild, showCarItem));
					ft.commit();
					return super.onSingleTapConfirmed(e);
				}
			});
		}
	}
	
}
