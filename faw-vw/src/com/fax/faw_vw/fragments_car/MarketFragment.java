package com.fax.faw_vw.fragments_car;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.amap.api.location.AMapLocation;
import com.amap.api.maps2d.AMapUtils;
import com.amap.api.maps2d.model.LatLng;
import com.fax.faw_vw.MyApp;
import com.fax.faw_vw.MyFragment;
import com.fax.faw_vw.R;
import com.fax.faw_vw.fargment_common.LocalWebViewFragment;
import com.fax.faw_vw.model.CityInfo;
import com.fax.faw_vw.model.CityInfoResponse;
import com.fax.faw_vw.model.MarketNewsModelList;
import com.fax.faw_vw.model.ShowCarItem;
import com.fax.faw_vw.model.MarketNewsModelList.NewsModel;
import com.fax.faw_vw.model.ProvinceList;
import com.fax.faw_vw.util.BodyWrapResponseTask;
import com.fax.faw_vw.util.LocManager;
import com.fax.faw_vw.views.MyTopBar;
import com.fax.utils.bitmap.BitmapManager;
import com.fax.utils.task.HttpAsyncTask;
import com.fax.utils.view.TopBarContain;
import com.fax.utils.view.list.ObjectXAdapter;
import com.fax.utils.view.list.ObjectXListView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * 市场活动
 */
public class MarketFragment extends MyFragment {
    int sourcetype;
    String cityid;
    ShowCarItem showCarItem;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	final TopBarContain topBar = new MyTopBar(context).setLeftBack()
        		.setTitle("市场活动").setContentView(R.layout.market);
    	showCarItem = getSerializableExtra(ShowCarItem.class);
        String title = getSerializableExtra(String.class);
        if(!TextUtils.isEmpty(title)){
        	topBar.setTitle(title);
        }
        
        final ProgressDialog pd = ProgressDialog.show(context, "提示", "请稍候...");
        LocManager.reqLoc(context, new LocManager.LocationListener() {
			@Override
			public void onFindLocation(final AMapLocation aMapLocation) {
				pd.dismiss();
				
				String json = null;
				try {
					json = IOUtils.toString(getActivity().getAssets().open("cityConfig.json"));
				} catch (IOException e) {
					e.printStackTrace();
				}
		    	LatLng locLatLng = new LatLng(aMapLocation.getLatitude(), aMapLocation.getLongitude());
		    	ProvinceList.Province wantProvince = null;
		    	float minDistance = Float.MAX_VALUE;
				ProvinceList provinceList = new Gson().fromJson(json, ProvinceList.class);
		    	for(ProvinceList.Province province : provinceList.getData()){
		    		float distance = AMapUtils.calculateLineDistance(province.getLatLng(), locLatLng);
		    		if(wantProvince == null || distance < minDistance){
		    			minDistance = distance;
		    			wantProvince = province;
		    		}
		    	}
		    	
		    	new BodyWrapResponseTask<CityInfoResponse>(context, MyApp.getCityListUrl(wantProvince)) {
					@Override
					protected void onPostExecuteSuc(CityInfoResponse response) {
						ArrayList<CityInfo> result = response.getBody();
						if(result.size()==0){
							return;
						}
						String locationCity = aMapLocation.getCity();
						if(!TextUtils.isEmpty(locationCity)){
							for(CityInfo cityInfo : result){
								if(cityInfo.getAreaName()!=null && cityInfo.getAreaName().contains(locationCity)){
									cityid = cityInfo.getAreaCode();
									initView(topBar);
									return;
								}
							}
						}
						
						cityid = result.get(0).getAreaCode();
						initView(topBar);
					}
				}.setProgressDialog().execute();
			}
		});

        return topBar;
    }
    private void initView(final TopBarContain topBar){
    	final ObjectXListView listView = (ObjectXListView) topBar.findViewById(android.R.id.list);
        listView.setDivider(new ColorDrawable(Color.TRANSPARENT));
        listView.setDividerHeight((int) MyApp.convertToDp(8));

		int column = isPortrait()? 1:2;
        listView.setAdapter(new ObjectXAdapter.GridPagesAdapter<MarketNewsModelList.NewsModel>(column) {
            @Override
            public String getUrl(int page) {
                return "http://faw-vw.allyes.com/index.php?g=api&m=apicache&a=getnews&newsbigtype=0&sourcetype="+sourcetype
                		+"&page="+page+"&pagesize=10" + (sourcetype>0&&cityid!=null ? ("&cityid="+cityid) : "") 
                		+ (showCarItem!=null ? "&Series="+showCarItem.getId() : "");
            }
            @Override
            public View bindGridView(ViewGroup contain, NewsModel t, int position, View convertView)  {
                if(convertView== null){
                	convertView = View.inflate(context, R.layout.market_news_list_item, null);
                }
                BitmapManager.bindView(convertView.findViewById(android.R.id.icon), t.getTHUM_PICTURE());
                ((TextView)convertView.findViewById(android.R.id.title)).setText(t.getTITLE());
                ((TextView)convertView.findViewById(android.R.id.summary)).setText(t.getUPDATE_TIME());
                return convertView;
            }
            @Override
            public List<MarketNewsModelList.NewsModel> instanceNewList(String json) throws Exception {
                return new Gson().fromJson(json, MarketNewsModelList.class).getModels();//用gson实例化json数据
            }
            @Override
            public void onItemClick(MarketNewsModelList.NewsModel t, View view, int position, long id) {
                super.onItemClick(t, view, position, id);
                LocalWebViewFragment.start(getActivity(), t.getCONTENT(), topBar.getTitle(), false);
            }
			@Override
			protected int getDividerHeight() {
				return (int) MyApp.convertToDp(8);
			}
        });
        RadioGroup radioGroup = (RadioGroup) topBar.findViewById(R.id.radio_group);
		radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				CompoundButton checkedBtn = (CompoundButton)group.findViewById(checkedId);
				if(!checkedBtn.isChecked()) return;//avoid check off callback
				switch(checkedId){
				case R.id.radioButton1:
					sourcetype = 0;
					break;
				case R.id.radioButton2:
					sourcetype = 1;
					break;
				case R.id.radioButton3:
					sourcetype = 2;
					break;
				}
				listView.reload();
			}
		});
		((CompoundButton)radioGroup.findViewById(R.id.radioButton1)).setChecked(true);
    }
}
