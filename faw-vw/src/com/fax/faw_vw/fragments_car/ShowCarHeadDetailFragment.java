package com.fax.faw_vw.fragments_car;

import java.util.List;

import com.fax.faw_vw.MyApp;
import com.fax.faw_vw.MyFragment;
import com.fax.faw_vw.R;
import com.fax.faw_vw.model.ShowCarItem;
import com.fax.faw_vw.model.ShowCarItemRes;
import com.fax.faw_vw.model.ShowCarItemRes.ShowCarDetailHead;
import com.fax.faw_vw.util.MapNetFrameUtil;
import com.fax.utils.frameAnim.AssetFrame;
import com.fax.utils.frameAnim.Frame;
import com.fax.utils.frameAnim.FrameFactory;
import com.fax.utils.view.pager.PointIndicator;
import com.fax.utils.view.pager.SamePagerAdapter;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * Created by linfaxin on 2014/7/29 029.
 * Email: linlinfaxin@163.com
 */
public class ShowCarHeadDetailFragment extends MyFragment{
	View.OnClickListener backClick = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			backStack();
		}
	};
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		if(view != null){//防止Fragment在add的时候没有背景色叠加显示出错和touch事件传递底部
			view.setBackgroundResource(android.R.color.transparent);
			view.setOnClickListener(backClick);
		}
	}
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.car_detail_head_detail, container, false);
        final ShowCarDetailHead head = (ShowCarDetailHead) getArguments().getSerializable(ShowCarDetailHead.class.getName());
        final ShowCarItemRes.CarItemChild carItemChild = getSerializableExtra(ShowCarItemRes.CarItemChild.class);
        final ShowCarItem showCarItem = getSerializableExtra(ShowCarItem.class);
        
        view.postDelayed(new Runnable() {
			@Override
			public void run() {
				ViewPager viewPager = (ViewPager) view.findViewById(R.id.view_pager);
				String modelName = carItemChild==null?showCarItem.getModelResName():carItemChild.getNameEn();
		        List<AssetFrame> frames = FrameFactory.createFramesFromAsset(context, head.getAssetDir(modelName), -1);
		        int minBord = Math.min(getResources().getDisplayMetrics().heightPixels, getResources().getDisplayMetrics().widthPixels);
		        int size = isPortrait() ? minBord * 3/4 : minBord * 2/3;
		        viewPager.getLayoutParams().width = viewPager.getLayoutParams().height = size;
		        viewPager.setPageMargin((int) MyApp.convertToDp(10));
		        
		        viewPager.setAdapter(new SamePagerAdapter<AssetFrame>(frames) {
					@Override
					public View getView(AssetFrame t, int position, View convertView) {
						FrameLayout frame = (FrameLayout) convertView;
						if(frame == null){
							frame = new FrameLayout(context);
							frame.addView(new ImageView(context));
							frame.addView(new ProgressBar(context), new FrameLayout.LayoutParams(-2, -2, Gravity.CENTER));
							TextView tv;
							frame.addView( tv = new TextView(context), new FrameLayout.LayoutParams(-2, -2, Gravity.CENTER));
							tv.setTextColor(Color.WHITE);
							frame.setOnClickListener(backClick);
						}
						MapNetFrameUtil.bindImg((ImageView)frame.getChildAt(0), MapNetFrameUtil.getCarDetailUrl(t),
								(ProgressBar)frame.getChildAt(1), (TextView) frame.getChildAt(2));
						return frame;
					}
					@Override
					protected void onItemDestroyed(View view, AssetFrame t) {
						super.onItemDestroyed(view, t);
						t.recycle();
					}
				});
		        
		        PointIndicator pointIndicator = (PointIndicator) view.findViewById(R.id.point_indicator);
		        pointIndicator.bindViewPager(viewPager);
		        viewPager.startAnimation(AnimationUtils.loadAnimation(context, R.anim.alpha_small_to_normal));
		        
	        	View handImg = view.findViewById(R.id.tip_hand);
		        handImg.setVisibility(View.VISIBLE);
		        handImg.startAnimation(AnimationUtils.loadAnimation(context, R.anim.fade_in_out));
			}
		}, 500);
        return view;
    }
}
