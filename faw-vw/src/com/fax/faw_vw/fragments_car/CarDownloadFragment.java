package com.fax.faw_vw.fragments_car;

import java.io.File;
import java.net.URL;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ImageView.ScaleType;
import android.widget.RadioGroup;

import com.fax.faw_vw.FragmentContain;
import com.fax.faw_vw.FragmentContainLandscape;
import com.fax.faw_vw.MyApp;
import com.fax.faw_vw.MyFragment;
import com.fax.faw_vw.R;
import com.fax.faw_vw.fargment_common.PdfReaderFragment;
import com.fax.faw_vw.fragment_360.PackedFileLoader;
import com.fax.faw_vw.fragment_360.Show360FrameFragment;
import com.fax.faw_vw.model.ShowCarItem;
import com.fax.faw_vw.util.HanziToPinyin;
import com.fax.faw_vw.views.FitWidthImageView;
import com.fax.faw_vw.views.MyTopBar;
import com.fax.utils.frameAnim.AssetFrame;
import com.fax.utils.frameAnim.AssetFrameViewPagerFragment;
import com.fax.utils.frameAnim.Frame;
import com.fax.utils.frameAnim.FrameFactory;
import com.fax.utils.frameAnim.ZipFrameViewPagerFragment;
import com.fax.utils.task.DownloadTask;
import com.fax.utils.task.ResultAsyncTask;
import com.fax.utils.view.list.ObjectXAdapter;
import com.fax.utils.view.list.ObjectXListView;

//下载中心

public class CarDownloadFragment extends MyFragment{
	ShowCarItem carItem;
	String dirNameAboutCheckedPage;//与当前页卡相关的asset中的目录
	String currentTabName = "精美图片";
	ObjectXListView listView;
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			MyTopBar topBar = (MyTopBar) new MyTopBar(context).setLeftBack()
					.setTitle("下载中心").setContentView(R.layout.car_download_center);
			
			//取到传入的车型，不同车型的下载中心页不一样
			carItem = getSerializableExtra(ShowCarItem.class);
			
			listView = (ObjectXListView) topBar.findViewById(android.R.id.list);
			listView.setPullRefreshEnable(false);
			int column = isPortrait()? 2:4;
			listView.setAdapter(new ObjectXAdapter.SingleLocalGridPageAdapter<AssetFrame>(column){
				@Override
				public List<AssetFrame> instanceNewList() throws Exception {
					return FrameFactory.createFramesFromAsset(context, dirNameAboutCheckedPage, -1, true);
				}
				@Override
				protected View bindGridView(ViewGroup contain, AssetFrame t, int position, View convertView) {
					FrameLayout layout = (FrameLayout) convertView;
					if(layout==null){
						layout = new FrameLayout(context);
						layout.addView(new FitWidthImageView(context), -1, -1);
						layout.addView(new ImageView(context), -1, -1);
						
						int padding = (int) MyApp.convertToDp(4);
						layout.setPadding(padding, padding, padding, padding);
					}
					ImageView img1 = (ImageView) layout.getChildAt(0);
					img1.setImageDrawable(t.decodeDrawable(context));
					
					ImageView img2 = (ImageView) layout.getChildAt(1);
					img2.setScaleType(ScaleType.CENTER_INSIDE);
					if(!isLookPic() || !isBigImgPackDownloaded()){
						img2.setImageResource(R.drawable.common_img_download);
					}
					
					return layout;
				}
				@Override
				public void onItemClick(AssetFrame t, View view, int position, long id) {
					super.onItemClick(t, view, position, id);
					if(isLookPic()){//查看大图
						openBigImg(position);
						
					}else{
						String resUrl = getResUrl( ((AssetFrame)t).getPath() );
						//区分视频还是Pdf
						if(resUrl.endsWith(".mp4")){
							showTVC(resUrl);
						}else if(resUrl.endsWith(".pdf")){
							downloadAndOpenPdf(resUrl);
						}
					}
				}
				@Override
				public void onLoadFinish(List<AssetFrame> allList) {
					super.onLoadFinish(allList);
					listView.setFootHint("");
				}
				@Override
				protected boolean isAutoLoadAfterInit() {
					return false;
				}
	        });
			
			RadioGroup radioGroup = (RadioGroup) topBar.findViewById(R.id.radio_group);
			radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(RadioGroup group, int checkedId) {
					CompoundButton checkedBtn = (CompoundButton)group.findViewById(checkedId);
					if(!checkedBtn.isChecked()) return;//avoid check off callback
					currentTabName = checkedBtn.getText().toString();
			        dirNameAboutCheckedPage = "download_center/"+carItem.getBaseModelResName()+"/"+checkedBtn.getTag();
			        listView.reload();
				}
			});
			((CompoundButton)radioGroup.findViewById(
					getArguments().getInt(RadioGroup.class.getName(), R.id.car_download_rb_pics))).setChecked(true);
			return topBar;
		}
		private boolean isLookPic(){
			return dirNameAboutCheckedPage.endsWith("pics");
		}
		//返回资源的网络地址（结尾mp4或者pdf）
		private String getResUrl(String filePath){
			String fileNameWithOutEnd = new File(filePath).getName().replace(".jpg", "");
			if(dirNameAboutCheckedPage.contains("shouCe") || dirNameAboutCheckedPage.contains("zazhi")){
				return MyApp.ResUrl + "pdf/d_"+fileNameWithOutEnd + ".pdf";
			}else if(dirNameAboutCheckedPage.contains("TVC")){
				String url = MyApp.ResUrl + "video/";
				
				if(dirNameAboutCheckedPage.contains("BORA")) url+="bl";
				else if(dirNameAboutCheckedPage.contains("CC")) url+="cc";
				else if(dirNameAboutCheckedPage.contains("GOLF")) url+="glof";
				else if(dirNameAboutCheckedPage.contains("JETTA")) url+="jd";
				else if(dirNameAboutCheckedPage.contains("MAGOTAN")) url+="mt";
				else if(dirNameAboutCheckedPage.contains("NEW SAGITAR")) url+="nst";
				else if(dirNameAboutCheckedPage.contains("SAGITAR")) url+="st";
				
				url += ("/video"+fileNameWithOutEnd.replace("tvc", "") + ".mp4");
				return url;
			}
			return null;
		}
		
		private File getBigImgPackDownloadFile(){
			File file = new File(context.getExternalCacheDir(), "download/bigImage_"+carItem.getBaseModelResName()+".zip");
			return file;
		}
		private boolean isBigImgPackDownloaded(){
			return getBigImgPackDownloadFile().length()>0;
		}
		//打开大图（检查是否下载）
		private void openBigImg(final int position){
			String fileName = null;
			if(dirNameAboutCheckedPage.contains("BORA")) fileName="bigImage_baolai";
			else if(dirNameAboutCheckedPage.contains("CC")) fileName="bigImage_cc";
			else if(dirNameAboutCheckedPage.contains("GOLF")) fileName="bigImage_gaoerfu";
			else if(dirNameAboutCheckedPage.contains("JETTA")) fileName="bigImage_jieda";
			else if(dirNameAboutCheckedPage.contains("MAGOTAN")) fileName="bigImage_maiteng";
			else if(dirNameAboutCheckedPage.contains("NEW SAGITAR")) fileName="bigImage_xinsuteng";
			else if(dirNameAboutCheckedPage.contains("SAGITAR")) fileName="bigImage_suteng";
			final String url = MyApp.ResUrl + "download/" + fileName + ".zip";
			final String finalFileName = fileName;
			
			final File file = getBigImgPackDownloadFile();
			final Runnable openRun = new Runnable() {
				@Override
				public void run() {
					FragmentContain.start(getActivity(), 
							ZipFrameViewPagerFragment.createInstance(file.getPath(), finalFileName, position));
				}
			};
			if(file.exists() && !file.isDirectory() && file.length()>0 ){
				openRun.run();
			}else{
				file.delete();
				final Runnable downRun = new Runnable() {
					@Override
					public void run() {
						final File dlFile = new File(file.getParent(), file.getName()+".dl");
						new DownloadTask(context, url, dlFile, true) {
							@Override
							protected void onPostExecuteSuc(File result) {
								dlFile.renameTo(file);
								openRun.run();
								listView.notifyDataSetChanged();
							}
						}.setProgressDialog().execute();
					}
				};
				if(MyApp.isWifiConnect()){
					new AlertDialog.Builder(context).setTitle("下载中心")
					.setMessage("您即将开始下载"+carItem.getModel_cn()+currentTabName+"，请点击确认或取消")
					.setNegativeButton(android.R.string.cancel, null)
					.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							downRun.run();
						}
					}).show();
				}else{

					new ResultAsyncTask<Integer>(context) {
						@Override
						protected void onPostExecuteSuc(Integer result) {
							if(result!=null){
								String fileSizeMsg = result<=0 ? "" : "("+DownloadTask.sizeToString(result)+")";
								new AlertDialog.Builder(context).setTitle("下载中心")
								.setMessage("即将开始下载"+
						carItem.getModel_cn()+currentTabName+fileSizeMsg+"，建议您使用WIFI。土豪随意!^_^")
								.setNegativeButton(android.R.string.cancel, null)
								.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										downRun.run();
									}
								}).show();
							}
						}
						@Override
						protected Integer doInBackground(Void... params) {
							try {
								return new URL(url).openConnection().getContentLength();
							} catch (Exception e) {
								e.printStackTrace();
							}
							return 0;
						}
					}.setProgressDialog().execute();
					
				}
			}
		}
		private void showTVC(final String resUrl){
			final Intent intent = new Intent(Intent.ACTION_VIEW).setDataAndType(Uri.parse(resUrl), "video/*");
			if(MyApp.isWifiConnect()){
				startActivity(intent);
				return;
			}
			new ResultAsyncTask<Integer>(context) {
				@Override
				protected void onPostExecuteSuc(Integer result) {
					if(result!=null){
						String fileSizeMsg = result<=0 ? "" : "("+DownloadTask.sizeToString(result)+")";
						new AlertDialog.Builder(context).setTitle("下载中心")
						.setMessage("即将开始观看"+carItem.getModel_cn()+"TVC"+fileSizeMsg+"，建议您使用WIFI。土豪随意!^_^")
						.setNegativeButton(android.R.string.cancel, null)
						.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
						        startActivity(intent);
							}
						}).show();
					}
				}
				@Override
				protected Integer doInBackground(Void... params) {
					try {
						return new URL(resUrl).openConnection().getContentLength();
					} catch (Exception e) {
						e.printStackTrace();
					}
					return 0;
				}
			}.setProgressDialog().execute();
		}
		
		private void downloadAndOpenPdf(final String url){
			String name = new File(Uri.parse(url).getPath()).getName();
			final File file = new File(context.getExternalCacheDir(), name);
			final Runnable openRun = new Runnable() {
				@Override
				public void run() {
					FragmentContain.start(getActivity(), MyApp.createFragment(PdfReaderFragment.class, file));
				}
			};
			if(file.exists() && !file.isDirectory() && file.length()>0 ){
				openRun.run();
			}else{
				file.delete();
				final Runnable downRun = new Runnable() {
					@Override
					public void run() {
						final File dlFile = new File(file.getParent(), file.getName()+".dl");
						new DownloadTask(context, url, dlFile, true) {
							@Override
							protected void onPostExecuteSuc(File result) {
								dlFile.renameTo(file);
								openRun.run();
							}
						}.setProgressDialog().execute();
					}
				};
				if(MyApp.isWifiConnect()){
					new AlertDialog.Builder(context).setTitle("下载中心")
					.setMessage("您即将开始下载"+carItem.getModel_cn()+currentTabName+"，请点击确认或取消")
					.setNegativeButton(android.R.string.cancel, null)
					.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							downRun.run();
						}
					}).show();
				}else{

					new ResultAsyncTask<Integer>(context) {
						@Override
						protected void onPostExecuteSuc(Integer result) {
							if(result!=null){
								String fileSizeMsg = result<=0 ? "" : "("+DownloadTask.sizeToString(result)+")";
								new AlertDialog.Builder(context).setTitle("下载中心")
								.setMessage("即将开始下载"+
						carItem.getModel_cn()+currentTabName+ fileSizeMsg +"，建议您使用WIFI。土豪随意!^_^")
								.setNegativeButton(android.R.string.cancel, null)
								.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										downRun.run();
									}
								}).show();
							}
						}
						@Override
						protected Integer doInBackground(Void... params) {
							try {
								return new URL(url).openConnection().getContentLength();
							} catch (Exception e) {
								e.printStackTrace();
							}
							return 0;
						}
					}.setProgressDialog().execute();
					
				}
			}
		}
}
