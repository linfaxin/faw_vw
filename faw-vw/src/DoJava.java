import java.io.File;
import java.io.IOException;
import java.util.Arrays;


public class DoJava {
	public static void main(String[] args){
		File dir = new File("assets/car_detail/NEW SAGITAR");
		clearAllFileContent(dir);
	}
	
	private static void clearAllFileContent(File dir){
		for(File file : dir.listFiles()){
			if(file.isDirectory()){
				clearAllFileContent(file);
				continue;
			};
			System.out.println("clear:"+file.getPath());
			file.delete();
			try {
				new File(file.getPath().replace("@2x", "")).createNewFile();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
