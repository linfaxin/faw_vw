package com.fax.utils.view.pager;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class FixViewPager extends ViewPager {

    public FixViewPager(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public FixViewPager(Context context) {
		super(context);
	}

	@Override
    public boolean onTouchEvent(MotionEvent ev) {
        try {
            return super.onTouchEvent(ev);
        } catch (IllegalArgumentException ignore) {//avoid pointerIndex out of range in some android4.x
        }
        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        try {
            return super.onInterceptTouchEvent(ev);
        } catch (IllegalArgumentException ignore) {//avoid pointerIndex out of range in some android4.x
        }
        return false;
    }
}
